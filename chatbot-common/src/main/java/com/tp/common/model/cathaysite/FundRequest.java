package com.tp.common.model.cathaysite;

import com.tp.common.model.BotRequest;

public class FundRequest extends BotRequest {

    private String cstno;
    private String fund;
    private String urlPara;
    private String clientIp;
    private String searchType; //nav:淨值, allotinterest:配息
    private String isallot; //1:配息 0:不配息
    private String sendType; //1:單筆申購;2:定期定額;3:定期不定額
    private String token;
    
    private String cmd;
    private String type;
    private boolean isPass;
    private String fundName;
    private String fundkindTxt;
    private String kyctypeTxt;
    
    private String amt;
    private String rdate;
    private String acnt;
    private String sno;
    private String acntName;
    private String pwd;
    private String query;//代碼
    private String note;
    private String fundDoc;
    private String tradetype;
    private String highamt;
    private String midamt;
    private String lowamt;
    private String stype;
    private String sday;
    
    private String otype;
    private String tType;
    private String tnos;
    
    private String dataString;
    private Integer tCount;
    
    private String runit;
    private String declare;
    
    private String uid;
    private String birth;
    
    private boolean onlyOne;
    
    public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}
    public String getCstno() {
        return cstno;
    }

    public void setCstno(String cstno) {
        this.cstno = cstno;
    }

    public String getFund() {
        return fund;
    }

    public void setFund(String fund) {
        this.fund = fund;
    }

    public String getUrlPara() {
        return urlPara;
    }

    public void setUrlPara(String urlPara) {
        this.urlPara = urlPara;
    }


    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

	public String getIsallot() {
		return isallot;
	}

	public void setIsallot(String isallot) {
		this.isallot = isallot;
	}

	public String getSendType() {
		return sendType;
	}

	public void setSendType(String sendType) {
		this.sendType = sendType;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getAmt() {
		return amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public String getRdate() {
		return rdate;
	}

	public void setRdate(String rdate) {
		this.rdate = rdate;
	}

	public String getAcnt() {
		return acnt;
	}

	public void setAcnt(String acnt) {
		this.acnt = acnt;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getCmd() {
		return cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	public String getFundkindTxt() {
		return fundkindTxt;
	}

	public void setFundkindTxt(String fundkindTxt) {
		this.fundkindTxt = fundkindTxt;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isPass() {
		return isPass;
	}

	public void setPass(boolean isPass) {
		this.isPass = isPass;
	}

	public String getAcntName() {
		return acntName;
	}

	public void setAcntName(String acntName) {
		this.acntName = acntName;
	}

	public String getSno() {
		return sno;
	}

	public void setSno(String sno) {
		this.sno = sno;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getFundDoc() {
		return fundDoc;
	}

	public void setFundDoc(String fundDoc) {
		this.fundDoc = fundDoc;
	}

	public String getTradetype() {
		return tradetype;
	}

	public void setTradetype(String tradetype) {
		this.tradetype = tradetype;
	}

	public String getHighamt() {
		return highamt;
	}

	public void setHighamt(String highamt) {
		this.highamt = highamt;
	}

	public String getMidamt() {
		return midamt;
	}

	public void setMidamt(String midamt) {
		this.midamt = midamt;
	}

	public String getLowamt() {
		return lowamt;
	}

	public void setLowamt(String lowamt) {
		this.lowamt = lowamt;
	}

	public String getStype() {
		return stype;
	}

	public void setStype(String stype) {
		this.stype = stype;
	}

	public String getSday() {
		return sday;
	}

	public void setSday(String sday) {
		this.sday = sday;
	}

	public String getOtype() {
		return otype;
	}

	public void setOtype(String otype) {
		this.otype = otype;
	}

	public String gettType() {
		return tType;
	}

	public void settType(String tType) {
		this.tType = tType;
	}

	public String getTnos() {
		return tnos;
	}

	public void setTnos(String tnos) {
		this.tnos = tnos;
	}

	public String getDataString() {
		return dataString;
	}

	public void setDataString(String dataString) {
		this.dataString = dataString;
	}

	public Integer gettCount() {
		return tCount;
	}

	public void settCount(Integer tCount) {
		this.tCount = tCount;
	}

	public String getKyctypeTxt() {
		return kyctypeTxt;
	}

	public void setKyctypeTxt(String kyctypeTxt) {
		this.kyctypeTxt = kyctypeTxt;
	}	

	public String getRunit() {
		return runit;
	}

	public void setRunit(String runit) {
		this.runit = runit;
	}

	public String getDeclare() {
		return declare;
	}

	public void setDeclare(String declare) {
		this.declare = declare;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getBirth() {
		return birth;
	}

	public void setBirth(String birth) {
		this.birth = birth;
	}

	public boolean isOnlyOne() {
		return onlyOne;
	}

	public void setOnlyOne(boolean onlyOne) {
		this.onlyOne = onlyOne;
	}

}
