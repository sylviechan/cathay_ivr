package com.tp.common.model.cathaysec;

import com.tp.common.model.BotRequest;

public class DefaultRequest extends BotRequest {

	private String date;
	private String type;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
