package com.tp.common.model;

import com.tp.common.util.QuickReplay;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * TPIGatewayResult
 */
public class TPIGatewayResult extends HashMap<String, Object> {

    private static final long serialVersionUID = 1L;

    public TPIGatewayResult() {
        put("code", 0);
        put("msg", "success");
    }

    public static TPIGatewayResult error() {
        return error(500, "系統異常");
    }

    public static TPIGatewayResult error(String msg) {
        return error(500, msg);
    }

    public static TPIGatewayResult error(int code, String msg) {
        TPIGatewayResult r = new TPIGatewayResult();
        r.put("code", code);
        r.put("msg", msg);
        return r;
    }

    public static TPIGatewayResult ok(String msg) {
        TPIGatewayResult r = new TPIGatewayResult();
        r.put("msg", msg);
        return r;
    }

    public static TPIGatewayResult ok(Map<String, Object> map) {
        TPIGatewayResult r = new TPIGatewayResult();
        r.putAll(map);
        return r;
    }

    public static TPIGatewayResult ok() {
        return new TPIGatewayResult();
    }

    public TPIGatewayResult setData(Object value) {
        super.put("data", value);
        return this;
    }

    public TPIGatewayResult setMsgList(List<String> msgList) {
        super.put("msgList", msgList);
        return this;
    }

    public TPIGatewayResult setOptionList(List<QuickReplay> quickReplays) {
        super.put("optionList", quickReplays);
        return this;
    }

    @Override
    public TPIGatewayResult put(String key, Object value) {
        super.put(key, value);
        return this;
    }

    public int getCode() {
        return (int) get("code");
    }

    public boolean isSuccess() {
        return (int) get("code") == 0;
    }

    public String getMsg() {
        return (String) get("msg");
    }

    public Object getData() {
        return get("data");
    }

}
