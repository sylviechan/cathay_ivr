package com.tp.common.model.cathaysec;

public class AccountData {
	
	private String accountType;
	
	// 分公司名稱
	private String branchDesc;
	
	// 分公司代碼
	private String branchCode;
	
	// 帳號
	private String account;
	
	// 是否已有委任人
	private String isExistAgent;
	
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getBranchDesc() {
		return branchDesc;
	}
	public void setBranchDesc(String branchDesc) {
		this.branchDesc = branchDesc;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getIsExistAgent() {
		return isExistAgent;
	}
	public void setIsExistAgent(String isExistAgent) {
		this.isExistAgent = isExistAgent;
	}
		
}
