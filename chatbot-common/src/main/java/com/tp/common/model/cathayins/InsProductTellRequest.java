package com.tp.common.model.cathayins;

public class InsProductTellRequest {
	// commodityType 範例:
	//-------------------------------------------------------------------------------------
	public static final String PC01 = "旅遊綜合保險";
	public static final String PC01_A = "國泰產險旅遊綜合保險";
	public static final String PC01_A01 = "海外旅遊";
	public static final String PC01_A01001 = "旅平險";
	public static final String PC01_A01001_1 = "旅遊傷害";
	public static final String PC01_A01001_2 = "旅遊傷害特定事故";
	public static final String PC01_A01002 = "不便險";
	public static final String PC01_A01002_1 = "第三人責任";
	public static final String PC01_A01002_2 = "行李損失";
	public static final String PC01_A02 = "國內旅遊";
	public static final String PC01_A02001 = "旅平險";
	public static final String PC01_A02001_1 = "旅遊傷害";
	public static final String PC01_A02001_2 = "旅遊傷害特定事故";
	public static final String PC01_A02002 = "不便險";
	public static final String PC01_A02002_1 = "第三人責任";
	public static final String PC01_A02002_2 = "旅程縮短";
	public static final String PC01_B01001 = "刷國泰世華銀行信用卡買國泰人壽旅平險送的不便險";
	public static final String PC01_B01001_1 = "第三人責任";
	public static final String PC01_B01001_2 = "行李損失";
	public static final String PC01_B01001_2_IS = "承保範圍";
	public static final String PC01_B01001_2_CIA = "理賠項目/金額";
	//---------------------------------------------------------------------------------------
	
	public static final String IS = "INSURE_SCOPE";// 承保範圍
	public static final String CIA = "CLAIM_ITEM_AMOUNT";// 理賠項目/金額
	public static final String NII = "NOT_INSURE_ITEM";// 不保物品
	public static final String NIM = "NOT_INSURE_MATTER";// 不保事項
	public static final String EL = "EXCL_LIAB";// 除外責任
	public static final String NC = "NOTICE_CONTEXT";// 其他應注意/遵守事項
	public static final String PD = "PREPARE_DOCUMENT";// 理賠應備文件
	
	public static final String INSURE_SCOPE = "承保範圍";
	public static final String CLAIM_ITEM_AMOUNT = "理賠項目/金額";
	public static final String NOT_INSURE_ITEM = "不保物品";
	public static final String NOT_INSURE_MATTER = "不保事項";
	public static final String EXCL_LIAB = "除外責任";
	public static final String NOTICE_CONTEXT = "其他應注意/遵守事項";
	public static final String PREPARE_DOCUMENT = "理賠應備文件";

	private String sessionId;
	private String channel;
	private String role;
	private String cmd;
	private String msg;
	private String commodityType;
	private String commodityClick;
	private String commodityClickName;
	private String productCode;
	private String productSubCode;
	private String codeIdL1;
	private String codeIdL2;
	private String codeIdL3;
	private String insureItemId;
	private String policyNo;
	private String productPolicyCode;
	private String productSubId;
	private String categoryCode;
	private String ssoId;
	private String conflictType; //衝突項目
	private String insType; //險種: 1:車險 2:旅綜險 3:健傷險 4:其他

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getCmd() {
		return cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCommodityType() {
		return commodityType;
	}

	public void setCommodityType(String commodityType) {
		this.commodityType = commodityType;
	}
	
	public String getInsureItemId() {
		return insureItemId;
	}
	
	public void setInsureItemId(String insureItemId) {
		this.insureItemId = insureItemId;
	}
	
	public String getPolicyNo() {
		return policyNo;
	}
	
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	
	public String getProductPolicyCode() {
		return productPolicyCode;
	}
	
	public void setProductPolicyCode(String productPolicyCode) {
		this.productPolicyCode = productPolicyCode;
	}
	
	public String getProductSubId() {
		return productSubId;
	}
	
	public void setProductSubId(String productSubId) {
		this.productSubId = productSubId;
	}

	public String getProductCode() {
		return productCode;
	}

	public String getProductSubCode() {
		return productSubCode;
	}

	public String getCodeIdL1() {
		return codeIdL1;
	}

	public String getCodeIdL2() {
		return codeIdL2;
	}

	public String getCodeIdL3() {
		return codeIdL3;
	}
	
	public String getCommodityClick() {
		return commodityClick;
	}
	
	public String getCommodityClickName() {
		return commodityClickName;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getSsoId() {
		return ssoId;
	}

	public void setSsoId(String ssoId) {
		this.ssoId = ssoId;
	}

	public String getConflictType() {
		return conflictType;
	}

	public void setConflictType(String conflictType) {
		this.conflictType = conflictType;
	}

	public String getInsType() {
		return insType;
	}

	public void setInsType(String insType) {
		this.insType = insType;
	}

	//拆解commodityType
	public InsProductTellRequest AnalyzeCommodityType() {
		String[] types = this.commodityType.split("_");
		this.productCode = types[0].substring(0, 1);
		this.productSubCode = types[0];
		
		if (types.length == 2) {
			getCodeIdL1L2L3(types[1]);
			
		} else if (types.length == 3) {
			getCodeIdL1L2L3(types[1]);
			this.insureItemId = types[2];
			
		} else if (types.length == 4) {
			getCodeIdL1L2L3(types[1]);
			this.insureItemId = types[2];
			getProjectDescription(types[3]);
		}
		
		return this;
	}
	
	//取出CodeIdL1、CodeIdL2、CodeIdL3
	private void getCodeIdL1L2L3(String type) {
		int length = type.length();
		switch (length) {
		case 1:
			this.codeIdL1 = type;
			this.codeIdL2 = "";
			this.codeIdL3 = "";
			break;
		case 3:
			this.codeIdL1 = type.substring(0,1);
			this.codeIdL2 = type;
			this.codeIdL3 = "";
			break;
		case 6:
			this.codeIdL1 = type.substring(0,1);
			this.codeIdL2 = type.substring(0,3);
			this.codeIdL3 = type;
			break;
		default:
			this.codeIdL1 = "";
			this.codeIdL2 = "";
			this.codeIdL3 = "";
			break;
		}
	}

	//取出ProjectDescription
	public void getProjectDescription(String type) {
		switch (type) {
		case "IS":
			this.commodityClick = this.IS;
			this.commodityClickName = this.INSURE_SCOPE;
			break;
		case "CIA":
			this.commodityClick = this.CIA;
			this.commodityClickName = this.CLAIM_ITEM_AMOUNT;
			break;
		case "NII":
			this.commodityClick = this.NII;
			this.commodityClickName = this.NOT_INSURE_ITEM;
			break;
		case "NIM":
			this.commodityClick = this.NIM;
			this.commodityClickName = this.NOT_INSURE_MATTER;
			break;
		case "EL":
			this.commodityClick = this.EL;
			this.commodityClickName = this.EXCL_LIAB;
			break;
		case "NC":
			this.commodityClick = this.NC;
			this.commodityClickName = this.NOTICE_CONTEXT;
			break;
		case "PD":
			this.commodityClick = this.PD;
			this.commodityClickName = this.PREPARE_DOCUMENT;
			break;
		default:
			this.commodityClick = "";
			this.commodityClickName = "";
			break;
		}
	}

}
