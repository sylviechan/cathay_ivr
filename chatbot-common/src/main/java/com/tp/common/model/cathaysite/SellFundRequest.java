package com.tp.common.model.cathaysite;

import com.tp.common.model.BotRequest;

public class SellFundRequest extends BotRequest {

	private String fund;
	private String clientIp;
	private String token;

	private String fundName;
	private String fundkindTxt;
	private String kyctypeTxt;

	private String sno;
	private String acnt;
	private String acntName;
	private String pwd;

	private String tcode;
	private String runit;
	private String rtype;
	private String ramt;
	private String trcode;
	private String trfund;
	private String trfundName;
	private String trfundkindTxt;

	public String getFund() {
		return fund;
	}

	public void setFund(String fund) {
		this.fund = fund;
	}

	public String getClientIp() {
		return clientIp;
	}

	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getFundName() {
		return fundName;
	}

	public void setFundName(String fundName) {
		this.fundName = fundName;
	}

	public String getFundkindTxt() {
		return fundkindTxt;
	}

	public void setFundkindTxt(String fundkindTxt) {
		this.fundkindTxt = fundkindTxt;
	}

	public String getKyctypeTxt() {
		return kyctypeTxt;
	}

	public void setKyctypeTxt(String kyctypeTxt) {
		this.kyctypeTxt = kyctypeTxt;
	}

	public String getSno() {
		return sno;
	}

	public void setSno(String sno) {
		this.sno = sno;
	}

	public String getAcnt() {
		return acnt;
	}

	public void setAcnt(String acnt) {
		this.acnt = acnt;
	}

	public String getAcntName() {
		return acntName;
	}

	public void setAcntName(String acntName) {
		this.acntName = acntName;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getTcode() {
		return tcode;
	}

	public void setTcode(String tcode) {
		this.tcode = tcode;
	}

	public String getRunit() {
		return runit;
	}

	public void setRunit(String runit) {
		this.runit = runit;
	}

	public String getRtype() {
		return rtype;
	}

	public void setRtype(String rtype) {
		this.rtype = rtype;
	}

	public String getRamt() {
		return ramt;
	}

	public void setRamt(String ramt) {
		this.ramt = ramt;
	}

	public String getTrcode() {
		return trcode;
	}

	public void setTrcode(String trcode) {
		this.trcode = trcode;
	}

	public String getTrfund() {
		return trfund;
	}

	public void setTrfund(String trfund) {
		this.trfund = trfund;
	}

	public String getTrfundName() {
		return trfundName;
	}

	public void setTrfundName(String trfundName) {
		this.trfundName = trfundName;
	}

	public String getTrfundkindTxt() {
		return trfundkindTxt;
	}

	public void setTrfundkindTxt(String trfundkindTxt) {
		this.trfundkindTxt = trfundkindTxt;
	}
}