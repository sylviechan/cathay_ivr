package com.tp.common.model.cathaysite;

import com.tp.common.model.BotRequest;

public class SiteAccountRequest extends BotRequest {
	private String clientIp;
	private String type;
	private String token;

	public String getClientIp() {
		return clientIp;
	}

	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}