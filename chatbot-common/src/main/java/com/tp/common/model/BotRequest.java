package com.tp.common.model;

import org.apache.commons.lang3.StringUtils;

public class BotRequest {

	private String sessionId;
	private String channel;
	private String role;
	private String cmd;
	private String msg;
	private String ssoId;
	private String deviceType; //andriod, ios, other

    public boolean isMobile() {
        return StringUtils.equalsIgnoreCase(this.deviceType, "andriod") || StringUtils.equals(this.deviceType, "ios");
    }

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getCmd() {
		return cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getSsoId() {
		return ssoId;
	}

	public void setSsoId(String ssoId) {
		this.ssoId = ssoId;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
}
