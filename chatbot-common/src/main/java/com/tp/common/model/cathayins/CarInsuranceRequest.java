package com.tp.common.model.cathayins;

import java.util.List;
import java.util.Map;

public class CarInsuranceRequest {

	private String sessionId;
	private String channel;
	private String role;
	private String cmd;
	private String msg;
	private String memberName;
	private String memberId;
	private String memberMarriage;
	private String memberToken;
	private String viewDevice;
	private String loginIp;
	private String receiveType;
	private String ssoId;

	private String carNoArray;// 多筆逗號分開

	private String policyNoArray;// 多筆逗號分開

	private String idImgPositive;// 身分證件正面BASE64
	private String idImgNegative;// 身分證件反面BASE64

	private String newCellularPhone;// 新市話
	private String newPhone;// 新手機
	private String newEmail;// 新信箱
	private String newAddress;// 新地址

	private String newCarNo;// 新車號
	private String newEngineNo;// 新引擎號
	private String newOriginIssueDate;// 新汽車發證日
	private String vehicleFrontBase64;// 汽車證件

	private String newMemberName;// 新名字

	private List<Map<String, Object>> ajaxData;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getCmd() {
		return cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getMemberMarriage() {
		return memberMarriage;
	}

	public void setMemberMarriage(String memberMarriage) {
		this.memberMarriage = memberMarriage;
	}

	public String getMemberToken() {
		return memberToken;
	}

	public void setMemberToken(String memberToken) {
		this.memberToken = memberToken;
	}

	public String getViewDevice() {
		return viewDevice;
	}

	public void setViewDevice(String viewDevice) {
		this.viewDevice = viewDevice;
	}

	public String getIdImgPositive() {
		return idImgPositive;
	}

	public void setIdImgPositive(String idImgPositive) {
		this.idImgPositive = idImgPositive;
	}

	public String getIdImgNegative() {
		return idImgNegative;
	}

	public void setIdImgNegative(String idImgNegative) {
		this.idImgNegative = idImgNegative;
	}

	public String getPolicyNoArray() {
		return policyNoArray;
	}

	public void setPolicyNoArray(String policyNoArray) {
		this.policyNoArray = policyNoArray;
	}

	public String getLoginIp() {
		return loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

	public String getCarNoArray() {
		return carNoArray;
	}

	public void setCarNoArray(String carNoArray) {
		this.carNoArray = carNoArray;
	}

	public List<Map<String, Object>> getAjaxData() {
		return ajaxData;
	}

	public void setAjaxData(List<Map<String, Object>> ajaxData) {
		this.ajaxData = ajaxData;
	}

	public String getNewCellularPhone() {
		return newCellularPhone;
	}

	public void setNewCellularPhone(String newCellularPhone) {
		this.newCellularPhone = newCellularPhone;
	}

	public String getNewPhone() {
		return newPhone;
	}

	public void setNewPhone(String newPhone) {
		this.newPhone = newPhone;
	}

	public String getNewEmail() {
		return newEmail;
	}

	public void setNewEmail(String newEmail) {
		this.newEmail = newEmail;
	}

	public String getNewAddress() {
		return newAddress;
	}

	public void setNewAddress(String newAddress) {
		this.newAddress = newAddress;
	}

	public String getNewCarNo() {
		return newCarNo;
	}

	public void setNewCarNo(String newCarNo) {
		this.newCarNo = newCarNo;
	}

	public String getNewEngineNo() {
		return newEngineNo;
	}

	public void setNewEngineNo(String newEngineNo) {
		this.newEngineNo = newEngineNo;
	}

	public String getNewOriginIssueDate() {
		return newOriginIssueDate;
	}

	public void setNewOriginIssueDate(String newOriginIssueDate) {
		this.newOriginIssueDate = newOriginIssueDate;
	}

	public String getVehicleFrontBase64() {
		return vehicleFrontBase64;
	}

	public void setVehicleFrontBase64(String vehicleFrontBase64) {
		this.vehicleFrontBase64 = vehicleFrontBase64;
	}

	public String getNewMemberName() {
		return newMemberName;
	}

	public void setNewMemberName(String newMemberName) {
		this.newMemberName = newMemberName;
	}

	public String getReceiveType() {
		return receiveType;
	}

	public void setReceiveType(String receiveType) {
		this.receiveType = receiveType;
	}

	public String getSsoId() {
		return ssoId;
	}

	public void setSsoId(String ssoId) {
		this.ssoId = ssoId;
	}

}
