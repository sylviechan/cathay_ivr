package com.tp.common.model;

import java.util.List;
import java.util.Map;

import com.tp.common.util.QuickReplay;

public class DemandProductionCardItem {

	private int cImageTextType;
	private String title;
	private String imageName;
	private List<Map<String, Object>> cTextItemListMap;
	private String cTextItemMapTitleKey;
	private String cTextItemMapValKey;
	private List<QuickReplay> quickReplayList;

	public int getcImageTextType() {
		return cImageTextType;
	}

	public void setcImageTextType(int cImageTextType) {
		this.cImageTextType = cImageTextType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public List<Map<String, Object>> getcTextItemListMap() {
		return cTextItemListMap;
	}

	public void setcTextItemListMap(List<Map<String, Object>> cTextItemListMap) {
		this.cTextItemListMap = cTextItemListMap;
	}

	public String getcTextItemMapTitleKey() {
		return cTextItemMapTitleKey;
	}

	public void setcTextItemMapTitleKey(String cTextItemMapTitleKey) {
		this.cTextItemMapTitleKey = cTextItemMapTitleKey;
	}

	public String getcTextItemMapValKey() {
		return cTextItemMapValKey;
	}

	public void setcTextItemMapValKey(String cTextItemMapValKey) {
		this.cTextItemMapValKey = cTextItemMapValKey;
	}

	public List<QuickReplay> getQuickReplayList() {
		return quickReplayList;
	}

	public void setQuickReplayList(List<QuickReplay> quickReplayList) {
		this.quickReplayList = quickReplayList;
	}

}
