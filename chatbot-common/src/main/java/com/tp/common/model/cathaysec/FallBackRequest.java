package com.tp.common.model.cathaysec;

import com.tp.common.model.BotRequest;

import java.util.List;
import java.util.Map;

public class FallBackRequest extends BotRequest {

	private String firstNluCanAnswer;

	private List<Map<String, Object>> faqResult;

	private List<Map<String, Object>> firstNluIntents;

	private String secondBot;

	private String fallbackNlu;

	public String getSecondBot() {
		return secondBot;
	}

	public String getFirstNluCanAnswer() {
		return firstNluCanAnswer;
	}

	public void setFirstNluCanAnswer(String firstNluCanAnswer) {
		this.firstNluCanAnswer = firstNluCanAnswer;
	}

	public List<Map<String, Object>> getFaqResult() {
		return faqResult;
	}

	public void setFaqResult(List<Map<String, Object>> faqResult) {
		this.faqResult = faqResult;
	}

	public List<Map<String, Object>> getFirstNluIntents() {
		return firstNluIntents;
	}

	public void setFirstNluIntents(List<Map<String, Object>> firstNluIntents) {
		this.firstNluIntents = firstNluIntents;
	}

	public void setSecondBot(String secondBot) {
		this.secondBot = secondBot;
	}

	public String getFallbackNlu() {
		return fallbackNlu;
	}

	public void setFallbackNlu(String fallbackNlu) {
		this.fallbackNlu = fallbackNlu;
	}
}
