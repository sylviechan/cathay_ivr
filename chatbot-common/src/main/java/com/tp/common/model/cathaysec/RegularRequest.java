package com.tp.common.model.cathaysec;

import com.tp.common.model.BotRequest;

public class RegularRequest extends BotRequest {

	private String functionId;
	private String branchCode;
	private String id;
	private String account;
	private String symbol;
	private String symbolName;
	private String groupseq;
	private String chargeAmt;
	private String chargeDay;
	private String cn;
	private String plainText;
	private String p1Sign;
	private String caNum;
	private String plainTextUnCode;
	private String ip;

	private String udid;

	private String money;
	private String month;
	private String day;
	private String name;
	private String stockId;
	private String stockName;

	private String sort;
	private String status;
	private String amount;
	private String kind;

	private String closePrice;
	private String riseFallPcnt;
	private String eps;
	private String yields;

	private String yieldsDate;
	private String epsYq;

	private String SumAmt;
	private String SumQty;
	private String AvgPrice;

	private String question;
	private String answer;
	private String cmtDesc;
	private String cmtPic;
	private String showQA;
	private String phone;
	private String cusStatus;

	private String signDate;
	private String device;

	public String getUdid() {
		return udid;
	}

	public void setUdid(String udid) {
		this.udid = udid;
	}

	public String getFunctionId() {
		return functionId;
	}

	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getChargeAmt() {
		return chargeAmt;
	}

	public void setChargeAmt(String chargeAmt) {
		this.chargeAmt = chargeAmt;
	}

	public String getChargeDay() {
		return chargeDay;
	}

	public void setChargeDay(String chargeDay) {
		this.chargeDay = chargeDay;
	}

	public String getCn() {
		return cn;
	}

	public void setCn(String cn) {
		this.cn = cn;
	}

	public String getPlainText() {
		return plainText;
	}

	public void setPlainText(String plainText) {
		this.plainText = plainText;
	}

	public String getP1Sign() {
		return p1Sign;
	}

	public void setP1Sign(String p1Sign) {
		this.p1Sign = p1Sign;
	}

	public String getCaNum() {
		return caNum;
	}

	public void setCaNum(String caNum) {
		this.caNum = caNum;
	}

	public String getPlainTextUnCode() {
		return plainTextUnCode;
	}

	public void setPlainTextUnCode(String plainTextUnCode) {
		this.plainTextUnCode = plainTextUnCode;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStockId() {
		return stockId;
	}

	public void setStockId(String stockId) {
		this.stockId = stockId;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getClosePrice() {
		return closePrice;
	}

	public void setClosePrice(String closePrice) {
		this.closePrice = closePrice;
	}

	public String getRiseFallPcnt() {
		return riseFallPcnt;
	}

	public void setRiseFallPcnt(String riseFallPcnt) {
		this.riseFallPcnt = riseFallPcnt;
	}

	public String getEps() {
		return eps;
	}

	public void setEps(String eps) {
		this.eps = eps;
	}

	public String getYields() {
		return yields;
	}

	public void setYields(String yields) {
		this.yields = yields;
	}

	public String getSymbolName() {
		return symbolName;
	}

	public void setSymbolName(String symbolName) {
		this.symbolName = symbolName;
	}

	public String getGroupseq() {
		return groupseq;
	}

	public void setGroupseq(String groupseq) {
		this.groupseq = groupseq;
	}

	public String getYieldsDate() {
		return yieldsDate;
	}

	public void setYieldsDate(String yieldsDate) {
		this.yieldsDate = yieldsDate;
	}

	public String getEpsYq() {
		return epsYq;
	}

	public void setEpsYq(String epsYq) {
		this.epsYq = epsYq;
	}

	public String getSumAmt() {
		return SumAmt;
	}

	public void setSumAmt(String sumAmt) {
		SumAmt = sumAmt;
	}

	public String getSumQty() {
		return SumQty;
	}

	public void setSumQty(String sumQty) {
		SumQty = sumQty;
	}

	public String getAvgPrice() {
		return AvgPrice;
	}

	public void setAvgPrice(String avgPrice) {
		AvgPrice = avgPrice;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getCmtDesc() {
		return cmtDesc;
	}

	public void setCmtDesc(String cmtDesc) {
		this.cmtDesc = cmtDesc;
	}

	public String getCmtPic() {
		return cmtPic;
	}

	public void setCmtPic(String cmtPic) {
		this.cmtPic = cmtPic;
	}

	public String getShowQA() {
		return showQA;
	}

	public RegularRequest setShowQA(String showQA) {
		this.showQA = showQA;
		return this;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCusStatus() {
		return cusStatus;
	}

	public void setCusStatus(String cusStatus) {
		this.cusStatus = cusStatus;
	}

	public String getSignDate() {
		return signDate;
	}

	public void setSignDate(String signDate) {
		this.signDate = signDate;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

}
