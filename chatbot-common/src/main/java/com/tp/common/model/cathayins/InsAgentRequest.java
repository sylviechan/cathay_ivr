package com.tp.common.model.cathayins;

public class InsAgentRequest {

	private String channel;
	private String role;
	private String sessionId;
	private String loginIp;

	private String msg;
	private String cmd;

	private String chatID;
	private String chatType;
	private String chatLog;
	private String chatLogUrl;
	private String customerId;
	private boolean isAppeal;
	private String userName;
	private String sex;
	private String policyNo;
	private String rejectLogin;
	private String loginStatus;

	private String customerMsgId;
	private String chatMessage;
	private String faqAnsList;

	private String agentId;
	private String comment;
	private String commentType;
	private String disConnCode;
	private String reason;

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getLoginIp() {
		return loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCmd() {
		return cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public String getChatID() {
		return chatID;
	}

	public void setChatID(String chatID) {
		this.chatID = chatID;
	}

	public String getChatType() {
		return chatType;
	}

	public void setChatType(String chatType) {
		this.chatType = chatType;
	}

	public String getChatLog() {
		return chatLog;
	}

	public void setChatLog(String chatLog) {
		this.chatLog = chatLog;
	}

	public String getChatLogUrl() {
		return chatLogUrl;
	}

	public void setChatLogUrl(String chatLogUrl) {
		this.chatLogUrl = chatLogUrl;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public boolean isAppeal() {
		return isAppeal;
	}

	public void setAppeal(boolean isAppeal) {
		this.isAppeal = isAppeal;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public String getRejectLogin() {
		return rejectLogin;
	}

	public void setRejectLogin(String rejectLogin) {
		this.rejectLogin = rejectLogin;
	}

	public String getLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(String loginStatus) {
		this.loginStatus = loginStatus;
	}

	public String getCustomerMsgId() {
		return customerMsgId;
	}

	public void setCustomerMsgId(String customerMsgId) {
		this.customerMsgId = customerMsgId;
	}

	public String getChatMessage() {
		return chatMessage;
	}

	public void setChatMessage(String chatMessage) {
		this.chatMessage = chatMessage;
	}

	public String getFaqAnsList() {
		return faqAnsList;
	}

	public void setFaqAnsList(String faqAnsList) {
		this.faqAnsList = faqAnsList;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getCommentType() {
		return commentType;
	}

	public void setCommentType(String commentType) {
		this.commentType = commentType;
	}

	public String getDisConnCode() {
		return disConnCode;
	}

	public void setDisConnCode(String disConnCode) {
		this.disConnCode = disConnCode;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
