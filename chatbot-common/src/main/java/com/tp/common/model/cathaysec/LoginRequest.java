package com.tp.common.model.cathaysec;

import com.tp.common.model.BotRequest;

import java.util.List;

public class LoginRequest extends BotRequest {

	private List<AccountData> accountDatas; //全部帳號資料

    private String id;
    
    private String password;
    
    private String clientIP;
    
    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClientIP() {
		return clientIP;
	}

	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}

	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public List<AccountData> getAccountDatas() {
		return accountDatas;
	}

	public void setAccountDatas(List<AccountData> accountDatas) {
		this.accountDatas = accountDatas;
	}

}
