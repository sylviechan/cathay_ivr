package com.tp.common.model.cathaysite;

import com.tp.common.model.BotRequest;

public class FundStopRequest extends BotRequest {
	private String urlPara;
	private String clientIp;
	private String tno;
	private String pwd;
	private String token;

	public String getUrlPara() {
		return urlPara;
	}

	public void setUrlPara(String urlPara) {
		this.urlPara = urlPara;
	}

	public String getClientIp() {
		return clientIp;
	}

	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}

	public String getTno() {
		return tno;
	}

	public void setTno(String tno) {
		this.tno = tno;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}