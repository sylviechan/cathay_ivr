package com.tp.common.model.cathayins;

public class InsuranceRequest {

	private String channel;
	private String role;
	
	private String code;//驗證碼
	private String memberId;
	private String password;
	private String receiveType;
	private String sessionId;
	private String loginIp;
	private String viewDevice;
	private String birthday;
	private String otpPre;
	private String otpCode;
	private String memberToken;
	private Integer memberLevel; // 0:一般會員  1:認證會員
	private String ssoId;
	private String validKeyNo;
	private String productType;
	private String policyNo;
	private String msg;
	
	//C05 電子強制險證下載
	private String carNo;
	private String customerId;
	
	// A05 查詢保單
	private String applicantOfCustomerName;
	private String customerName;
	
    // A06 查詢保單明細
    private String listCarMain;
    private String memberType;
    private String personalOrTeam;
    private String appliteType;
    private String relNo;

	private String cmd;
	
    private String waitCount; // 請求客戶提供資料重覆輸入次數
    
    //T01 查詢商品保障內容
    private String productCode;
    private String productSubCode;
    private String codeL1;
    private String codeL2;
    private String codeL3;
    private String insureItemId;
    private String productPolicyCode;
    private String categoryCode;
    private String productSubId;
	private String conflictType; //衝突項目
	private String insType; //險種: 1:車險 2:旅綜險 3:健傷險 4:其他

    //T03 保單變更-檢核與試算保費

    private String phone;
    private String email;
    private String contractNo;
    private String insrnceDay;
    private String startDateTime;
    private String endDateTime;
    private String newStartDateTime;
    private String newEndDateTime;
    
    //T04 保單變更-信用卡
    private String uplAppllteNo;
    private String discountPremium;
    private String discountPremiumChange;
    private String creditCardNo1;
    private String creditCardNo2;
    private String creditCardNo3;
    private String creditCardNo4;
    private String creditCardY;
    private String creditCardM;
    private String creditCardCvc;
    private String intentData;

    //熱門問題
	private Integer questionType;//1.投保 2.理賠 3.道路救援
	
	//日曆ViewData
	private String title;
	private String dateStart;
	private String dateEnd;
	private String dateTime;
	private String place;
	
	private String fallback;
	private String checkDate;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDateStart() {
		return dateStart;
	}

	public void setDateStart(String dateStart) {
		this.dateStart = dateStart;
	}

	public String getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(String dateEnd) {
		this.dateEnd = dateEnd;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIntentData() {
		return intentData;
	}

	public void setIntentData(String intentData) {
		this.intentData = intentData;
	}

	public String getUplAppllteNo() {
		return uplAppllteNo;
	}

	public void setUplAppllteNo(String uplAppllteNo) {
		this.uplAppllteNo = uplAppllteNo;
	}

	public String getDiscountPremium() {
		return discountPremium;
	}

	public void setDiscountPremium(String discountPremium) {
		this.discountPremium = discountPremium;
	}

	public String getDiscountPremiumChange() {
		return discountPremiumChange;
	}

	public void setDiscountPremiumChange(String discountPremiumChange) {
		this.discountPremiumChange = discountPremiumChange;
	}

	public String getCreditCardNo1() {
		return creditCardNo1;
	}

	public void setCreditCardNo1(String creditCardNo1) {
		this.creditCardNo1 = creditCardNo1;
	}

	public String getCreditCardNo2() {
		return creditCardNo2;
	}

	public void setCreditCardNo2(String creditCardNo2) {
		this.creditCardNo2 = creditCardNo2;
	}

	public String getCreditCardNo3() {
		return creditCardNo3;
	}

	public void setCreditCardNo3(String creditCardNo3) {
		this.creditCardNo3 = creditCardNo3;
	}

	public String getCreditCardNo4() {
		return creditCardNo4;
	}

	public void setCreditCardNo4(String creditCardNo4) {
		this.creditCardNo4 = creditCardNo4;
	}

	public String getCreditCardY() {
		return creditCardY;
	}

	public void setCreditCardY(String creditCardY) {
		this.creditCardY = creditCardY;
	}

	public String getCreditCardM() {
		return creditCardM;
	}

	public void setCreditCardM(String creditCardM) {
		this.creditCardM = creditCardM;
	}

	public String getCreditCardCvc() {
		return creditCardCvc;
	}

	public void setCreditCardCvc(String creditCardCvc) {
		this.creditCardCvc = creditCardCvc;
	}

	public String getContractNo() {
		return contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public String getInsrnceDay() {
		return insrnceDay;
	}

	public void setInsrnceDay(String insrnceDay) {
		this.insrnceDay = insrnceDay;
	}

	public String getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(String startDateTime) {
		this.startDateTime = startDateTime;
	}

	public String getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(String endDateTime) {
		this.endDateTime = endDateTime;
	}

	public String getNewStartDateTime() {
		return newStartDateTime;
	}

	public void setNewStartDateTime(String newStartDateTime) {
		this.newStartDateTime = newStartDateTime;
	}

	public String getNewEndDateTime() {
		return newEndDateTime;
	}

	public void setNewEndDateTime(String newEndDateTime) {
		this.newEndDateTime = newEndDateTime;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getReceiveType() {
		return receiveType;
	}

	public void setReceiveType(String receiveType) {
		this.receiveType = receiveType;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getLoginIp() {
		return loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

	public String getViewDevice() {
		return viewDevice;
	}

	public void setViewDevice(String viewDevice) {
		this.viewDevice = viewDevice;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}	

	public String getOtpPre() {
		return otpPre;
	}

	public void setOtpPre(String otpPre) {
		this.otpPre = otpPre;
	}

	public String getOtpCode() {
		return otpCode;
	}

	public void setOtpCode(String otpCode) {
		this.otpCode = otpCode;
	}

	public String getMemberToken() {
		return memberToken;
	}

	public void setMemberToken(String memberToken) {
		this.memberToken = memberToken;
	}

	public Integer getMemberLevel() {
		return memberLevel;
	}

	public void setMemberLevel(Integer memberLevel) {
		this.memberLevel = memberLevel;
	}

	public String getSsoId() {
		return ssoId;
	}

	public void setSsoId(String ssoId) {
		this.ssoId = ssoId;
	}

	public String getValidKeyNo() {
		return validKeyNo;
	}

	public void setValidKeyNo(String validKeyNo) {
		this.validKeyNo = validKeyNo;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getCmd() {
		return cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public String getCarNo() {
		return carNo;
	}

	public void setCarNo(String carNo) {
		this.carNo = carNo;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
		
    public String getWaitCount() {
        return waitCount;
    }

    public void setWaitCount(String waitCount) {
        this.waitCount = waitCount;
    }

    public String getListCarMain() {
        return listCarMain;
    }

    public void setListCarMain(String listCarMain) {
        this.listCarMain = listCarMain;
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    public String getPersonalOrTeam() {
        return personalOrTeam;
    }

    public void setPersonalOrTeam(String personalOrTeam) {
        this.personalOrTeam = personalOrTeam;
    }

    public String getAppliteType() {
        return appliteType;
    }

    public void setAppliteType(String appliteType) {
        this.appliteType = appliteType;
    }

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductSubCode() {
		return productSubCode;
	}

	public void setProductSubCode(String productSubCode) {
		this.productSubCode = productSubCode;
	}

	public String getCodeL1() {
		return codeL1;
	}

	public void setCodeL1(String codeL1) {
		this.codeL1 = codeL1;
	}

	public String getCodeL2() {
		return codeL2;
	}

	public void setCodeL2(String codeL2) {
		this.codeL2 = codeL2;
	}

	public String getCodeL3() {
		return codeL3;
	}

	public void setCodeL3(String codeL3) {
		this.codeL3 = codeL3;
	}

	public String getInsureItemId() {
		return insureItemId;
	}

	public void setInsureItemId(String insureItemId) {
		this.insureItemId = insureItemId;
	}

	public String getProductPolicyCode() {
		return productPolicyCode;
	}

	public void setProductPolicyCode(String productPolicyCode) {
		this.productPolicyCode = productPolicyCode;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getProductSubId() {
		return productSubId;
	}

	public void setProductSubId(String productSubId) {
		this.productSubId = productSubId;
	}

	public String getApplicantOfCustomerName() {
		return applicantOfCustomerName;
	}

	public void setApplicantOfCustomerName(String applicantOfCustomerName) {
		this.applicantOfCustomerName = applicantOfCustomerName;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Integer getQuestionType() {
		return questionType;
	}

	public void setQuestionType(Integer questionType) {
		this.questionType = questionType;
	}

	public String getRelNo() {
		return relNo;
	}

	public void setRelNo(String relNo) {
		this.relNo = relNo;
	}

	public String getConflictType() {
		return conflictType;
	}

	public void setConflictType(String conflictType) {
		this.conflictType = conflictType;
	}

	public String getInsType() {
		return insType;
	}

	public void setInsType(String insType) {
		this.insType = insType;
	}

	public String getFallback() {
		return fallback;
	}

	public void setFallback(String fallback) {
		this.fallback = fallback;
	}

	public String getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}
}



