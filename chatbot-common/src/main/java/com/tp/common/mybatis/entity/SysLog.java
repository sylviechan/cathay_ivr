package com.tp.common.mybatis.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author matt
 * @since 2019-09-18
 */
public class SysLog extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    private String sessionId;

    private String method;

    private String params;

    private String result;

    private String remark;

    private Long costTime;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    public Integer getId() {
        return id;
    }

    public SysLog setId(Integer id) {
        this.id = id;
        return this;
    }
    public String getSessionId() {
        return sessionId;
    }

    public SysLog setSessionId(String sessionId) {
        this.sessionId = sessionId;
        return this;
    }
    public String getMethod() {
        return method;
    }

    public SysLog setMethod(String method) {
        this.method = method;
        return this;
    }

    public String getParams() {
        return params;
    }

    public SysLog setParams(String params) {
        this.params = params;
        return this;
    }

    public String getResult() {
        return result;
    }

    public SysLog setResult(String result) {
        this.result = result;
        return this;
    }

    public String getRemark() {
        return remark;
    }

    public SysLog setRemark(String remark) {
        this.remark = remark;
        return this;
    }

    public Long getCostTime() {
        return costTime;
    }

    public SysLog setCostTime(Long costTime) {
        this.costTime = costTime;
        return this;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public SysLog setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
        return this;
    }
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public SysLog setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    @Override
    public String toString() {
        return "SysLog{" +
        "id=" + id +
        ", sessionId=" + sessionId +
        ", method=" + method +
        ", costTime=" + costTime +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
