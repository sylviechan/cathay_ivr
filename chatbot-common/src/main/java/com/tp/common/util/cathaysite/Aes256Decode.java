package com.tp.common.util.cathaysite;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class Aes256Decode {

	private final static String cipherMsg = "AES/ECB/PKCS5Padding";
	private final static String encodeRules = "70827406";
	
	/*
	 * MD5訊息摘要演算法（英語：MD5 Message-Digest Algorithm）
	 * ，一種被廣泛使用的密碼雜湊函式
	 * ，可以產生出一個128位元（16位元組）的雜湊值（hash value）
	 */
    public static String GetMD5(String algorithm,String encodeRules){
    	String md5Value = null;
		try {
			// 生成一個MD5加密計算摘要
			MessageDigest md = MessageDigest.getInstance(algorithm);
			// 計算MD5函數
			md.update(encodeRules.getBytes());
			// digest()最後確定返回MD5 hash值，返回值為8位字符串。因為MD5 hash值是16位的hex值，實際上就是8位的字符
			// BigInteger函數則將8位的字符串轉換成16位hex值，用字符串來表示；得到字符串形式的hash值
			md5Value = new BigInteger(1, md.digest()).toString(16);
		} catch (Exception e) {
			//throw new SpeedException("MD5加密出現錯誤");
			e.printStackTrace();
		}
		return md5Value;
	}
    
    public static String decode(String content){
    	String encodeRulesMD5 = GetMD5("MD5",encodeRules);
		byte[] keyArray = encodeRulesMD5.getBytes(StandardCharsets.UTF_8);
        try {
            //1.根據字串(encodeRulesMD5)byte Array生成AES密鑰
            SecretKey key=new SecretKeySpec(keyArray, "AES");
            //2.根據指定算法AES自成密碼器
            Cipher cipher=Cipher.getInstance(cipherMsg);//"算法/加密/填充"
            // 等同 Cipher cipher=Cipher.getInstance("AES");  
            //3.初始化密碼器，第一個參數為加密(Encrypt_mode)或者解密(Decrypt_mode)操作，第二個參數為使用的KEY
            cipher.init(Cipher.DECRYPT_MODE, key);
            //4.將解密並編碼後的内容解析成字串byte Array
            byte [] byte_content = Base64.decodeBase64(content);
         	//5.解密
            byte [] byte_decode=cipher.doFinal(byte_content);
            String decode=new String(byte_decode,"utf-8");
            return decode;
        } catch (Exception e) {
            e.printStackTrace();
        } 
        return null;
    }
}
