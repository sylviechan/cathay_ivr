package com.tp.common.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotMessageVO.CImageDataItem;
import com.tp.common.model.BotMessageVO.CImageTextItem;
import com.tp.common.model.BotMessageVO.CLinkListItem;
import com.tp.common.model.BotMessageVO.CTextItem;
import com.tp.common.model.BotMessageVO.CardItem;
import com.tp.common.model.BotMessageVO.ContentItem;
import com.tp.common.model.cathayins.InsProductTellRequest;
import com.tp.common.model.DemandProductionCardItem;
import com.tp.common.model.TPIGatewayResult;

/**
 * 客製化牌卡快捷功能
 * 
 * @author sakin
 */
@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
@Component
public class DemandProductionCardUtis {

	public void setCard(TPIGatewayResult tpiGatewayResult, BotMessageVO botMessageVO, DemandProductionCardItem item) {

		// 初始分組
		ContentItem content = botMessageVO.new ContentItem();
		content.setType(30);

		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		// 牌卡
		CardItem cards = botMessageVO.new CardItem();
		List<Map<String, Object>> cTextsList = new ArrayList<>();

		// 組牌卡-------

		// 圖
		CImageDataItem cImageData = botMessageVO.new CImageDataItem();
		CImageTextItem cImageTextItem = botMessageVO.new CImageTextItem();
		cImageTextItem.setCImageTextTitle("title");
		cTextsList.add(cImageTextItem.getCImageTexts());
		cImageData.setCImageTexts(cTextsList);
		cImageData.setCImageUrl("card35-NoTypeface.png");
		cImageData.setCImageTextType(2);

		cards.setCImageData(cImageData.getCImageDatas());// 組裝起來
		// 中間
		List<Map<String, Object>> cTexts = new ArrayList<Map<String, Object>>();

		cards.setCTextType("1");
		cards.setCWidth(botMessageVO.CWIDTH_200);

		CTextItem cText = botMessageVO.new CTextItem();

		for (Map<String, Object> map : item.getcTextItemListMap()) {
			cText = botMessageVO.new CTextItem();
			if (map.containsKey(item.getcTextItemMapTitleKey())) {
				cText.setCLabel((String) map.get(item.getcTextItemMapTitleKey()));

			}
			if (map.containsKey(item.getcTextItemMapValKey())) {
				cText.setCText((String) map.get(item.getcTextItemMapValKey()));

			}
			cTexts.add(cText.getCTexts());
			cards.setCTexts(cTexts);// 組裝起來
		}

		// 底下按鈕
		List<Map<String, Object>> cLinkContents = new ArrayList<Map<String, Object>>();

		List msgList = new ArrayList<>();
		List optionList = new ArrayList<>();
		Boolean isSetParameter = false;

		for (QuickReplay quickReplay : item.getQuickReplayList()) {
			cLinkContents.add(getCLinkListItem(botMessageVO, quickReplay).getCLinkList());
			if (quickReplay.getParameter() != null && quickReplay.getParameter().size() > 0) {
				isSetParameter = true;
				int action = quickReplay.getAction();

				if (action == 15) {
					msgList.add(quickReplay.getReply());
					optionList.add(item.getQuickReplayList());
				} else {
					msgList.add(quickReplay.getAlt());
					optionList.add(item.getQuickReplayList());
				}
			}

		}

		// 這一段要儲存按鈕資訊給flow
		if (isSetParameter) {
			tpiGatewayResult.setMsgList(msgList);
			tpiGatewayResult.setOptionList(optionList);
		}
		// --

		cards.setCLinkList(cLinkContents);// 組裝起來

		cardList.add(cards.getCards());

		content.setCards(cardList);

		botMessageVO.getContent().add(content.getContent());

	}

	/**
	 * 牌卡圖片正中間綠色文字
	 * 
	 * @param tpiGatewayResult
	 * @param botMessageVO
	 * @param item
	 */
	private void setCardType1(TPIGatewayResult tpiGatewayResult, BotMessageVO botMessageVO,
			DemandProductionCardItem item) {
		item.setcImageTextType(1);
		setCard(tpiGatewayResult, botMessageVO, item);
	}

	/**
	 * 牌卡圖片正上方白色文字
	 * 
	 * @param tpiGatewayResult
	 * @param botMessageVO
	 * @param item
	 */
	private void setCardType2(TPIGatewayResult tpiGatewayResult, BotMessageVO botMessageVO,
			DemandProductionCardItem item) {
		item.setcImageTextType(2);
		setCard(tpiGatewayResult, botMessageVO, item);
	}

	/**
	 * 牌卡(一圖一按鈕)
	 * 
	 * @param tpiGatewayResult
	 * @param vo
	 * @param ImageUrls
	 * @param quickReplays
	 * @return
	 */
	private List<Map<String, Object>> setCardButton(TPIGatewayResult tpiGatewayResult, BotMessageVO vo,
			String[] ImageUrls, QuickReplay[] quickReplays) {
		List<Map<String, Object>> cardList = new ArrayList<>();
		List<String> msgList = new ArrayList<String>();
		List<QuickReplay> optionList = new ArrayList<QuickReplay>();

		for (int i = 0; i < ImageUrls.length; i++) {
			// card1
			CardItem cards = vo.new CardItem();
			cards.setCName("");
			cards.setCWidth(BotMessageVO.CWIDTH_200);

			CImageDataItem cImageData = vo.new CImageDataItem();
			cImageData.setCImage(BotMessageVO.IMG_CC);
			cImageData.setCImageUrl(ImageUrls[i]);
			cards.setCImageData(cImageData.getCImageDatas());
			List<Map<String, Object>> quickReplayList = new ArrayList<Map<String, Object>>();
			quickReplayList.add(getCLinkListItem(vo, quickReplays[i]).getCLinkList());
			cards.setCLinkList(quickReplayList);

			msgList.add(quickReplays[i].getText());
			optionList.add(quickReplays[i]);
			cardList.add(cards.getCards());
		}

		tpiGatewayResult.setMsgList(msgList);
		tpiGatewayResult.setOptionList(optionList);
		return cardList;
	}
	
	/**
	 * 產險
	 * 商品小類-牌卡
	 * @param tpiGatewayResult
	 * @param vo
	 * @param datas
	 * @param detailTest (牌卡cImageData資料)
	 * @return
	 */
	public List<Map<String, Object>> getCommoditySmallSortCard(TPIGatewayResult tpiGatewayResult, BotMessageVO vo,
			List<Map<String, Object>> datas, String[] detailTest, String intent, String secondBotIntent) {
		List<Map<String, Object>> cardList = new ArrayList<>();
		List<String> msgList = new ArrayList<String>();
		List<QuickReplay> optionList = new ArrayList<QuickReplay>();

		for (int i = 0; i < detailTest.length; i++) {
			// card1
			CardItem cards = vo.new CardItem();
			cards.setCName("");
			cards.setCWidth(BotMessageVO.CWIDTH_200);

			CImageDataItem cImageData = vo.new CImageDataItem();
			cImageData.setCImage(BotMessageVO.IMG_CC);

			cImageData.setCImageTextType(1);
			List<Map<String, Object>> cImageTextList = new ArrayList<Map<String, Object>>();

			CImageTextItem cImageText = vo.new CImageTextItem();

			String title = "";
			String text = "";
			Map<String, Object> data = datas.get(i);

			for (String test : detailTest) {
				String[] tests = test.split(":");
				if (StringUtils.equals(tests[0], data.get("CODE_NAME_L3").toString())) {
					if(StringUtils.equals(tests[0], "旅平險")) {
						title = "旅遊平安險";
					} else {
						title = tests[0];
					}
					
					text = tests[1];
				}
			}

			cImageText.setCImageTextTitle(title);
			cImageText.setCImageText(text);
			cImageTextList.add(cImageText.getCImageTexts());
			cImageData.setCImageTexts(cImageTextList);

			cards.setCImageData(cImageData.getCImageDatas());

			String commodityType = data.get("PRODUCT_SUB_CODE") + "_" + data.get("CODE_ID_L3");

			Map<String, Object> parameter = new HashMap<String, Object>();
			parameter.put("commodityType", commodityType);

			List<Map<String, Object>> quickReplayList = new ArrayList<Map<String, Object>>();
			QuickReplay quickReplay = new QuickReplayItem(1, title,
					title, intent, secondBotIntent, parameter);

			quickReplayList.add(getCLinkListItem(vo, quickReplay).getCLinkList());
			cards.setCLinkList(quickReplayList);

			msgList.add(data.get("CODE_NAME_L3").toString());
			optionList.add(quickReplay);
			cardList.add(cards.getCards());
		}

		tpiGatewayResult.setMsgList(msgList);
		tpiGatewayResult.setOptionList(optionList);
		return cardList;
	}

	/**
	 * 產險
	 * 商品小類:項目內容-牌卡
	 * @param vo
	 * @param reqVO
	 * @param datas
	 * @param msgList
	 * @param optionList
	 * @param intent 
	 * @param secondBotIntent 
	 * @return
	 */
	public List<Map<String, Object>> projectContentCard(BotMessageVO vo, InsProductTellRequest reqVO,
			List<Map<String, Object>> datas, List<String> msgList, List<QuickReplay> optionList, String intent,
			String secondBotIntent) {
		List<Map<String, Object>> cardList = new ArrayList<>();

		CardItem cards = vo.new CardItem();
		cards.setCName("");
		cards.setCWidth(BotMessageVO.CWIDTH_250);
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB);
		cImageData.setCImageTextType(1);

		List<Map<String, Object>> cImageTextList = new ArrayList<Map<String, Object>>();
		CImageTextItem cImageText = vo.new CImageTextItem();

		Map<String, Object> map = datas.get(0);
		
		String imageTitle = map.get("CODE_NAME_L1").toString() + "-" + map.get("CODE_NAME_L2").toString() + "-"
				+ map.get("CODE_NAME_L3").toString() + "-" + map.get("INSURE_ITEM").toString();
		
		// 2019/12/03 如果是B類或C類，只要秀一次CODE_NAME就好
		if (StringUtils.equals(map.get("CODE_ID_L1").toString(), "B") || StringUtils.equals(map.get("CODE_ID_L1").toString(), "C")) {
			imageTitle = map.get("CODE_NAME_L1").toString() + "-" + map.get("INSURE_ITEM").toString();
		}

		cImageText.setCImageTextTitle(imageTitle);
		cImageTextList.add(cImageText.getCImageTexts());
		cImageData.setCImageTexts(cImageTextList);
		cards.setCImageData(cImageData.getCImageDatas());

		List<Map<String, Object>> cLinkContentList = new ArrayList<>();
		String[] keys = { "IS", "CIA", "NII", "NIM", "EL", "NC", "PD" };
		for (int i = 0; i < keys.length; i++) {
			// card1
			Map<String, Object> data = datas.get(0);
			String commodityType = data.get("PRODUCT_SUB_CODE") + "_" + data.get("CODE_ID_L3") + "_"
					+ data.get("INSURE_ITEM_ID") + "_" + keys[i];
			reqVO.getProjectDescription(keys[i]);

			Map<String, Object> parameter = new HashMap<String, Object>();
			parameter.put("commodityType", commodityType);
			if (StringUtils.isNotBlank(data.get(reqVO.getCommodityClick()).toString())) {
				QuickReplay quickReplay = new QuickReplayItem(1, reqVO.getCommodityClickName(), reqVO.getCommodityClickName(), intent, secondBotIntent, parameter);
				cLinkContentList.add(getCLinkListItem(vo, quickReplay).getCLinkList());

				msgList.add(reqVO.getCommodityClickName());
				optionList.add(quickReplay);
			}
			
		}
		cards.setCLinkList(cLinkContentList);
		cardList.add(cards.getCards());

		return cardList;
	}

	public CLinkListItem getCLinkListItem(BotMessageVO vo, QuickReplay quickReplay) {
		CLinkListItem cLinkContent = vo.new CLinkListItem();
		cLinkContent.setClAction(quickReplay.getAction());
		cLinkContent.setClText(quickReplay.getText());
		cLinkContent.setClAlt(quickReplay.getAlt());
		cLinkContent.setClReply(quickReplay.getReply());
		cLinkContent.setClUrl(quickReplay.getUrl());
		cLinkContent.setClIntent(quickReplay.getIntent());
		cLinkContent.setClSecondBotIntent(quickReplay.getSecondBotIntent());
		cLinkContent.setClParameter(quickReplay.getParameter());
		return cLinkContent;
	}

	class QuickReplayItem extends QuickReplay {
		public QuickReplayItem(int action, String text, String alt) {
			super(action, text, alt);
		}

		public QuickReplayItem(int action, String text, String alt, String reply, String url) {
			super(action, text, alt, reply, url);
		}

		public QuickReplayItem(int action, String text, String alt, String intent, String secondBotIntent,
				Map<String, Object> parameter) {
			super(action, text, alt, intent, secondBotIntent, parameter);
		}

		public QuickReplayItem(int action, String text, String alt, String reply, String url, String customerAction) {
			super(action, text, alt, reply, url, customerAction);
		}

		public QuickReplayItem(int action, String text, String alt, String reply, String url, String customerAction,
				String intent, String secondBotIntent, Map<String, Object> parameter) {
			super(action, text, alt, reply, url, customerAction, intent, secondBotIntent, parameter);
		}

	}
}
