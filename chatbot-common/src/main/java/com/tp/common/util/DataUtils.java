package com.tp.common.util;

import com.tp.common.model.BotRequest;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.beans.FeatureDescriptor;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

public class DataUtils {

	private static final Logger log = LoggerFactory.getLogger(DataUtils.class);

	public static String convertToCardDateStyle(String s) {
		return s == null ? "" : s.replaceAll("-", "/");
	}

	public static BotRequest getBaseBotRequest(String sessionId, String msg) {
		BotRequest botRequest = new BotRequest();
		botRequest.setChannel("ChatWeb");
		botRequest.setRole("alpha");
		botRequest.setSessionId(sessionId);
		botRequest.setMsg(msg);
		return botRequest;
	}

	/*
	 * 1.去掉小數點後面多餘的零 2.將數字格式化為千分位表示法。
	 */
	public static String handleMoney(String s) {
		if (s == null)
			return "";

		DecimalFormat decimalFormat = new DecimalFormat(",###");

		if (s.indexOf(".") > 0) {
			s = s.replaceAll("0+?$", ""); // 去掉多餘的零
			s = s.replaceAll("[.]$", ""); // 如最後一位是.則去掉
		}

		String intPart = "";
		String dotPart = "";
		intPart = s.split("\\.")[0];
		if (s.indexOf(".") > 0) {
			dotPart = "." + s.split("\\.")[1];
		}

		s = decimalFormat.format(Double.parseDouble(intPart)) + dotPart;

		return s;
	}

	/*
	 * 1.留小數點後2位 2.將數字格式化為千分位表示法。
	 */
	public static String handleMoney2(String s) {
		if (s == null)
			return "0.00";

		DecimalFormat decimalFormat = new DecimalFormat(",##0.00");
		s = decimalFormat.format(Double.parseDouble(s));
		if (StringUtils.equals(s, ".00")) {
			s = "0.00";
		}
		return s;
	}

	public static String[] getNullPropertyNames(Object source) {
		final BeanWrapper wrappedSource = new BeanWrapperImpl(source);
		return Stream.of(wrappedSource.getPropertyDescriptors()).map(FeatureDescriptor::getName)
				.filter(propertyName -> wrappedSource.getPropertyValue(propertyName) == null).toArray(String[]::new);
	}

	public static void main(String[] args) {
		String s = "00";
		System.out.println(DataUtils.handleMoney2(s));
	}

	public static String getUUID() {
		return UUID.randomUUID().toString();
	}

	public static String genMsgId(String sessionId, String textMessage) {
		return "" + Math.abs((System.currentTimeMillis() + sessionId + textMessage).hashCode());
	}

	public static String getCookieId(HttpServletRequest request) {
		String cookideId = "";
		try {
			for (Cookie cookie : request.getCookies()) {
				if (StringUtils.equals(cookie.getName(), "cubcsapersisted")) {
					String value = cookie.getValue();
					String[] values = value.split("_");
					if (values.length >= 2) {
						cookideId = values[2] == null ? "" : values[2];
					}
				}
			}
		} catch (Exception ex) {
			log.error("ex: " + ex);
		}

		return cookideId;
	}

	/**
	 * 統一整理ssoId
	 * 
	 * @param ssoId
	 * @return String
	 */
	public static String getRectifySSOId(String ssoId) {
		if (StringUtils.isNotBlank(ssoId)) {
			return "?uniquePageNo=" + ssoId;
		} else {
			return "";
		}
	}
	
	/**
	 * 統一整理ssoId 當網址有已經1個或以上的參數的時候用這一支
	 * 
	 * @param ssoId
	 * @return String
	 */
	public static String getRectifySSOId2(String ssoId) {
		if (StringUtils.isNotBlank(ssoId)) {
			return "&uniquePageNo=" + ssoId;
		} else {
			return "";
		}
	}
	
	/**
	 *
	 * @return String
	 */
	public static String getRegFrom() {
		return "?regFrom=Alpha";
	}

	public static Map<String, Object> abnormalOrder(Map req, Map resp) {

		Date nowDateTime = new Date();
		Map<String, Object> rtn = new HashMap<String, Object>();
		String functionID = "BR29";
		String id = "";
		String channel = "";
		String udid = "";
		String logDate = new SimpleDateFormat("yyyy-MM-dd").format(nowDateTime);
		String logTime = new SimpleDateFormat("HH:mm:ss").format(nowDateTime);
		String errCode = "";
		String msg = "";
		String apCode = "";

		if (resp.containsKey("ResultCode")) {
			errCode = MapUtils.getString(resp, "ResultCode");
		}

		if (resp.containsKey("ResultMessage")) {
			msg = MapUtils.getString(resp, "ResultMessage");
		}

		if (req.containsKey("Channel")) {
			channel = MapUtils.getString(req, "Channel");
		}

		if (req.containsKey("FunctionID")) {
			apCode = MapUtils.getString(req, "FunctionID");
		} else if (resp.containsKey("FunctionID")) {
			apCode = MapUtils.getString(resp, "FunctionID");
		}

		if (req.containsKey("ID")) {
			id = MapUtils.getString(req, "ID");
		} else if (resp.containsKey("ID")) {
			id = MapUtils.getString(resp, "ID");
		}

		if (req.containsKey("UDID")) {
			udid = MapUtils.getString(req, "UDID");
		} else if (resp.containsKey("UDID")) {
			udid = MapUtils.getString(resp, "UDID");
		}

		rtn.put("FunctionID", functionID);
		rtn.put("ID", id);
		rtn.put("Channel", channel);
		rtn.put("UDID", udid);
		rtn.put("LogDate", logDate);
		rtn.put("LogTime", logTime);
		rtn.put("ErrCode", errCode);
		rtn.put("Msg", msg);
		rtn.put("APCode", apCode);

		return rtn;
	}

}
