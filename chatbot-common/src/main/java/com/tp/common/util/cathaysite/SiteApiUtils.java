package com.tp.common.util.cathaysite;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SiteApiUtils {

    public static Map<String, String> returnCodeData;

    static {
        returnCodeData = new HashMap<>();
        returnCodeData.put("E001", "資料庫存取異常");
        returnCodeData.put("E002", "存取權限異常");
        returnCodeData.put("E003", "登入逾時");
        returnCodeData.put("E004", "權限不足無法使用");
        returnCodeData.put("E005", "重覆登入本交易系統");
        returnCodeData.put("E006", "系統異常");
        returnCodeData.put("E007", "IP資料異常");
        returnCodeData.put("S001", "成功");
        returnCodeData.put("W001", "NULL");
        returnCodeData.put("W002", "無任何資料");
        returnCodeData.put("W003", "帳號鎖定");
        returnCodeData.put("W004", "密碼錯誤");
        returnCodeData.put("W005", "超過申購截止時間");
    }

    public static String getDescByCode(String code) {
        return returnCodeData.get(code);
    }

    public static boolean isSuccess(Map<String, Object> result) {
        return StringUtils.equals("S001", getCode(result));
    }
    
    public static boolean isPwdError(Map<String, Object> result) {
        return StringUtils.equals("W004", getCode(result));
    }
    
    public static boolean isOverTime(Map<String, Object> result) {
        return StringUtils.equals("W005", getCode(result));
    }

    public static String getCode(Map<String, Object> data) {
        return String.valueOf(data.get("code"));
    }

    public static String getMsg(Map<String, Object> data) {
        return String.valueOf(data.get("msg"));
    }

    public static Map<String, Object> getDataValue(Map<String, Object> data) {
        return (Map<String, Object>) data.get("DataValue");
    }

    public static List<Map<String, Object>> getList(Map<String, Object> data) {
        Map<String, Object> DataValue = (Map<String, Object>) data.get("DataValue");
        String jsonStr = (String) DataValue.get("list");
        List list = JSON.parseObject(jsonStr, List.class);
        return list;
    }
    
    public static List<Map<String, Object>> getListPur(Map<String, Object> data) {
        Map<String, Object> DataValue = (Map<String, Object>) data.get("DataValue");
        String jsonStr = (String) DataValue.get("list_pur");
        List list = JSON.parseObject(jsonStr, List.class);
        return list;
    }
    
    public static List<Map<String, Object>> getListRede(Map<String, Object> data) {
        Map<String, Object> DataValue = (Map<String, Object>) data.get("DataValue");
        String jsonStr = (String) DataValue.get("list_rede");
        List list = JSON.parseObject(jsonStr, List.class);
        return list;
    }
    
    public static List<Map<String, Object>> getListRsp(Map<String, Object> data) {
        Map<String, Object> DataValue = (Map<String, Object>) data.get("DataValue");
        String jsonStr = (String) DataValue.get("list_rsp");
        List list = JSON.parseObject(jsonStr, List.class);
        return list;
    }
    
    public static Map<String, Object> getDetail(Map<String, Object> data) {
        Map<String, Object> DataValue = (Map<String, Object>) data.get("DataValue");
        String jsonStr = (String) DataValue.get("detail");        
        return JSON.parseObject(jsonStr, Map.class);
    }

	public static Map<String, Object> getDataMap(Map<String, Object> data) {
		Map<String, Object> DataValue = (Map<String, Object>) data.get("DataValue");
		Object dataObj = DataValue.get("data");
		if (dataObj instanceof String) {
			Map<String, Object> dataMap = JSON.parseObject((String) dataObj, new TypeReference<Map<String, Object>>() {
			});
			return dataMap;
		}
		Map<String, Object> dataMap = (Map) dataObj;
		return dataMap;
	}
    
    public static Map<String, Object> getData(Map<String, Object> result) {
        String dataStr = (String) result.get("d");
        return JSON.parseObject(dataStr, Map.class);
    }

}
