package com.tp.common.util;

import java.util.Map;

abstract public class QuickReplay {

    private final int action;
    private final String text;
    private final String alt;
    private final String reply;
    private final String url;
    private final String intent;
    private final String secondBotIntent;
    private final String customerAction;
    private final Map<String, Object> parameter;

    public QuickReplay(int action, String text, String alt, String reply, String url, String customerAction) {
        super();
        this.action = action;
        this.text = text;
        this.alt = alt;
        this.reply = reply;
        this.url = url;
        this.customerAction = customerAction;
        this.intent = "";
        this.secondBotIntent = "";
        this.parameter = null;
    }

    public QuickReplay(int action, String text, String alt, String reply, String url, String customerAction, String intent, String secondBotIntent, Map<String, Object> parameter) {
        super();
        this.action = action;
        this.text = text;
        this.alt = alt;
        this.reply = reply;
        this.url = url;
        this.customerAction = customerAction;
        this.intent = intent;
        this.secondBotIntent = secondBotIntent;
        this.parameter = parameter;
    }

    public QuickReplay(int action, String text, String alt, String intent, String secondBotIntent, Map<String, Object> parameter) {
        super();
        this.action = action;
        this.text = text;
        this.alt = alt;
        this.reply = "";
        this.url = "";
        this.customerAction = "";
        this.intent = intent;
        this.secondBotIntent = secondBotIntent;
        this.parameter = parameter;
    }

    public QuickReplay(int action, String text, String alt, String replay, String intent, String secondBotIntent, Map<String, Object> parameter) {
        super();
        this.action = action;
        this.text = text;
        this.alt = alt;
        this.reply = replay;
        this.url = "";
        this.customerAction = "";
        this.intent = intent;
        this.secondBotIntent = secondBotIntent;
        this.parameter = parameter;
    }

    public QuickReplay(int action, String text, String alt) {
        super();
        this.action = action;
        this.text = text;
        this.alt = alt;
        this.reply = "";
        this.url = "";
        this.customerAction = "";
        this.intent = "";
        this.secondBotIntent = "";
        this.parameter = null;
    }

    public QuickReplay(int action, String text, String alt, String reply, String url) {
        super();
        this.action = action;
        this.text = text;
        this.alt = alt;
        this.reply = reply;
        this.url = url;
        this.customerAction = "";
        this.intent = "";
        this.secondBotIntent = "";
        this.parameter = null;
    }

    public int getAction() {
        return action;
    }

    public String getText() {
        return text;
    }

    public String getAlt() {
        return alt;
    }

    public String getReply() {
        return reply;
    }

    public String getUrl() {
        return url;
    }

    public String getCustomerAction() {
        return customerAction;
    }

    public String getIntent() {
        return intent;
    }

    public String getSecondBotIntent() {
        return secondBotIntent;
    }

    public Map<String, Object> getParameter() {
        return parameter;
    }
}