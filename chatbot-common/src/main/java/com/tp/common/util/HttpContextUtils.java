package com.tp.common.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

public class HttpContextUtils {

    private static Logger logger = LoggerFactory.getLogger(HttpContextUtils.class);

    public static HttpServletRequest getHttpServletRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

    public static String getSessionId() {
        HttpServletRequest request = getHttpServletRequest();

        String sessionId = request.getParameter("sessionId");

        if (StringUtils.isEmpty(sessionId)) {
            try {
                String body = IOUtils.toString(request.getReader());
                JSONObject jsonObject = JSON.parseObject(body);
                sessionId = (String) jsonObject.get("sessionId");
            } catch (Exception e) {
                //logger.error(e.getMessage());
            }
        }

        return sessionId;
    }
}
