package com.tp.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.codec.Hex;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.FileImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;

public class ImageUtils {

	private static final Logger log = LoggerFactory.getLogger(ImageUtils.class);

	public static int getImageWidth(File sourceFile) throws IOException {
		return ImageIO.read(sourceFile).getWidth();
	}

	public static int getScaleHeight(File sourceFile, int width) throws IOException {
		if (sourceFile == null)
			return 0;
		return getScaleHeight(ImageIO.read(sourceFile), width);
	}

	public static int[] getScaleHeights(File sourceFile, int... widths) throws IOException {
		if (sourceFile == null)
			return new int[0];
		log.debug("[getScaleHeights] widths size=" + widths.length);
		int[] returnInt = new int[widths.length];
		BufferedImage originalImage = ImageIO.read(sourceFile);
		for (int i = 0; i < widths.length; i++) {
			int width = widths[i];
			returnInt[i] = getScaleHeight(originalImage, width);
		}
		return returnInt;
	}

	public static boolean checkImageWidth(File sourceFile, int width) throws IOException {
		BufferedImage originalImage = ImageIO.read(sourceFile);
		log.debug("==========[checkImageWidth]==========");
		log.debug("originalImage.getWidth()=" + originalImage.getWidth());
		log.debug("width=" + width);
		log.debug("=====================================");
		return originalImage.getWidth() >= width;
	}

	public static boolean checkFileScaleEquals(File sourceFile, int width, int height) throws IOException {
		BufferedImage originalImage = ImageIO.read(sourceFile);
		log.debug("==========[checkFileScaleEquals]==========");
		log.debug("originalImage.getWidth()=" + originalImage.getWidth());
		log.debug("originalImage.getHeight()=" + originalImage.getHeight());
		log.debug("width=" + width);
		log.debug("height=" + height);
		log.debug("==========================================");
		return originalImage.getWidth() == width && originalImage.getHeight() == height;
	}

	public static void resizeImage(File imgFile, File targetFile, int newWidth) throws IOException {
		BufferedImage originalImage = ImageIO.read(imgFile);
		int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();

		BufferedImage resizeImage = reduce(originalImage, type, newWidth);
		// ImageIO.write(resizeImageJpg, "jpg", targetFile);

		if (isCanCompress(imgFile)) {
			log.debug("image type is jpg");
        	JPEGImageWriteParam jpegParams = new JPEGImageWriteParam(null);
        	jpegParams.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        	jpegParams.setCompressionQuality(0.92f);
        	
        	final ImageWriter writer = ImageIO.getImageWritersByFormatName("jpg").next();
        	// specifies where the jpg image has to be written
        	writer.setOutput(new FileImageOutputStream(targetFile));
        	
        	// writes the file with given compression level
        	// from your JPEGImageWriteParam instance
        	writer.write(null, new IIOImage(resizeImage, null, null), jpegParams);
        } else {
        	log.debug("image type is png");
        	String format = "png";
        	ImageIO.write(resizeImage, format, targetFile);
        }
	}

	private static int getScaleHeight(BufferedImage img, int width) {
		int imgW = img.getWidth();
		int imgH = img.getHeight();
		double theScale = (double) imgH / imgW;
		log.debug("[getScaleHeight] theScale=" + theScale);
		int height = new BigDecimal(width * theScale).setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
		log.debug("[getScaleHeight] height=" + height);
		return height;
	}
	
	private static BufferedImage reduce(final BufferedImage original, int type, int w) {
    	
    	int h = getScaleHeight(original, w);
    	
    	final Image rescaled = original.getScaledInstance(w, h,
    			Image.SCALE_AREA_AVERAGING);
    	final BufferedImage result = new BufferedImage(w, h, type);
    	final Graphics2D g = result.createGraphics();
    	g.drawImage(rescaled, 0, 0, null);
    	g.dispose();
    	return result;
    }
	
	private static boolean isCanCompress(File file) throws IOException {

		FileInputStream imgFile = null;
		byte[] b = new byte[10];
		try {
			imgFile = new FileInputStream(file);
			imgFile.read(b, 0, b.length);
			String type = Hex.encode(b).toString().toUpperCase();

			log.debug("type=" + type);
			if (type.contains("FFD8FF")) {
				return true;
			} else if (type.contains("89504E47")) {
				return false;
			} else if (type.contains("47494638")) {
				return false;
			} else if (type.contains("424D")) {
				return true;
			} else {
				return true;
			}
		} catch (IOException e) {
			log.error(e.getMessage());
			return true;
		} finally {
			if (imgFile != null) {
				try {
					imgFile.close();
				} catch (IOException e) {}
			}
		}
	}

	/**
	 * 旋转图片为指定角度
     * @param bufferedimage 目标图像  
	 * @param degree 旋转角度
	 * @return      
	 */
	public static BufferedImage rotateImage(BufferedImage bufferedimage, int degree) {
		int iw = bufferedimage.getWidth(); // 原始图象的宽度    
		int ih = bufferedimage.getHeight();// 原始图象的高度    
		int w = 0;
		int h = 0;
		int x = 0;
		int y = 0;
		degree = degree % 360;
		if (degree < 0)
			degree = 360 + degree;
		// 将角度转换到0-360度之间     /**    
		/* 确定旋转后的图象的高度和宽度     */ 
		if (degree == 180 || degree == 0 || degree == 360) {
			w = iw;
			h = ih;
		} else if (degree == 90 || degree == 270) {
			w = ih;
			h = iw;
		}
		x = (w / 2) - (iw / 2);// 确定原点坐标
		y = (h / 2) - (ih / 2);
		int type = bufferedimage.getColorModel().getTransparency();
		BufferedImage img;
		Graphics2D graphics2d;
		(graphics2d = (img = new BufferedImage(w, h, type)).createGraphics()).setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		graphics2d.rotate(Math.toRadians(degree), w / 2, h / 2);
		graphics2d.translate(x, y);
		graphics2d.drawImage(bufferedimage, 0, 0, null);
		graphics2d.dispose();
		return img;
	}

	
	//for local test
    public static void main(String[] args) {
    	
    	String path = "d:/temp/images/";
		String oriFilename = "登入成功.PNG";
		String videoCode = "DIS-TW-001-A0167"; //影響到產出的資料夾以及檔名
		
		int[] size = {1200,704,544,448,272};
		
		try {
			//for (int i = 0; i < size.length; i++) {
				//int j = size[i];
				resizeImage(new File(path + oriFilename), new File(path + videoCode + "-P.png"), 250);
			//}
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
	}
}
