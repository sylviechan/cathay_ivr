
package com.tp.common.validator;

import com.tp.common.exception.TPException;
import org.apache.commons.lang3.StringUtils;

public abstract class Assert {

    public static void isBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new TPException(message);
        }
    }

    public static void isNull(Object object, String message) {
        if (object == null) {
            throw new TPException(message);
        }
    }
}
