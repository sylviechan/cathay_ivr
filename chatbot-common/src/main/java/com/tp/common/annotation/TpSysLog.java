package com.tp.common.annotation;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TpSysLog {

    String remark() default "";

    boolean showParams() default true;

    boolean showResult() default true;
}
