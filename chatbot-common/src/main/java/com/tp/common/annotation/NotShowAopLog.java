package com.tp.common.annotation;

import java.lang.annotation.*;

/**
 * 如果有敏感資訊不要由aop打印輸入輸出參數(只會打印方法名,請求路徑跟花費時間)時,請再該方法上加上該註解
 * 然後看需要自行在該方法內打印需要的log
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface NotShowAopLog {

}
