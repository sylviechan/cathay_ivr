package com.tp.tpigateway.modules.cathayins.fegin;

import com.tp.common.annotation.TpSysLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.net.URI;
import java.util.Map;

@FeignClient(name = "insagentapi", url = "http://10.95.42.10/ChatbotMW/")
@TpSysLog(remark = "InsuranceAPIFeignService")
public interface InsAgentAPIFeignService {
	
	@RequestMapping(value = "flexchatconnector", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> get4_1(String json);
	
	@RequestMapping(value = "flexchatconnector", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> get4_2(String json);
	
	@RequestMapping(value = "flexchatconnector", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> get4_7(String json);
		
}
