package com.tp.tpigateway.modules.cathaysite.service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.cathaysite.FundRequest;
import com.tp.common.model.cathaysite.FundStopRequest;

import java.util.Map;

/*
 *  投資相關
 *  
 */

public interface SiteInvestmentService {
	/**
	 * For 查詢我的投資
	 *
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getQuota(FundRequest reqVO, Map<String, Object> param);
}
