package com.tp.tpigateway.modules.cathaysec.controller;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysec.FallBackRequest;
import com.tp.common.model.cathaysec.LoginRequest;
import com.tp.common.model.cathaysite.SiteRequest;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.cathaysec.model.HelloRequest;
import com.tp.tpigateway.modules.cathaysec.service.CathaySecService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("cathaysec")
public class CathaySecController extends BaseController {

    @Autowired
    CathaySecService cathaySecService;

    @RequestMapping(value = "/hello", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult hello(@RequestBody HelloRequest helloRequest) {
        BotMessageVO botMessageVO = cathaySecService.hello(helloRequest);
        return TPIGatewayResult.ok().setData(botMessageVO);
    }

    @RequestMapping(value = "/fallback", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult fallback(@RequestBody FallBackRequest fallBackRequest) {
        return cathaySecService.fallback(fallBackRequest);
    }

    /*
        組FAQ回答樣式
     */
    @RequestMapping(value = "/handleFaqReplay", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult handleFaqReplay(@RequestBody Map<String, Object> request) {
        return cathaySecService.handleFaqReplay(request);
    }

    /*
        服務台反問
     */
    @RequestMapping(value = "/supportNLU", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult supportNLU(@RequestBody Map<String, Object> request) {
        return cathaySecService.supportNLU(request);
    }

    @RequestMapping(value = "/showSurvey", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult showSurvey(@RequestBody BotRequest botRequest) {
        BotMessageVO botMessageVO = cathaySecService.showSurvey(botRequest);
        return TPIGatewayResult.ok().setData(botMessageVO);
    }

    @RequestMapping(value = "/doSecSelectAccount", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult doSecSelectAccount(@RequestBody LoginRequest loginRequest) {
        return cathaySecService.doSecSelectAccount(loginRequest);
    }
    @RequestMapping(value = "/cmd", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult cmd(@RequestBody SiteRequest siteRequest) {
        BotMessageVO botMessageVO = cathaySecService.cmd(siteRequest);
        return TPIGatewayResult.ok().setData(botMessageVO);
    }

}
