package com.tp.tpigateway.modules.cathayins.service.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotMessageVO.CImageDataItem;
import com.tp.common.model.BotMessageVO.CImageTextItem;
import com.tp.common.model.BotMessageVO.CTextItem;
import com.tp.common.model.BotMessageVO.CardItem;
import com.tp.common.model.BotMessageVO.ContentItem;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathayins.CarInsuranceRequest;
import com.tp.common.util.DataUtils;
import com.tp.common.util.QuickReplay;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.common.util.InsUrlGather;
import com.tp.tpigateway.modules.cathayins.InsTestApiData;
import com.tp.tpigateway.modules.cathayins.fegin.InsuranceAPIFeignService;
import com.tp.tpigateway.modules.cathayins.model.InsQuickReplay;
import com.tp.tpigateway.modules.cathayins.service.InsCarInsuranceService;

/**
 * 汽車保險相關
 */
@SuppressWarnings("unchecked")
@Service("insCarInsuranceService")
public class InsCarInsuranceServiceImpl implements InsCarInsuranceService {

	@Autowired
	BotResponseUtils botResponseUtils;

	@Autowired
	InsuranceAPIFeignService insuranceAPIFeignService;

	/**
	 * 測試用資料USER提供
	 * 
	 * @param resp
	 * @param type
	 * @return
	 */
	private Map<String, Object> dataTestQueryForType(Map<String, Object> resp, int type) {

		if (type == 1) {
			Map<String, Object> detail = new HashMap<>();
			Map<String, Object> DATAS = new HashMap<>();
			Map<String, Object> car = new HashMap<>();
			car.put("CAR_NO", "552-EDQ");
			car.put("CUSTOMER_ID_2", "S122531322");
			car.put("CUSTOMER_NAME_2", "余承遠");
			car.put("CARMARRIAGE_2_NO", "已婚");
			List<Map<String, Object>> list = new ArrayList<>();

			Map<String, Object> e = new HashMap<>();
			e.put("POLICY_NO", "150119W000028");
			e.put("POLICY_TYPE", "1");
			e.put("POLICY_TYPE_NAME", "強制險");
			list.add(e);

			Map<String, Object> e2 = new HashMap<>();
			e2.put("POLICY_NO", "150119W000028");
			e2.put("POLICY_TYPE", "3");
			e2.put("POLICY_TYPE_NAME", "駕駛人傷害險");
			list.add(e2);

			Map<String, Object> e3 = new HashMap<>();
			e3.put("POLICY_NO", "150119W000028");
			e3.put("POLICY_TYPE", "2");
			e3.put("POLICY_TYPE_NAME", "任意險");
			list.add(e3);

			car.put("POLICY_LIST", list);

			DATAS.put("普通重機 552-EDQ", car);
			DATAS.put("普通重機 553-EDQ", car);
			DATAS.put("普通重機 554-EDQ", car);

			detail.put("DATAS", DATAS);

			resp.put("detail", detail);
		}

		else if (type == 2) {

			List<Map<String, Object>> list = new ArrayList<>();

			Map<String, Object> e = new HashMap<>();
			e.put("POLICY_NO", "150119W000028");
			e.put("POLICY_TYPE", "1");
			e.put("POLICY_TYPE_NAME", "強制險");
			list.add(e);

			Map<String, Object> e2 = new HashMap<>();
			e2.put("POLICY_NO", "150119W000028");
			e2.put("POLICY_TYPE", "3");
			e2.put("POLICY_TYPE_NAME", "駕駛人傷害險");
			list.add(e2);

			Map<String, Object> e3 = new HashMap<>();
			e3.put("POLICY_NO", "150119W000028");
			e3.put("POLICY_TYPE", "2");
			e3.put("POLICY_TYPE_NAME", "任意險");
			list.add(e3);

			Map<String, Object> detail = new HashMap<>();
			Map<String, Object> DATAS = new HashMap<>();

			Map<String, Object> car = new HashMap<>();
			car.put("PHONE_CODE", "02-21621189#6296");
			car.put("CAR_NO", "552-EDQ");
			car.put("DUP_CAR_NO", "N");
			car.put("ADDRESS_NAME", "100 臺北市中正區測試路2號5樓");
			car.put("POLICY_NO", "150119W000028");
			car.put("CELLULAR_PHONE", "0935863758");
			car.put("EMAIL", "simor@yahoo.com.tw");
			car.put("POLICY_LIST", list);

			Map<String, Object> car2 = new HashMap<>();
			car2.put("PHONE_CODE", "02-21621189#6296");
			car2.put("CAR_NO", "6666-AS");
			car2.put("DUP_CAR_NO", "N");
			car2.put("ADDRESS_NAME", "100 臺北市中正區忠孝東路一段2號5樓");
			car2.put("POLICY_NO", "150119B901109");
			car2.put("CELLULAR_PHONE", "0935863758");
			car2.put("EMAIL", "simor@cathay-ins.com.tw");
			car2.put("POLICY_LIST", list);

			DATAS.put("普通重機 552-EDQ", car);
			DATAS.put("自小客車 6666-AS", car2);

			detail.put("DATAS", DATAS);

			resp.put("detail", detail);
		}

		else if (type == 3) {

			List<Map<String, Object>> list = new ArrayList<>();

			Map<String, Object> e = new HashMap<>();
			e.put("POLICY_NO", "150119W000028");
			e.put("POLICY_TYPE", "1");
			e.put("POLICY_TYPE_NAME", "強制險");
			list.add(e);
			Map<String, Object> e2 = new HashMap<>();
			e2.put("POLICY_NO", "150119W000028");
			e2.put("POLICY_TYPE", "3");
			e2.put("POLICY_TYPE_NAME", "駕駛人傷害險");
			list.add(e2);

			Map<String, Object> e3 = new HashMap<>();
			e3.put("POLICY_NO", "150119W000028");
			e3.put("POLICY_TYPE", "2");
			e3.put("POLICY_TYPE_NAME", "任意險");
			list.add(e3);

			Map<String, Object> detail = new HashMap<>();

			Map<String, Object> DATAS = new HashMap<>();

			Map<String, Object> car = new HashMap<>();
			car.put("CAR_NO_NAME", "02-21621189#6296");
			car.put("CAR_NO", "552-EDQ");
			car.put("DUP_CAR_NO", "N");
			car.put("ENGINE_NO", "A345HYk134");
			car.put("ORIGIN_ISSUE_DATE", "106/03/01");
			car.put("POLICY_LIST", list);

			Map<String, Object> car2 = new HashMap<>();
			car2.put("PHONE_CODE", "02-21621189#6296");
			car2.put("CAR_NO", "6666-AS");
			car2.put("DUP_CAR_NO", "N");
			car2.put("ENGINE_NO", "33A456QQ99");
			car2.put("ORIGIN_ISSUE_DATE", "106/03/11");
			car2.put("POLICY_LIST", list);

			DATAS.put("普通重機 552-EDQ", car);
			DATAS.put("自小客車 6666-AS", car2);

			detail.put("DATAS", DATAS);

			resp.put("detail", detail);
		}

		return resp;
	}

	/**
	 * 發送更改結果資訊
	 * 
	 * @param botMessageVO
	 * @param reqVO
	 * @param isDetailedVersion 是否詳細版
	 */
	private void set2OKRtnMsg(BotMessageVO botMessageVO, CarInsuranceRequest reqVO, Boolean isDetailedVersion) {
		botResponseUtils.setTextResult(botMessageVO, "已變更完成！同一台車下的這些保單也會一併更改喔~");
		String[] carNoArray = reqVO.getCarNoArray().split(",");
		List<Map<String, Object>> ajaxData = reqVO.getAjaxData();
		String msg = "";
		for (Map<String, Object> map : ajaxData) {
			for (String carNo : carNoArray) {
				if (StringUtils.equals(carNo, (String) map.get("CAR_NO"))) {
					try {
						JSONArray jsonArr = new JSONArray(map.get("POLICY_LIST").toString());

						for (int i = 0; i < jsonArr.length(); i++) {
							JSONObject jsonObj = jsonArr.getJSONObject(i);

							msg = msg + "<font color='#FF0000'>" + jsonObj.get("POLICY_TYPE_NAME") + "</font>" + "<br>";
							msg = msg + "<font color='#FF0000'>" + jsonObj.get("POLICY_NO") + "</font>" + "<br>";
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
			}
		}
		botResponseUtils.setTextResult(botMessageVO, msg);

		if (isDetailedVersion) {
			botResponseUtils.setTextResult(botMessageVO, "阿發提醒你，變更車籍資料需要人工審核，大概要過一個工作天後才會完成變更喔！");
			botResponseUtils.setTextResult(botMessageVO, "你還要改其他的項目嗎？或者阿發也能幫你追蹤變更進度喔~要現在看看嗎？");
			botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_03,
					InsQuickReplay.ChangeProgressQuery, InsQuickReplay.INIT);
		} else {
			botResponseUtils.setTextResult(botMessageVO, "您還要改其他項目?");
			botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_03,
					InsQuickReplay.ChangeCarInsurance, InsQuickReplay.INIT);
		}

	}

	/**
	 * 物件轉JSON功能
	 * 
	 * @param obj
	 * @return
	 */
	public static String Object2Json(Object obj) {

		ObjectMapper mapper = new ObjectMapper();

		String jsonInString = "";

		try {
			jsonInString = mapper.writeValueAsString(obj);

		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return jsonInString;

	}

	/**
	 * EXL資訊顯示
	 * 
	 * @param excelPath
	 * @return
	 * @throws Exception
	 */
	private List<Map<String, Object>> ReadCityExcel(String excelPath) throws Exception {
		InputStream in = new FileInputStream(excelPath);
		XSSFWorkbook xssfWorkbook = null;// .xlsx
		xssfWorkbook = new XSSFWorkbook(in);
		int rowSize = (xssfWorkbook.getSheetAt(0).getLastRowNum() + 1);// 總行數//
																		// SheetAt(0)
																		// 第一標籤頁面
		List<Map<String, Object>> cityList = new ArrayList<>();
		String[] key = {};
		for (int i = 0; i < rowSize; i++) {
			XSSFRow nowRow = xssfWorkbook.getSheetAt(0).getRow(i);// 讀取的該行
			if (nowRow != null) {
				Map<String, Object> map = new HashMap<String, Object>();
				for (int j = 0; j < nowRow.getLastCellNum(); j++) {
					if (i == 0) {
						if (j == 0) {
							key = new String[nowRow.getLastCellNum()]; // 新增標題數量的空間給array
						}
						Array.set(key, j, nowRow.getCell(j).getStringCellValue()); // 暫存標題當key，等下一筆當key
					} else {
						map.put(key[j], nowRow.getCell(j).getStringCellValue());
						cityList.add(map);
					}
				}
			}
		}
		return cityList;
	}

	/**
	 * 僅Ajax使用 changeCarMemberCheck
	 */
	@Override
	public TPIGatewayResult changeCarMemberCheck(CarInsuranceRequest reqVO) {
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		// 初始分組
		ContentItem content = botMessageVO.new ContentItem();
		content.setType(30);

		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		// 牌卡
		CardItem cards = botMessageVO.new CardItem();
		List<Map<String, Object>> cTextsList = new ArrayList<>();

		// 組牌卡-------

		// 圖
		CImageDataItem cImageData = botMessageVO.new CImageDataItem();
		CImageTextItem cImageTextItem = botMessageVO.new CImageTextItem();
		cImageTextItem.setCImageTextTitle("資料變更");
		cTextsList.add(cImageTextItem.getCImageTexts());
		cImageData.setCImageTexts(cTextsList);
		cImageData.setCImageUrl("card37-01.png");
		cImageData.setCImageTextType(1);

		cards.setCImageData(cImageData.getCImageDatas());// 組裝起來
		// 中間
		List<Map<String, Object>> cTexts = new ArrayList<Map<String, Object>>();

		cards.setCTextType("1");
		cards.setCWidth("segment");

		CTextItem cText = botMessageVO.new CTextItem();

		cText = botMessageVO.new CTextItem();
		cText.setCLabel("車主姓名");
		cText.setCText(reqVO.getMemberName());
		cTexts.add(cText.getCTexts());
		cards.setCTexts(cTexts);// 組裝起來

		cText = botMessageVO.new CTextItem();
		cText.setCLabel("婚姻狀況");
		cText.setCText(reqVO.getMemberMarriage());
		cTexts.add(cText.getCTexts());
		cards.setCTexts(cTexts);// 組裝起來

		// 底下按鈕
		List<Map<String, Object>> cLinkContents = new ArrayList<Map<String, Object>>();
		Map<String, Object> parameter = new HashMap<>();
		parameter.put("policyNoArray", reqVO.getPolicyNoArray());
		parameter.put("memberName", reqVO.getMemberName());
		parameter.put("memberMarriage", reqVO.getMemberMarriage());

		QuickReplay q1 = new InsQuickReplay(1, "改車主姓名", "改車主姓名", "", "", parameter);
		cLinkContents.add(botResponseUtils.getCLinkListItem(botMessageVO, q1).getCLinkList());
		QuickReplay q2 = new InsQuickReplay(1, "改婚姻狀況", "改婚姻狀況", "", "", parameter);
		cLinkContents.add(botResponseUtils.getCLinkListItem(botMessageVO, q2).getCLinkList());

		// 這一段要儲存按鈕資訊給flow
		tpiGatewayResult.setMsgList(Arrays.asList(q1.getText(), q2.getText()));
		tpiGatewayResult.setOptionList(Arrays.asList(q1, q2));
		// --

		cards.setCLinkList(cLinkContents);// 組裝起來

		cardList.add(cards.getCards());

		content.setCards(cardList);

		botMessageVO.getContent().add(content.getContent());

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	/**
	 * 僅Ajax使用 changeCarInsuranceCheck
	 */
	@Override
	public TPIGatewayResult changeCarInsuranceCheck(CarInsuranceRequest reqVO) {

		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		if (reqVO.getAjaxData().size() > 0 && StringUtils.isNotBlank(reqVO.getCarNoArray())) {

			String selectedCarNo[] = reqVO.getCarNoArray().split(",");

			// 初始分組
			ContentItem content = botMessageVO.new ContentItem();
			content.setType(30);

			List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
			// 牌卡
			CardItem cards = botMessageVO.new CardItem();
			List<Map<String, Object>> cTextsList = new ArrayList<>();

			// 組牌卡-------

			// 圖
			CImageDataItem cImageData = botMessageVO.new CImageDataItem();
			CImageTextItem cImageTextItem = botMessageVO.new CImageTextItem();
			cImageTextItem.setCImageTextTitle("資料變更");
			cTextsList.add(cImageTextItem.getCImageTexts());
			cImageData.setCImageTexts(cTextsList);
			cImageData.setCImageUrl("card37-01.png");
			cImageData.setCImageTextType(1);

			cards.setCImageData(cImageData.getCImageDatas());// 組裝起來
			// 中間
			List<Map<String, Object>> cTexts = new ArrayList<Map<String, Object>>();

			cards.setCTextType("1");
			cards.setCWidth("segment");

			CTextItem cText = botMessageVO.new CTextItem();

			if (selectedCarNo.length < 2) {
				for (String string : selectedCarNo) {
					List<Map<String, Object>> dataList = reqVO.getAjaxData();
					for (Map<String, Object> data : dataList) {
						if (StringUtils.equals(string, (String) data.get("CAR_NO"))) {
							cText = botMessageVO.new CTextItem();
							cText.setCLabel("市話");
							cText.setCText((String) data.get("CELLULAR_PHONE"));
							cTexts.add(cText.getCTexts());
							cards.setCTexts(cTexts);// 組裝起來

							cText = botMessageVO.new CTextItem();
							cText.setCLabel("手機");
							cText.setCText((String) data.get("PHONE_CODE"));
							cTexts.add(cText.getCTexts());
							cards.setCTexts(cTexts);// 組裝起來

							cText = botMessageVO.new CTextItem();
							cText.setCLabel("電子信箱");
							cText.setCText((String) data.get("EMAIL"));
							cTexts.add(cText.getCTexts());
							cards.setCTexts(cTexts);// 組裝起來

							cText = botMessageVO.new CTextItem();
							cText.setCLabel("地址");
							cText.setCText((String) data.get("ADDRESS_NAME"));
							cTexts.add(cText.getCTexts());
							cards.setCTexts(cTexts);// 組裝起來
						}
					}
				}
			} else {
				cText = botMessageVO.new CTextItem();
				cText.setCLabel("所選的多台車資料會一起變更~");
				cText.setCText("");
				cTexts.add(cText.getCTexts());
				cards.setCTexts(cTexts);// 組裝起來
			}

			// 底下按鈕
			List<Map<String, Object>> cLinkContents = new ArrayList<Map<String, Object>>();
			Map<String, Object> parameter = new HashMap<>();
			parameter.put("carNoArray", reqVO.getCarNoArray());
			parameter.put("ajaxData", reqVO.getAjaxData());

			QuickReplay q1 = new InsQuickReplay(1, "改市話", "改市話", "", "", parameter);
			cLinkContents.add(botResponseUtils.getCLinkListItem(botMessageVO, q1).getCLinkList());
			QuickReplay q2 = new InsQuickReplay(1, "改手機", "改手機", "", "", parameter);
			cLinkContents.add(botResponseUtils.getCLinkListItem(botMessageVO, q2).getCLinkList());
			QuickReplay q3 = new InsQuickReplay(1, "改電子信箱", "改電子信箱", "", "", parameter);
			cLinkContents.add(botResponseUtils.getCLinkListItem(botMessageVO, q3).getCLinkList());
			QuickReplay q4 = new InsQuickReplay(1, "改地址", "改地址", "", "", parameter);
			cLinkContents.add(botResponseUtils.getCLinkListItem(botMessageVO, q4).getCLinkList());

			// 這一段要儲存按鈕資訊給flow
			tpiGatewayResult.setMsgList(Arrays.asList(q1.getText(), q2.getText(), q3.getText(), q4.getText()));
			tpiGatewayResult.setOptionList(Arrays.asList(q1, q2, q3, q4));
			// --

			cards.setCLinkList(cLinkContents);// 組裝起來

			cardList.add(cards.getCards());

			content.setCards(cardList);

			botMessageVO.getContent().add(content.getContent());

		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	/**
	 * 僅Ajax使用 changeCarRegistrationCheck
	 */
	@Override
	public TPIGatewayResult changeCarRegistrationCheck(CarInsuranceRequest reqVO) {
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		if (reqVO.getAjaxData().size() > 0 && StringUtils.isNotBlank(reqVO.getCarNoArray())) {

			String selectedCarNo[] = reqVO.getCarNoArray().split(",");

			// 初始分組
			ContentItem content = botMessageVO.new ContentItem();
			content.setType(30);

			List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
			// 牌卡
			CardItem cards = botMessageVO.new CardItem();
			List<Map<String, Object>> cTextsList = new ArrayList<>();

			// 組牌卡-------

			// 圖
			CImageDataItem cImageData = botMessageVO.new CImageDataItem();
			CImageTextItem cImageTextItem = botMessageVO.new CImageTextItem();
			cImageTextItem.setCImageTextTitle("資料變更");
			cTextsList.add(cImageTextItem.getCImageTexts());
			cImageData.setCImageTexts(cTextsList);
			cImageData.setCImageUrl("card37-01.png");
			cImageData.setCImageTextType(1);

			cards.setCImageData(cImageData.getCImageDatas());// 組裝起來
			// 中間
			List<Map<String, Object>> cTexts = new ArrayList<Map<String, Object>>();

			cards.setCTextType("1");
			cards.setCWidth("segment");

			CTextItem cText = botMessageVO.new CTextItem();

			if (selectedCarNo.length < 2) {
				for (String string : selectedCarNo) {
					List<Map<String, Object>> dataList = reqVO.getAjaxData();
					for (Map<String, Object> data : dataList) {
						if (StringUtils.equals(string, (String) data.get("CAR_NO"))) {
							cText = botMessageVO.new CTextItem();
							cText.setCLabel("車牌號碼");
							cText.setCText((String) data.get("CAR_NO"));
							cTexts.add(cText.getCTexts());
							cards.setCTexts(cTexts);// 組裝起來

							cText = botMessageVO.new CTextItem();
							cText.setCLabel("引擎/車身號碼");
							cText.setCText((String) data.get("ENGINE_NO"));
							cTexts.add(cText.getCTexts());
							cards.setCTexts(cTexts);// 組裝起來

							cText = botMessageVO.new CTextItem();
							cText.setCLabel("發照年月");
							cText.setCText((String) data.get("ORIGIN_ISSUE_DATE"));
							cTexts.add(cText.getCTexts());
							cards.setCTexts(cTexts);// 組裝起來
						}
					}
				}
			} else {
				cText = botMessageVO.new CTextItem();
				cText.setCLabel("所選的多台車資料會一起變更~");
				cText.setCText("");
				cTexts.add(cText.getCTexts());
				cards.setCTexts(cTexts);// 組裝起來
			}

			// 底下按鈕
			List<Map<String, Object>> cLinkContents = new ArrayList<Map<String, Object>>();
			Map<String, Object> parameter = new HashMap<>();
			parameter.put("carNoArray", reqVO.getCarNoArray());
			parameter.put("ajaxData", reqVO.getAjaxData());

			QuickReplay q1 = new InsQuickReplay(1, "改車牌號碼", "改車牌號碼", "", "", parameter);
			cLinkContents.add(botResponseUtils.getCLinkListItem(botMessageVO, q1).getCLinkList());
			QuickReplay q2 = new InsQuickReplay(1, "改引擎/車身號碼", "改引擎/車身號碼", "", "", parameter);
			cLinkContents.add(botResponseUtils.getCLinkListItem(botMessageVO, q2).getCLinkList());
			QuickReplay q3 = new InsQuickReplay(1, "改發照年月", "改發照年月", "", "", parameter);
			cLinkContents.add(botResponseUtils.getCLinkListItem(botMessageVO, q3).getCLinkList());

			// 這一段要儲存按鈕資訊給flow
			tpiGatewayResult.setMsgList(Arrays.asList(q1.getText(), q2.getText(), q3.getText()));
			tpiGatewayResult.setOptionList(Arrays.asList(q1, q2, q3));
			// --

			cards.setCLinkList(cLinkContents);// 組裝起來

			cardList.add(cards.getCards());

			content.setCards(cardList);

			botMessageVO.getContent().add(content.getContent());

		}
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_03, InsQuickReplay.ChangeCar,
				InsQuickReplay.INIT);

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	/**
	 * 僅Ajax使用 changeCarRegistration 該功能僅用於[車籍資料]確認送出按鈕用
	 */
	@Override
	public BotMessageVO changeCarRegistration(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		String rtc = "0000";

		// TODO API-C03 -----

		// ---

		if (rtc.equals("0000")) {
			set2OKRtnMsg(botMessageVO, reqVO, true);
		}
		return botMessageVO;
	};

	@Override
	public BotMessageVO memberInformationInitial(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setTextResult(botMessageVO, "阿發提醒你：<br>1.車主變更資料一天只能申請一次 <br>2.須附上身分證正、反面照 <br>3.須為要保人同一人的保單才可以申請變更 <br><br><span style=\"color:red\">要前往國泰產險官網的數位服務平台辦理嗎？</span>");
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_04, InsQuickReplay.GO_URL_MEMBER(reqVO.getSsoId()),
				InsQuickReplay.Change_CS, InsQuickReplay.INIT);
		return botMessageVO;
	}

	@Override
	public BotMessageVO memberInformationCarSelected(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setTextResult(botMessageVO, "登入成功~阿發馬上來調你的保單資料，稍等一下喔~");

		// API-C02 -----

		// API測試資料 QUERY_TYPE = 1
		// reqVO.setMemberId("S122531322");
		// reqVO.setMemberToken("xWQ2FcU9AYWIPEyIfmsoFHOFNRON944aniUDNorcmwtDF+xbKfLwrI8wcwZOddMY");
		// reqVO.setSessionId("66666");
		// reqVO.setLoginIp("127.0.0.1");

		Map<String, String> map = new HashMap<>();
		map.put("MEMBER_ID", reqVO.getMemberId());
		map.put("MEMBER_TOKEN", reqVO.getMemberToken());
		map.put("QUERY_TYPE", "1");
		map.put("RECEIVE_TYPE", "65");
		map.put("SESSION_ID", reqVO.getSessionId());
		map.put("LOGIN_IP", reqVO.getLoginIp());
		map.put("VIEW_DEVICE", "P");

		Map<String, Object> req = new HashMap<>();
		req.put("map", map);

		Map<String, Object> resp = new HashMap<>();
		resp = insuranceAPIFeignService.getQueryCarList(req);
		// resp = dataTestQueryForType(resp, 1);//QUERY_TYPE = 1 自製測試資料

		Map<String, Object> respDetail = (Map<String, Object>) resp.get("detail");
		Map<String, Object> respDATAS = (Map<String, Object>) respDetail.get("DATAS");

		List<Map<String, Object>> dataList = new ArrayList<>();
		String serviceKey = "";
		for (Map.Entry<String, Object> entry : respDATAS.entrySet()) {
			Map<String, Object> data = new HashMap<>();
			data.put("carName", entry.getKey());
			Map<String, Object> vals = (Map<String, Object>) entry.getValue();
			data.put("CUSTOMER_NAME_2", vals.get("CUSTOMER_NAME_2"));

			String memberMarriage = (String) vals.get("CARMARRIAGE_2_NO");
			if (StringUtils.isNotBlank(memberMarriage)) {

				if (StringUtils.equals(memberMarriage, "1")) {
					data.put("CARMARRIAGE_2_NO", "已婚");
				} else if (StringUtils.equals(memberMarriage, "2")) {
					data.put("CARMARRIAGE_2_NO", "未婚");
				} else {
					data.put("CARMARRIAGE_2_NO", memberMarriage);
				}

			}
			List<Map<String, Object>> policyList = (List<Map<String, Object>>) vals.get("POLICY_LIST");
			for (Map<String, Object> policyMap : policyList) {
				serviceKey = serviceKey + "," + policyMap.get("POLICY_NO");
			}

			dataList.add(data);
		}

		if (StringUtils.isNotBlank(serviceKey)) {
			serviceKey = serviceKey.substring(1);
		}
		if (dataList.size() > 0) {
			botResponseUtils.setDoSelectSearchComp(botMessageVO, dataList, "車輛選擇", "3", serviceKey,"");
		} else {
			botResponseUtils.setTextResult(botMessageVO, "阿發查不到以你為要保人或被保人的有效保單喔！");
			botResponseUtils.setTextResult(botMessageVO, "你可以到<a href=\"javascript:void(0);\" onclick=\"window.open('"+InsUrlGather.ECIE_1000+DataUtils.getRectifySSOId(reqVO.getSsoId())+"');\">bobe線上投保網站</a>，直接投保哦~");
			botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_03, InsQuickReplay.END_CHAT,
					InsQuickReplay.INIT);
		}

		return botMessageVO;
	}

	@Override
	public BotMessageVO memberInformationChangeNameSet1(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setTextResult(botMessageVO, "你要改成甚麼名字呢？要與身分證上的相同哦~");
		return botMessageVO;
	}

	@Override
	public BotMessageVO memberInformationChangeNameSet2(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		String rtnCode = "0000";

		// TODO API-C04 -----
		/*
		 * 先寫到 C04 QUERY_TYPE=1 newCustomerName=新名字 Policy_no_array=修改的保單 (多筆用逗點分隔)
		 * 
		 * 會return upl_no 批改進件編號
		 */
		Map<String, String> map = new HashMap<>();
		map.put("MEMBER_ID", reqVO.getMemberId());
		map.put("MEMBER_TOKEN", reqVO.getMemberToken());
		map.put("SESSION_ID", reqVO.getSessionId());
		map.put("RECEIVE_TYPE", "65");
		map.put("QUERY_TYPE", "1");
		map.put("LOGIN_IP", reqVO.getLoginIp());
		map.put("VIEW_DEVICE", "P");
		map.put("newCustomerName", reqVO.getNewMemberName());

		Map<String, Object> req = new HashMap<>();
		req.put("map", map);

		Map<String, Object> resp = insuranceAPIFeignService.saveTemp(req);
		// 客戶沒有寫非0000狀況
		if (StringUtils.equals(rtnCode, "0000")) {
			botResponseUtils.setTextResult(botMessageVO, "這項變更需要你上傳身分證正反面哦~");
			botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_03, InsQuickReplay.CameraOrUPD,
					InsQuickReplay.INIT);
		}
		return botMessageVO;
	}

	@Override
	public BotMessageVO memberInformationChangeMarriageSet1(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		String msg = "";
		String type = "";

		if (StringUtils.isNotBlank(reqVO.getMemberMarriage())) {
			type = reqVO.getMemberMarriage();

			if (StringUtils.equals(type, "1")) {
				type = "已婚";
			} else if (StringUtils.equals(type, "2")) {
				type = "未婚";
			}
		}

		if (StringUtils.equals(type, "未婚")) {
			msg = "已婚";
		} else if (StringUtils.equals(type, "已婚")) {
			msg = "未婚";
		}

		botResponseUtils.setTextResult(botMessageVO, "現在的狀態是" + reqVO.getMemberMarriage() + "，要改為" + msg + "嗎");
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_03, InsQuickReplay.Change,
				InsQuickReplay.No);
		return botMessageVO;
	}

	@Override
	public BotMessageVO memberInformationChangeMarriage(CarInsuranceRequest reqVO) {

		String rtnMsg = "0000";

		// TODO API-C03 -----
		/*
		 * UPDATE資料寫入 C03
		 * 
		 * QUERY_TYPE = 1 newMarriage = 1:已婚 2:未婚
		 */
		// --

		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		if (rtnMsg.equals("0000")) {

			botResponseUtils.setTextResult(botMessageVO, "已變更完成！同一台車下的這些保單也會一併更改喔~");

			// API-C02 -----

			// API測試資料 QUERY_TYPE = 1
			// reqVO.setMemberId("S122531322");
			// reqVO.setMemberToken("xWQ2FcU9AYWIPEyIfmsoFHOFNRON944aniUDNorcmwtDF+xbKfLwrI8wcwZOddMY");
			// reqVO.setSessionId("66666");
			// reqVO.setLoginIp("127.0.0.1");

			Map<String, String> map = new HashMap<>();
			map.put("MEMBER_ID", reqVO.getMemberId());
			map.put("MEMBER_TOKEN", reqVO.getMemberToken());
			map.put("QUERY_TYPE", "1");
			map.put("RECEIVE_TYPE", "65");
			map.put("SESSION_ID", reqVO.getSessionId());
			map.put("LOGIN_IP", reqVO.getLoginIp());
			map.put("VIEW_DEVICE", "P");

			Map<String, Object> req = new HashMap<>();
			req.put("map", map);

			Map<String, Object> resp = new HashMap<>();
			resp = insuranceAPIFeignService.getQueryCarList(req);

			// resp = dataTestQueryForType(resp, 1);//QUERY_TYPE = 1 自製測試資料

			Map<String, Object> respDetail = (Map<String, Object>) resp.get("detail");
			Map<String, Object> respDATAS = (Map<String, Object>) respDetail.get("DATAS");

			String msg = "";
			for (Map.Entry<String, Object> entry : respDATAS.entrySet()) {
				Map<String, Object> vals = (Map<String, Object>) entry.getValue();

				List<Map<String, Object>> policyList = (List<Map<String, Object>>) vals.get("POLICY_LIST");
				for (Map<String, Object> policyMap : policyList) {
					msg = msg + "<font color='#FF0000'>" + policyMap.get("POLICY_TYPE_NAME") + "</font>" + "<br>";
					msg = msg + "<font color='#FF0000'>" + policyMap.get("POLICY_NO") + "</font>" + "<br>";
				}

				botResponseUtils.setTextResult(botMessageVO, msg);
			}
			botResponseUtils.setTextResult(botMessageVO, "你還要改其他的項目嗎？");
		}
		return botMessageVO;
	}

	@Override
	public BotMessageVO memberInformationOpenCameraAlbum(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setInsCameraAlbumComp(botMessageVO, "1");
		return botMessageVO;
	}

	@Override
	public BotMessageVO cameraAlbumUPD(CarInsuranceRequest reqVO, Boolean isVehicleCertificate) {
		// API-C03 -----

		// ---
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		botResponseUtils.setTextResult(botMessageVO, "好喔，幫你送出變更了，同一台車下的這些保單也會一併更改喔~");

		// API-C02 -----

		// API測試資料 QUERY_TYPE = 1
		// reqVO.setMemberId("S122531322");
		// reqVO.setMemberToken("xWQ2FcU9AYWIPEyIfmsoFHOFNRON944aniUDNorcmwtDF+xbKfLwrI8wcwZOddMY");
		// reqVO.setSessionId("66666");
		// reqVO.setLoginIp("127.0.0.1");

		Map<String, String> map = new HashMap<>();
		map.put("MEMBER_ID", reqVO.getMemberId());
		map.put("MEMBER_TOKEN", reqVO.getMemberToken());
		map.put("QUERY_TYPE", "1");
		map.put("RECEIVE_TYPE", "65");
		map.put("SESSION_ID", reqVO.getSessionId());
		map.put("LOGIN_IP", reqVO.getLoginIp());
		map.put("VIEW_DEVICE", "P");

		Map<String, Object> req = new HashMap<>();
		req.put("map", map);

		Map<String, Object> resp = new HashMap<>();
		resp = insuranceAPIFeignService.getQueryCarList(req);
		// resp = dataTestQueryForType(resp, 1);//QUERY_TYPE = 1 自製測試資料

		Map<String, Object> respDetail = (Map<String, Object>) resp.get("detail");
		Map<String, Object> respDATAS = (Map<String, Object>) respDetail.get("DATAS");

		String msg = "";
		for (Map.Entry<String, Object> entry : respDATAS.entrySet()) {
			Map<String, Object> vals = (Map<String, Object>) entry.getValue();
			Map<String, Object> policy = (Map<String, Object>) vals.get("POLICY_LIST");
			for (Map.Entry<String, Object> policyEntry : policy.entrySet()) {
				Map<String, Object> ls = (Map<String, Object>) policyEntry.getValue();

				msg = msg + "<font color='#FF0000'>" + ls.get("POLICY_TYPE_NAME") + "</font>" + "<br>";
				msg = msg + "<font color='#FF0000'>" + ls.get("POLICY_NO") + "</font>" + "<br>";
			}
		}

		botResponseUtils.setTextResult(botMessageVO, msg);

		botResponseUtils.setTextResult(botMessageVO, "阿發提醒你，變更車主姓名需要人工審核，大概要過一個工作天後才會完成變更喔！");
		botResponseUtils.setTextResult(botMessageVO, "你還要改其他的項目嗎？或者阿發也能幫你追蹤變更進度喔~要現在看看嗎？");
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_03, InsQuickReplay.ChangeProgressQuery,
				InsQuickReplay.INIT);

		return botMessageVO;
	}

	@Override
	public BotMessageVO carInsuranceCommunicationInitial(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		
		botResponseUtils.setTextResult(botMessageVO, "阿發提醒你：<br />數位服務平台可以辦理車險通訊資料變更<br />1.一天僅開放申請一次 <br />2.須為要被保人同一人的保單才可申請保單變更：");

		botResponseUtils.setTextResult(botMessageVO, "要前往國泰產險官網的數位服務平台辦理嗎？");

		botResponseUtils.setTextResult(botMessageVO, "若是其他險種通訊地址變更，阿發可以請真人客服來幫你哦~");
		
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_04, InsQuickReplay.GO_URL_COMMUNICATION(reqVO.getSsoId()),
				InsQuickReplay.Change_CS, InsQuickReplay.INIT);
		return botMessageVO;
	}

	@Override
	public BotMessageVO carInsuranceCommunicationCarSelected(CarInsuranceRequest reqVO) {

		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setTextResult(botMessageVO, "登入成功~阿發馬上來調你的保單資料，稍等一下喔~");

		// API-C02 -----

		// API測試資料 QUERY_TYPE = 2
		// reqVO.setMemberId("S122531322");
		// reqVO.setMemberToken("xWQ2FcU9AYWIPEyIfmsoFHOFNRON944aniUDNorcmwtDF+xbKfLwrI8wcwZOddMY");
		// reqVO.setSessionId("66666");
		// reqVO.setLoginIp("127.0.0.1");

		Map<String, String> map = new HashMap<>();
		map.put("MEMBER_ID", reqVO.getMemberId());
		map.put("MEMBER_TOKEN", reqVO.getMemberToken());
		map.put("QUERY_TYPE", "2");
		map.put("RECEIVE_TYPE", "65");
		map.put("SESSION_ID", reqVO.getSessionId());
		map.put("LOGIN_IP", reqVO.getLoginIp());
		map.put("VIEW_DEVICE", "P");

		Map<String, Object> req = new HashMap<>();
		req.put("map", map);

		Map<String, Object> resp = new HashMap<>();
		resp = insuranceAPIFeignService.getQueryCarList(req);
		// resp = dataTestQueryForType(resp, 2);//QUERY_TYPE = 2 自製測試資料

		Map<String, Object> respDetail = (Map<String, Object>) resp.get("detail");
		Map<String, Object> respDATAS = (Map<String, Object>) respDetail.get("DATAS");

		List<Map<String, Object>> dataList = new ArrayList<>();
		for (Map.Entry<String, Object> entry : respDATAS.entrySet()) {
			Map<String, Object> data = new HashMap<>();
			data.put("carName", entry.getKey());
			Map<String, Object> vals = (Map<String, Object>) entry.getValue();
			data.put("PHONE_CODE", vals.get("PHONE_CODE"));
			data.put("CAR_NO", vals.get("CAR_NO"));
			data.put("DUP_CAR_NO", vals.get("DUP_CAR_NO"));
			data.put("ADDRESS_NAME", vals.get("ADDRESS_NAME"));
			data.put("POLICY_NO", vals.get("POLICY_NO"));
			data.put("CELLULAR_PHONE", vals.get("CELLULAR_PHONE"));
			data.put("EMAIL", vals.get("EMAIL"));
			/*
			 * 因ajax傳送資訊會因為List<Map<String, Object>>內包含List<Map<String,
			 * Object>>造成無法依照GS準則放入參數
			 */
			data.put("POLICY_LIST", Object2Json((List<Map<String, Object>>) vals.get("POLICY_LIST")));
			dataList.add(data);
		}

		if (dataList.size() > 0) {
			botResponseUtils.setDoSelectSearchComp(botMessageVO, dataList, "車輛選擇", "4", "","");
		} else {
			botResponseUtils.setTextResult(botMessageVO, "阿發查不到以你為要保人或被保人的有效保單喔！");
			botResponseUtils.setTextResult(botMessageVO, "你可以到<a href=\"javascript:void(0);\" onclick=\"window.open('"+InsUrlGather.ECIE_1000+DataUtils.getRectifySSOId(reqVO.getSsoId())+"');\">bobe線上投保網站</a>，直接投保哦~");
			botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_03, InsQuickReplay.END_CHAT,
					InsQuickReplay.INIT);
		}

		return botMessageVO;
	}

	@Override
	public BotMessageVO carInsuranceCommunicationChangeCellularPhoneSet1(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setTextResult(botMessageVO, "市話要改成幾號呢？直接告訴阿發吧，像是這樣：02-26566999");
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_03, InsQuickReplay.ChangeCar,
				InsQuickReplay.INIT);
		return botMessageVO;
	}

	@Override
	public BotMessageVO carInsuranceCommunicationChangePhoneSet1(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setTextResult(botMessageVO, "手機要改成幾號呢？直接告訴阿發吧，像是這樣：0910123456");
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_03, InsQuickReplay.ChangeCar,
				InsQuickReplay.INIT);
		return botMessageVO;
	}

	@Override
	public BotMessageVO carInsuranceCommunicationChangeEmailSet1(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setTextResult(botMessageVO, "電子信箱要改成什麼呢？直接告訴阿發吧，像是這樣：demo@cathay-ins.com.tw");
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_03, InsQuickReplay.ChangeCar,
				InsQuickReplay.INIT);
		return botMessageVO;
	}

	@Override
	public BotMessageVO carInsuranceCommunicationChangeAddressSet1(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setTextResult(botMessageVO, "地址要改成什麼呢？");

		/*
		 * 地址顯示功能，會將data先存著等等步驟二會用
		 */

		List<Map<String, Object>> dataList = new ArrayList<>();

		List<Map<String, Object>> cityExcelList = new ArrayList<>();

		// 要開窗 讀EXL顯示選單
		try {
			cityExcelList = ReadCityExcel("/Users/sakin/Downloads/郵遞區號明細檔.xlsx");
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<Map<String, Object>> city = new ArrayList<>();
		if (cityExcelList.size() > 0) {

			String s = "";
			for (Map<String, Object> map : cityExcelList) {
				if (!s.contains((String) map.get("DISTRICT_NAME"))) {
					s = s + "," + map.get("DISTRICT_NAME");
					Map<String, Object> setCity = new HashMap<>();
					setCity.put("DISTRICT_CODE", map.get("DISTRICT_CODE"));
					setCity.put("DISTRICT_NAME", map.get("DISTRICT_NAME"));
					city.add(setCity);
				}
			}

		}

		Map<String, Object> m = new HashMap<>();
		m.put("cityExcelList", cityExcelList);
		m.put("city", city);
		dataList.add(m);
		botResponseUtils.setDoSelectSearchComp(botMessageVO, dataList, "車輛選擇", "5", "","");
		return botMessageVO;
	}

	@Override
	public BotMessageVO carInsuranceCommunicationChangeCellularPhoneSet2(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Boolean verificationStatus = true; // 驗證是否成功
		String s = reqVO.getNewCellularPhone();
		if (StringUtils.isNotBlank(s)) {

			// 5碼 馬祖[4] 6金門[3]{區082||0826} 台東[089] 苗栗[037] 7碼 桃園 新竹 宜蘭 花蓮 彰化
			// 南投[049]
			Pattern pattern = Pattern.compile("[0-9]{0,4}[-]{1}[0-9]{5,8}");
			Matcher matcher = pattern.matcher(s);

			if (matcher.matches()) {
				String dv[] = s.split("-");
				// 金門
				if (StringUtils.equals(dv[0], "0826") | StringUtils.equals(dv[0], "0836")) {
					if (dv[0].length() != 5) {
						verificationStatus = false;
					}
				}
				// 金門 台東 苗栗
				else if (StringUtils.equals(dv[0], "082") || StringUtils.equals(dv[0], "089")
						|| StringUtils.equals(dv[0], "037")) {
					if (dv[0].length() != 6) {
						verificationStatus = false;
					}
				}
				// 屏東 高雄 澎湖 台南 雲林 嘉義 南投 桃園 新竹 花蓮 宜蘭
				else if (StringUtils.equals(dv[0], "08") || StringUtils.equals(dv[0], "07")
						|| StringUtils.equals(dv[0], "06") || StringUtils.equals(dv[0], "05")
						|| StringUtils.equals(dv[0], "049") || StringUtils.equals(dv[0], "03")) {
					if (dv[0].length() != 7) {
						verificationStatus = false;
					}
				}
				// 彰化 台中
				else if (StringUtils.equals(dv[0], "04")) {
					if (dv[0].length() != 7 && dv[0].length() != 8) {
						verificationStatus = false;
					}
				}
				// 台北
				else if (StringUtils.equals(dv[0], "02")) {
					if (dv[0].length() != 8) {
						verificationStatus = false;
					}
				}

				else {
					verificationStatus = false;
				}

			} else {
				verificationStatus = false;
			}

		} else {
			verificationStatus = false;
		}

		if (verificationStatus) {
			botResponseUtils.setTextResult(botMessageVO, "怪怪的~是不是打錯字了呢？再告訴阿發一次吧~");
		}

		// TODO API-C03 -----

		// ---
		String rtc = "0000";

		if (rtc.equals("0000")) {
			set2OKRtnMsg(botMessageVO, reqVO, false);
		}
		return botMessageVO;
	}

	@Override
	public BotMessageVO carInsuranceCommunicationChangePhoneSet2(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Boolean verificationStatus = true; // 驗證是否成功
		if (StringUtils.isNotBlank(reqVO.getNewPhone())) {

			String s = reqVO.getNewPhone();
			Pattern pattern = Pattern.compile("(09[0-9]{8})||(886[0-9]{9})");
			Matcher matcher = pattern.matcher(s);

			if (matcher.matches()) {
				if (s.indexOf("886") == 0) {
					s = "0" + s.substring(3);
				}
			} else {
				verificationStatus = false;
			}

		} else {
			verificationStatus = false;
		}
		if (verificationStatus) {
			botResponseUtils.setTextResult(botMessageVO, "怪怪的~是不是打錯字了呢？再告訴阿發一次吧~");
		}

		// TODO API-C03 -----

		// ---

		String rtc = "0000";

		if (rtc.equals("0000")) {
			set2OKRtnMsg(botMessageVO, reqVO, false);
		}
		return botMessageVO;
	}

	@Override
	public BotMessageVO carInsuranceCommunicationChangeEmailSet2(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Boolean verificationStatus = true; // 驗證是否成功
		if (StringUtils.isNotBlank(reqVO.getNewEmail())) {
			String s = reqVO.getNewEmail();
			Pattern pattern = Pattern.compile("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");
			Matcher matcher = pattern.matcher(s);

			if (!matcher.matches()) {
				verificationStatus = false;
			}

		} else {
			verificationStatus = false;
		}
		if (verificationStatus) {
			botResponseUtils.setTextResult(botMessageVO, "怪怪的~是不是打錯字了呢？再告訴阿發一次吧~");
		}

		// TODO API-C03 -----

		// ---

		String rtc = "0000";

		if (rtc.equals("0000")) {
			set2OKRtnMsg(botMessageVO, reqVO, false);
		}
		return botMessageVO;
	}

	@Override
	public BotMessageVO carInsuranceCommunicationChangeAddressSet2(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		// API Q2 資料沒有保單資訊，確認後再寫 20191020
		/*
		 * 20191021 早上確認Q2沒有保單資訊 下午得到回覆會有新的格式，卻只有一張保單形式回應，已回覆SA是否與Q1是多張保單形式一樣，SA請客戶確認一下
		 * ，為確保資訊沒有錯誤，靜待窗口回覆佳音。
		 * 
		 * 201910221 1510 確定增加 POLICY_LIST
		 */

		// TODO API-C03 -----

		// ---
		String rtc = "0000";

		if (rtc.equals("0000")) {
			set2OKRtnMsg(botMessageVO, reqVO, false);
		}
		return botMessageVO;
	}

	@Override
	public BotMessageVO carRegistrationInitial(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setTextResult(botMessageVO, "阿發提醒你：</br>1.車籍資料變更一天僅能申請一次 </br>2.需附上行照正面照 </br>3.須為要被保人同一人的保單才可申請保單變更 <br><br><span style=\"color:red\">要前往國泰產險官網的數位服務平台辦理嗎？</span>");
		botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_04, InsQuickReplay.GO_URL_REGISTRATION(reqVO.getSsoId()),
				InsQuickReplay.Change_CS, InsQuickReplay.INIT);
		return botMessageVO;
	}

	@Override
	public BotMessageVO carRegistrationCarSelected(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setTextResult(botMessageVO, "登入成功~阿發馬上來調你的保單資料，稍等一下喔~");

		// API-C02 -----
		// API測試資料 QUERY_TYPE = 3
		// reqVO.setMemberId("S122531322");
		// reqVO.setMemberToken("xWQ2FcU9AYWIPEyIfmsoFNF1fw0PWAOzH4GSgwLLFShs71maLwoDpYnTTuAnFH2K");
		// reqVO.setSessionId("66666");
		// reqVO.setLoginIp("127.0.0.1");

		Map<String, String> map = new HashMap<>();
		map.put("MEMBER_ID", reqVO.getMemberId());
		map.put("MEMBER_TOKEN", reqVO.getMemberToken());
		map.put("QUERY_TYPE", "3");
		map.put("RECEIVE_TYPE", "65");
		map.put("SESSION_ID", reqVO.getSessionId());
		map.put("LOGIN_IP", reqVO.getLoginIp());
		map.put("VIEW_DEVICE", "P");

		Map<String, Object> req = new HashMap<>();
		req.put("map", map);

		Map<String, Object> resp = new HashMap<>();
		resp = insuranceAPIFeignService.getQueryCarList(req);

		// resp = dataTestQueryForType(resp, 3);//QUERY_TYPE = 1 自製測試資料

		Map<String, Object> respDetail = (Map<String, Object>) resp.get("detail");
		Map<String, Object> respDATAS = (Map<String, Object>) respDetail.get("DATAS");

		List<Map<String, Object>> dataList = new ArrayList<>();
		for (Map.Entry<String, Object> entry : respDATAS.entrySet()) {
			Map<String, Object> data = new HashMap<>();
			data.put("carName", entry.getKey());
			Map<String, Object> vals = (Map<String, Object>) entry.getValue();
			data.put("PHONE_CODE", vals.get("PHONE_CODE"));
			data.put("CAR_NO", vals.get("CAR_NO"));
			data.put("DUP_CAR_NO", vals.get("DUP_CAR_NO"));
			data.put("ENGINE_NO", vals.get("ENGINE_NO"));
			data.put("ORIGIN_ISSUE_DATE", vals.get("ORIGIN_ISSUE_DATE"));
			/*
			 * 因ajax傳送資訊會因為List<Map<String, Object>>內包含List<Map<String,
			 * Object>>造成無法依照GS準則放入參數
			 */
			data.put("POLICY_LIST", Object2Json((List<Map<String, Object>>) vals.get("POLICY_LIST")));

			dataList.add(data);
		}

		if (dataList.size() > 0) {
			botResponseUtils.setDoSelectSearchComp(botMessageVO, dataList, "車輛選擇", "6", "","");
		} else {
			botResponseUtils.setTextResult(botMessageVO, "阿發查不到以你為要保人或被保人的有效保單喔！");
			botResponseUtils.setTextResult(botMessageVO, "你可以到<a href=\"javascript:void(0);\" onclick=\"window.open('"+InsUrlGather.ECIE_1000+DataUtils.getRectifySSOId(reqVO.getSsoId())+"');\">bobe線上投保網站</a>，直接投保哦~");
			botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_03, InsQuickReplay.END_CHAT,
					InsQuickReplay.INIT);
		}

		return botMessageVO;
	}

	@Override
	public BotMessageVO carRegistrationCarNoSet1(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setTextResult(botMessageVO, "車牌號碼要改成什麼呢？直接告訴阿發吧。要與行照上的相同哦~");
		return botMessageVO;
	}

	@Override
	public BotMessageVO carRegistrationCarEngineSet1(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setTextResult(botMessageVO, "引擎/車身號碼要改成什麼呢？直接告訴阿發吧。要與行照上的相同哦~");
		return botMessageVO;
	}

	@Override
	public BotMessageVO carRegistrationCarDocumentSet1(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setTextResult(botMessageVO, "發照年月要改成什麼呢？直接告訴阿發吧。要與行照上的相同哦~YYYY-MM-dd");
		return botMessageVO;
	}

	@Override
	public BotMessageVO carRegistrationCarNoSet2(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Boolean verificationStatus = true; // 驗證是否成功
		// TODO 客戶沒有提供驗證所需驗證的部分
		if (verificationStatus) {
			botResponseUtils.setTextResult(botMessageVO, "怪怪的~是不是打錯字了呢？再告訴阿發一次吧~");
		} else {
			// TODO API-C04 -----
			// --
			if (StringUtils.isNotBlank(reqVO.getVehicleFrontBase64())) {
				botResponseUtils.setTextResult(botMessageVO, "這項變更需要拍照上傳行照喔，準備好了嗎？");
				botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_03, InsQuickReplay.CameraOrUPD,
						InsQuickReplay.INIT);
			} else {
				carRegistrationSummaryPage(reqVO);
			}
		}
		return botMessageVO;
	}

	@Override
	public BotMessageVO carRegistrationCarEngineSet2(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Boolean verificationStatus = true; // 驗證是否成功
		// TODO 客戶沒有提供驗證所需驗證的部分
		if (verificationStatus) {
			botResponseUtils.setTextResult(botMessageVO, "怪怪的~是不是打錯字了呢？再告訴阿發一次吧~");
		} else {
			// TODO API-C04 -----
			// --
			if (StringUtils.isNotBlank(reqVO.getVehicleFrontBase64())) {
				botResponseUtils.setTextResult(botMessageVO, "這項變更需要拍照上傳行照喔，準備好了嗎？");
				botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_03, InsQuickReplay.CameraOrUPD,
						InsQuickReplay.INIT);
			} else {
				carRegistrationSummaryPage(reqVO);
			}
		}
		return botMessageVO;
	}

	@Override
	public BotMessageVO carRegistrationCarDocumentSet2(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Boolean verificationStatus = true; // 驗證是否成功

		Pattern pattern = Pattern.compile("[0-9]{0,4}[-]{1}[0-9]{2}[-]{1}[0-9]{2}");
		Matcher matcher = pattern.matcher(reqVO.getNewOriginIssueDate());

		if (!matcher.matches()) {
			verificationStatus = false;
		}
		if (verificationStatus) {
			botResponseUtils.setTextResult(botMessageVO, "怪怪的~是不是打錯字了呢？再告訴阿發一次吧~YYYY-MM-dd");
		} else {
			// TODO API-C04 -----
			// --
			if (StringUtils.isNotBlank(reqVO.getVehicleFrontBase64())) {
				botResponseUtils.setTextResult(botMessageVO, "這項變更需要拍照上傳行照喔，準備好了嗎？");
				botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_03, InsQuickReplay.CameraOrUPD,
						InsQuickReplay.INIT);
			} else {
				carRegistrationSummaryPage(reqVO);
			}
		}
		return botMessageVO;
	}

	@Override
	public BotMessageVO carRegistrationOpenCameraAlbum(CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setInsCameraAlbumComp(botMessageVO, "2");
		return botMessageVO;
	}

	@Override
	public TPIGatewayResult carRegistrationSummaryPage(CarInsuranceRequest reqVO) {

		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		// API-C02 -----
		// API測試資料 QUERY_TYPE = 3
		reqVO.setMemberId("S122531322");
		reqVO.setMemberToken("xWQ2FcU9AYWIPEyIfmsoFPOayYLX3gc7jHNo3AhJBmOVJv8BMtEnXRzgkR46W22c");
		reqVO.setSessionId("66666");
		reqVO.setLoginIp("127.0.0.1");

		Map<String, String> map = new HashMap<>();
		map.put("MEMBER_ID", reqVO.getMemberId());
		map.put("MEMBER_TOKEN", reqVO.getMemberToken());
		map.put("QUERY_TYPE", "3");
		map.put("RECEIVE_TYPE", "65");
		map.put("SESSION_ID", reqVO.getSessionId());
		map.put("LOGIN_IP", reqVO.getLoginIp());
		map.put("VIEW_DEVICE", "P");

		Map<String, Object> req = new HashMap<>();
		req.put("map", map);

		Map<String, Object> resp = new HashMap<>();
		resp = insuranceAPIFeignService.getQueryCarList(req);

		// resp = dataTestQueryForType(resp, 3);//QUERY_TYPE = 1 自製測試資料

		// 以下測試資料
		Map<String, Object> respDetail = (Map<String, Object>) resp.get("detail");
		Map<String, Object> respDATAS = (Map<String, Object>) respDetail.get("DATAS");

		List<Map<String, Object>> dddtest = new ArrayList<>();
		for (Map.Entry<String, Object> entry : respDATAS.entrySet()) {
			Map<String, Object> data = new HashMap<>();
			data.put("carName", entry.getKey());
			Map<String, Object> vals = (Map<String, Object>) entry.getValue();
			data.put("PHONE_CODE", vals.get("PHONE_CODE"));
			data.put("CAR_NO", vals.get("CAR_NO"));
			data.put("DUP_CAR_NO", vals.get("DUP_CAR_NO"));
			data.put("ENGINE_NO", vals.get("ENGINE_NO"));
			data.put("ORIGIN_ISSUE_DATE", vals.get("ORIGIN_ISSUE_DATE"));
			/*
			 * 因ajax傳送資訊會因為List<Map<String, Object>>內包含List<Map<String,
			 * Object>>造成無法依照GS準則放入參數
			 */
			data.put("POLICY_LIST", Object2Json((List<Map<String, Object>>) vals.get("POLICY_LIST")));

			dddtest.add(data);
		}

		reqVO.setMemberId("S122531322");
		reqVO.setNewCarNo("A552-EDQ");
		reqVO.setNewEngineNo("QQ223333445");
		reqVO.setNewOriginIssueDate("2019-03-04");
		reqVO.setAjaxData(dddtest);
		reqVO.setCarNoArray("6666-AS");
		// ---

		if (reqVO.getAjaxData().size() > 0 && StringUtils.isNotBlank(reqVO.getCarNoArray())) {

			String selectedCarNo[] = reqVO.getCarNoArray().split(",");

			// 初始分組
			ContentItem content = botMessageVO.new ContentItem();
			content.setType(30);

			List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
			// 牌卡
			CardItem cards = botMessageVO.new CardItem();
			List<Map<String, Object>> cTextsList = new ArrayList<>();

			// 組牌卡-------

			// 圖
			CImageDataItem cImageData = botMessageVO.new CImageDataItem();
			CImageTextItem cImageTextItem = botMessageVO.new CImageTextItem();
			cImageTextItem.setCImageTextTitle("資料變更");
			cTextsList.add(cImageTextItem.getCImageTexts());
			cImageData.setCImageTexts(cTextsList);
			cImageData.setCImageUrl("card37-01.png");
			cImageData.setCImageTextType(1);

			cards.setCImageData(cImageData.getCImageDatas());// 組裝起來
			// 中間
			List<Map<String, Object>> cTexts = new ArrayList<Map<String, Object>>();

			cards.setCTextType("1");
			cards.setCWidth("segment");

			CTextItem cText = botMessageVO.new CTextItem();

			for (String string : selectedCarNo) {
				List<Map<String, Object>> dataList = reqVO.getAjaxData();
				for (Map<String, Object> data : dataList) {
					if (StringUtils.equals(string, (String) data.get("CAR_NO"))) {
						cText = botMessageVO.new CTextItem();
						cText.setCLabel("車牌號碼");
						if (StringUtils.isNotBlank(reqVO.getNewCarNo())) {
							cText.setCText("<font color='#FF0000'>" + reqVO.getNewCarNo() + "</font>");
						} else {
							cText.setCText((String) data.get("CAR_NO"));
						}
						cTexts.add(cText.getCTexts());
						cards.setCTexts(cTexts);// 組裝起來

						cText = botMessageVO.new CTextItem();
						cText.setCLabel("引擎/車身號碼");
						if (StringUtils.isNotBlank(reqVO.getNewEngineNo())) {
							cText.setCText("<font color='#FF0000'>" + reqVO.getNewEngineNo() + "</font>");
						} else {
							cText.setCText((String) data.get("ENGINE_NO"));
						}
						cTexts.add(cText.getCTexts());
						cards.setCTexts(cTexts);// 組裝起來

						cText = botMessageVO.new CTextItem();
						cText.setCLabel("發照年月");
						if (StringUtils.isNotBlank(reqVO.getNewOriginIssueDate())) {
							cText.setCText("<font color='#FF0000'>" + reqVO.getNewOriginIssueDate() + "</font>");
						} else {
							cText.setCText((String) data.get("ORIGIN_ISSUE_DATE"));
						}
						cTexts.add(cText.getCTexts());
						cards.setCTexts(cTexts);// 組裝起來

						cText = botMessageVO.new CTextItem();
						cText.setCLabel("證件");
						if (StringUtils.isNotBlank(reqVO.getVehicleFrontBase64())) {
							cText.setCText("<font color='#FF0000'>已上傳</font>");
						} else {
							cText.setCText("未上傳");
						}
						cTexts.add(cText.getCTexts());
						cards.setCTexts(cTexts);// 組裝起來
					}
				}
			}

			// 底下按鈕
			/*
			 * 該步驟作法採取將圖片交由WebUser端處理資料，按下確認送出時，會交由ajax直接將資訊交給tpi進行處理，
			 * user選中照片時就會先以base64字串處理web端
			 */
			List<Map<String, Object>> cLinkContents = new ArrayList<Map<String, Object>>();
			Map<String, Object> parameter = new HashMap<>();
			parameter.put("carNoArray", reqVO.getCarNoArray());
			parameter.put("ajaxData", reqVO.getAjaxData());
			parameter.put("memberId", reqVO.getMemberId());
			parameter.put("newCarNo", reqVO.getNewCarNo());
			parameter.put("newEngineNo", reqVO.getNewEngineNo());
			parameter.put("newOriginIssueDate", reqVO.getNewOriginIssueDate());

			QuickReplay q1 = new InsQuickReplay(1, "改車牌號碼", "改車牌號碼", "", "", parameter);
			cLinkContents.add(botResponseUtils.getCLinkListItem(botMessageVO, q1).getCLinkList());
			QuickReplay q2 = new InsQuickReplay(1, "改引擎/車身號碼", "改引擎/車身號碼", "", "", parameter);
			cLinkContents.add(botResponseUtils.getCLinkListItem(botMessageVO, q2).getCLinkList());
			QuickReplay q3 = new InsQuickReplay(1, "改發照年月", "改發照年月", "", "", parameter);
			cLinkContents.add(botResponseUtils.getCLinkListItem(botMessageVO, q3).getCLinkList());
			QuickReplay q4 = new InsQuickReplay(1, "拍照/上傳證件", "拍照/上傳證件", "", "", parameter);
			cLinkContents.add(botResponseUtils.getCLinkListItem(botMessageVO, q4).getCLinkList());

			// 根據 app.js handleLink 跟 handleCLink type 20 處理上傳照片傳遞api的部分
			QuickReplay q5 = new InsQuickReplay(20, "確認送出", "確認送出", "", "", parameter);
			cLinkContents.add(botResponseUtils.getCLinkListItem(botMessageVO, q5).getCLinkList());

			// 這一段要儲存按鈕資訊給flow
			tpiGatewayResult
					.setMsgList(Arrays.asList(q1.getText(), q2.getText(), q3.getText(), q4.getText(), q5.getText()));
			tpiGatewayResult.setOptionList(Arrays.asList(q1, q2, q3, q4, q5));
			// --

			cards.setCLinkList(cLinkContents);// 組裝起來

			cardList.add(cards.getCards());

			content.setCards(cardList);

			botMessageVO.getContent().add(content.getContent());

		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@Override
	public BotMessageVO searchCarChangeList(CarInsuranceRequest reqVO) {

		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		botResponseUtils.setTextResult(vo, "在數位服務平台辦理的車險批改，可以線上查詢進度與紀錄，也可以補上傳文件喔！若是透過業務員或臨櫃辦理的案件，阿發可以請真人客服來幫你哦~");
		botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, InsQuickReplay.GODigitalService(getString(reqVO.getSsoId())),
				InsQuickReplay.A05_OTHERHELP, InsQuickReplay.Change_CS);
		return vo;
	}
	
	public String getString(Object str) {
		String returnStr = "";
		if (str != null) {
			returnStr = str.toString();
		}

		return returnStr;
	}

	public static Map<String, Object> returnToMap(String json) {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> rumap = new HashMap<String, Object>();
		try {
			rumap = mapper.readValue(json, new TypeReference<HashMap<String, Object>>() {
			});
			return rumap;
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return rumap;
	}

}
