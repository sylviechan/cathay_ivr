package com.tp.tpigateway.modules.cathaysec.controller;

import com.alibaba.fastjson.JSON;
import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysec.PasswordRequest;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.common.util.PropUtils;
import com.tp.tpigateway.modules.cathaysec.service.PasswordRelatedService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/*
    密碼相關流程
 */

@RestController
@RequestMapping("/PasswordRelated")
public class PasswordRelatedController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(PasswordRelatedController.class);
	private static final String SOURCE = "password";

	@Autowired
	PasswordRelatedService passwordRelatedService;

	@RequestMapping(value = "/related", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult pwdCmd(@RequestBody PasswordRequest reqVO) {
		String cmd = reqVO.getCmd();
		BotMessageVO botMessageVO = null;
		Map<String, Object> result = new HashMap<String, Object>();
		long costTimeStart = System.currentTimeMillis();

		// 密碼相關牌卡-0
		if (cmd.equals("PasswordProcessKey")) {
			botMessageVO = passwordRelatedService.passwordRelated(reqVO);
		} else if (cmd.equals("PasswordProcessKey_QA")) {
			botMessageVO = passwordRelatedService.updSpeechPwd(reqVO);
		}
		// 密碼修改流程
		// PasswordProcessKey_Change:跳出修改密碼
		// PasswordProcessKey_Change_API:修改密碼
		else if (cmd.equals("PasswordProcessKey_Change")) {
			botMessageVO = passwordRelatedService.pswdChangeInit(reqVO);
		} else if (cmd.equals("PasswordProcessKey_Change_API")) {
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();

			requireParams.put("IDNO", reqVO.getIdno());
			requireParams.put("AccountType", reqVO.getAccountType());
			requireParams.put("OldPassword", reqVO.getOldPassword());
			requireParams.put("NewPassword", reqVO.getNewPassword());
			requireParams.put("IP", reqVO.getIp());
			requireParams.put("Channel", PropUtils.getApiChannel());

			// 必要參數檢核
			if (checkParams(requireParams)) {
				requireParams.put("FunctionID", "RB04");

				result = passwordRelatedService.getRB04(reqVO, requireParams);

				logger.info("[" + SOURCE + "_Call_PasswordProcessKey_Change_API]:" + result.toString() + " ;time ="
						+ (System.currentTimeMillis() - costTimeStart) + " ms");

				String ResultCode = result.get("ResultCode").toString();

				if (ResultCode.equals("0000")) {
					botMessageVO = passwordRelatedService.pswdChangeSuccess(reqVO);
				} else if (ResultCode.equals("00021C")) {
					botMessageVO = passwordRelatedService.passwordRelatedLock(reqVO);
				} else {
					botMessageVO = passwordRelatedService.pswdChangeFail(reqVO);
				}

			} else {
				result.put("ResultCode", "9998");
				result.put("ResultMessage", "缺少必要參數");
				return TPIGatewayResult.ok().put("isRtMsg", true).setData(JSON.toJSONString(result));
			}
		}

		// 密碼解鎖流程
		// PasswordProcessKey_OnLock:跳出解鎖密碼
		// PasswordProcessKey_OnLock_Phone_API:申請解鎖密碼
		// PasswordProcessKey_OnLock_API:解鎖密碼
		else if (cmd.equals("PasswordProcessKey_OnLock")) {
			botMessageVO = passwordRelatedService.pswdOnLockInit(reqVO);
		} else if (cmd.equals("PasswordProcessKey_OnLock_Phone_API")) {
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("IDNO", reqVO.getIdno());
			requireParams.put("Birthday", reqVO.getBirthday());
			requireParams.put("IP", reqVO.getIp());
			requireParams.put("Channel", PropUtils.getApiChannel());

			// 必要參數檢核
			if (checkParams(requireParams)) {
				requireParams.put("FunctionID", "RB05");

				result = passwordRelatedService.getRB05(reqVO, requireParams);

				logger.info("[" + SOURCE + "_Call_PasswordProcessKey_OnLock_Phone_API]:" + result.toString()
						+ " ;time =" + (System.currentTimeMillis() - costTimeStart) + " ms");

				String ResultCode = result.get("ResultCode").toString();

				if (ResultCode.equals("0000")) {
					return TPIGatewayResult.ok().put("isRtMsg", true).setData(JSON.toJSONString(result).toString());
				} else if (ResultCode.equals("9002") || ResultCode.equals("9910") || ResultCode.equals("9999")) {
					botMessageVO = passwordRelatedService.pswdOnLockFail(reqVO);
				} else if (ResultCode.equals("9903") || ResultCode.equals("9904") || ResultCode.equals("9931")) {
					botMessageVO = passwordRelatedService.pswdOnLockCanNot(reqVO);
				} else if (ResultCode.equals("9905")) {
					botMessageVO = passwordRelatedService.pswdOnLockFullAmount(reqVO);
				} else if (ResultCode.equals("9000") || ResultCode.equals("9930")) {
					return TPIGatewayResult.ok().put("isRtMsg", true).setData(JSON.toJSONString(result));
				} else {
					return TPIGatewayResult.ok().put("isRtMsg", true).setData(JSON.toJSONString(result));
				}

			} else {
				result.put("ResultCode", "9998");
				result.put("ResultMessage", "缺少必要參數");
				return TPIGatewayResult.ok().put("isRtMsg", true).setData(JSON.toJSONString(result));
			}

		} else if (cmd.equals("PasswordProcessKey_OnLock_API")) {
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("IDNO", reqVO.getIdno());
			requireParams.put("OTP", reqVO.getOtp());
			requireParams.put("IP", reqVO.getIp());
			requireParams.put("Channel", PropUtils.getApiChannel());

			// 必要參數檢核
			if (checkParams(requireParams)) {
				requireParams.put("FunctionID", "RB06");

				result = passwordRelatedService.getRB06(reqVO, requireParams);

				logger.info("[" + SOURCE + "_Call_PasswordProcessKey_OnLock_Phone_API]:" + result.toString()
						+ " ;time =" + (System.currentTimeMillis() - costTimeStart) + " ms");

				String ResultCode = result.get("ResultCode").toString();

				if (ResultCode.equals("0000")) {
					botMessageVO = passwordRelatedService.pswdOnLockSuccess(reqVO);
				} else if (ResultCode.equals("9900") || ResultCode.equals("9902") || ResultCode.equals("9910")) {
					botMessageVO = passwordRelatedService.pswdOnLockFail(reqVO);
				} else if (ResultCode.equals("9906") || ResultCode.equals("9908") || ResultCode.equals("9909")) {
					return TPIGatewayResult.ok().put("isRtMsg", true).setData(JSON.toJSONString(result));
				} else {
					return TPIGatewayResult.ok().put("isRtMsg", true).setData(JSON.toJSONString(result));
				}

			} else {
				result.put("ResultCode", "9998");
				result.put("ResultMessage", "缺少必要參數");
				return TPIGatewayResult.ok().put("isRtMsg", true).setData(JSON.toJSONString(result));
			}

		}

		// 密碼補發流程
		// PasswordProcessKey_Reissue:跳出補發密碼
		// PasswordProcessKey_Reissue_Verify_API:驗證身份
		// PasswordProcessKey_Reissue_Phone_API:申請補發密碼
		// PasswordProcessKey_Reissue_API:補發密碼
		else if (cmd.equals("PasswordProcessKey_Reissue")) {
			botMessageVO = passwordRelatedService.pswdReissueInit(reqVO);
		} else if (cmd.equals("PasswordProcessKey_Reissue_Verify_API")) {
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("IDNO", reqVO.getIdno());
			requireParams.put("Birthday", reqVO.getBirthday());
			requireParams.put("Type", reqVO.getType());
			requireParams.put("IP", reqVO.getIp());
			requireParams.put("Channel", PropUtils.getApiChannel());

			// 必要參數檢核
			if (checkParams(requireParams)) {
				requireParams.put("FunctionID", "RB07");

				result = passwordRelatedService.getRB07(reqVO, requireParams);

				logger.info("[" + SOURCE + "_Call_PasswordProcessKey_OnLock_Phone_API]:" + result.toString()
						+ " ;time =" + (System.currentTimeMillis() - costTimeStart) + " ms");

				String ResultCode = result.get("ResultCode").toString();

				if (ResultCode.equals("0000")) {
					return TPIGatewayResult.ok().put("isRtMsg", true).setData(JSON.toJSONString(result));
				} else if (ResultCode.equals("9911") || ResultCode.equals("9912") || ResultCode.equals("9913")
						|| ResultCode.equals("9914")) {
					return TPIGatewayResult.ok().put("isRtMsg", true).setData(JSON.toJSONString(result));
				} else if (ResultCode.equals("9915") || ResultCode.equals("9919")) {
					return TPIGatewayResult.ok().put("isRtMsg", true).setData(JSON.toJSONString(result));
				} else if (ResultCode.equals("9900") || ResultCode.equals("9902") || ResultCode.equals("9910")) {
					botMessageVO = passwordRelatedService.pswdReissueFail(reqVO);
				} else if (ResultCode.equals("9916") || ResultCode.equals("9917") || ResultCode.equals("9918")
						|| ResultCode.equals("9920")) {
					botMessageVO = passwordRelatedService.pswdReissueCanNot(reqVO, ResultCode);
				} else if (ResultCode.equals("9921")) {
					botMessageVO = passwordRelatedService.pswdReissueFullAmount(reqVO);
				} else {
					return TPIGatewayResult.ok().put("isRtMsg", true).setData(JSON.toJSONString(result));
				}

			} else {
				result.put("ResultCode", "9998");
				result.put("ResultMessage", "缺少必要參數");
				return TPIGatewayResult.ok().put("isRtMsg", true).setData(JSON.toJSONString(result));
			}

		} else if (cmd.equals("PasswordProcessKey_Reissue_Phone_API")) {
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("IDNO", reqVO.getIdno());
			requireParams.put("Birthday", reqVO.getBirthday());
			requireParams.put("Type", reqVO.getType());
			requireParams.put("Name", reqVO.getName());
			requireParams.put("Ctel", reqVO.getCtel());
			requireParams.put("IP", reqVO.getIp());
			requireParams.put("Channel", PropUtils.getApiChannel());

			// 必要參數檢核
			if (checkParams(requireParams)) {
				requireParams.put("FunctionID", "RB08");

				result = passwordRelatedService.getRB08(reqVO, requireParams);

				logger.info("[" + SOURCE + "_Call_PasswordProcessKey_OnLock_Phone_API]:" + result.toString()
						+ " ;time =" + (System.currentTimeMillis() - costTimeStart) + " ms");

				String ResultCode = result.get("ResultCode").toString();

				if (ResultCode.equals("0000")) {
					return TPIGatewayResult.ok().put("isRtMsg", true).setData("0000");
				} else {
					botMessageVO = passwordRelatedService.pswdReissueFail(reqVO);
				}

			} else {
				result.put("ResultCode", "9998");
				result.put("ResultMessage", "缺少必要參數");
				return TPIGatewayResult.ok().put("isRtMsg", true).setData(JSON.toJSONString(result));
			}

		} else if (cmd.equals("PasswordProcessKey_Reissue_API")) {
			// 設定必要參數
			Map<String, String> requireParams = new HashMap<String, String>();
			requireParams.put("IDNO", reqVO.getIdno());
			requireParams.put("OTP", reqVO.getOtp());
			requireParams.put("Type", reqVO.getType());
			requireParams.put("IP", reqVO.getIp());
			requireParams.put("Channel", PropUtils.getApiChannel());

			// 必要參數檢核
			if (checkParams(requireParams)) {
				requireParams.put("FunctionID", "RB09");

				result = passwordRelatedService.getRB09(reqVO, requireParams);

				logger.info("[" + SOURCE + "_Call_PasswordProcessKey_OnLock_Phone_API]:" + result.toString()
						+ " ;time =" + (System.currentTimeMillis() - costTimeStart) + " ms");

				String ResultCode = result.get("ResultCode").toString();

				if (ResultCode.equals("0000")) {
					String epass64 = result.get("Epass").toString();

					Base64.Decoder decoder = Base64.getDecoder();
					try {
						String epass = new String(decoder.decode(epass64), "utf-8");
						botMessageVO = passwordRelatedService.pswdReissueSuccess(reqVO, epass);
					} catch (UnsupportedEncodingException e) {
						// TODO 自動產生的 catch 區塊
						e.printStackTrace();
					}
				} else if (ResultCode.equals("9902") || ResultCode.equals("9910") || ResultCode.equals("9922")
						|| ResultCode.equals("9923") || ResultCode.equals("9924") || ResultCode.equals("9925")) {
					botMessageVO = passwordRelatedService.pswdReissueFail(reqVO);
				} else if (ResultCode.equals("9906") || ResultCode.equals("9908") || ResultCode.equals("9909")) {
					botMessageVO = passwordRelatedService.pswdReissueRt(reqVO);
				} else {
					return TPIGatewayResult.ok().put("isRtMsg", true).setData(JSON.toJSONString(result));
				}

			} else {
				result.put("ResultCode", "9998");
				result.put("ResultMessage", "缺少必要參數");
				return TPIGatewayResult.ok().put("isRtMsg", true).setData(JSON.toJSONString(result));
			}

		} else if (cmd.indexOf("_SenderPhone") > -1) {
			botMessageVO = passwordRelatedService.pswdSenderPhoneF(reqVO);
		} else if (cmd.indexOf("_Stop") > -1) {
			botMessageVO = passwordRelatedService.pswdStop(reqVO);
		}

		return TPIGatewayResult.ok().put("isRtMsg", false).setData(botMessageVO);
	}

}
