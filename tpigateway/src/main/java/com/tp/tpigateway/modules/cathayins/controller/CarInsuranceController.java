package com.tp.tpigateway.modules.cathayins.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathayins.CarInsuranceRequest;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.cathayins.service.InsCarInsuranceService;

@RestController
@RequestMapping("/CarInsurance")
public class CarInsuranceController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(CarInsuranceController.class);

	@Autowired
	InsCarInsuranceService insCarInsuranceService;

	@RequestMapping(value = "/related")
	public TPIGatewayResult changeOwnerInformation(@RequestBody CarInsuranceRequest reqVO) {

		String cmd = reqVO.getCmd();
		BotMessageVO botMessageVO = null;

		// 第一組功能改車主資料 QUERY_TYPE = 1

		// 我要改車主資料 page 2
		if (cmd.equals("memberInformation_Initial")) {
			botMessageVO = insCarInsuranceService.memberInformationInitial(reqVO);
		}
		// 車輛選擇 page 5 [必須登入後]
		else if (cmd.equals("memberInformation_CarSelected")) {
			botMessageVO = insCarInsuranceService.memberInformationCarSelected(reqVO);
		}
		// 開始更改資料-名字 page 7
		else if (cmd.equals("memberInformation_ChangeNameSet1")) {
			botMessageVO = insCarInsuranceService.memberInformationChangeNameSet1(reqVO);
		}
		// 開始更改資料-名字-要求拍證件 page 7
		else if (cmd.equals("memberInformation_ChangeNameSet2")) {
			botMessageVO = insCarInsuranceService.memberInformationChangeNameSet2(reqVO);
		}
		// 開始更改資料-名字-拍證件功能呼叫 page 7
		else if (cmd.equals("memberInformation_OpenCameraAlbum")) {
			// 彈跳視窗
			botMessageVO = insCarInsuranceService.memberInformationOpenCameraAlbum(reqVO);
		}

		// 開始更改資料-婚姻確認 page11
		else if (cmd.equals("memberInformation_ChangeMarriageSet1")) {
			botMessageVO = insCarInsuranceService.memberInformationChangeMarriageSet1(reqVO);
		}
		// 開始更改資料-婚姻 page12
		else if (cmd.equals("memberInformation_ChangeMarriage")) {
			botMessageVO = insCarInsuranceService.memberInformationChangeMarriage(reqVO);
		}

		// 第二組功能改車險通訊資料 QUERY_TYPE = 2

		// 改車險通訊資料 page 15
		else if (cmd.equals("carInsuranceCommunication_Initial")) {
			botMessageVO = insCarInsuranceService.carInsuranceCommunicationInitial(reqVO);
		}
		// 顯示車輛通訊資料畫面 page 18 [必須登入後]
		else if (cmd.equals("carInsuranceCommunication_CarSelected")) {
			botMessageVO = insCarInsuranceService.carInsuranceCommunicationCarSelected(reqVO);
		}
		// 改市話提問 page 21
		else if (cmd.equals("carInsuranceCommunication_ChangeCellularPhoneSet1")) {
			botMessageVO = insCarInsuranceService.carInsuranceCommunicationChangeCellularPhoneSet1(reqVO);
		}
		// 驗證 改市話 page 21
		else if (cmd.equals("carInsuranceCommunication_ChangeCellularPhoneSet2")) {
			botMessageVO = insCarInsuranceService.carInsuranceCommunicationChangeCellularPhoneSet2(reqVO);
		}

		// 改手機提問 page 22
		else if (cmd.equals("carInsuranceCommunication_ChangePhoneSet1")) {
			botMessageVO = insCarInsuranceService.carInsuranceCommunicationChangePhoneSet1(reqVO);
		}
		// 驗證 改手機 page 22
		else if (cmd.equals("carInsuranceCommunication_ChangePhoneSet2")) {
			botMessageVO = insCarInsuranceService.carInsuranceCommunicationChangePhoneSet2(reqVO);
		}

		// 改信箱提問 page 23
		else if (cmd.equals("carInsuranceCommunication_ChangeEmailSet1")) {
			botMessageVO = insCarInsuranceService.carInsuranceCommunicationChangeEmailSet1(reqVO);
		}
		// 驗證 改信箱 page 23
		else if (cmd.equals("carInsuranceCommunication_ChangeEmailSet2")) {
			botMessageVO = insCarInsuranceService.carInsuranceCommunicationChangeEmailSet2(reqVO);
		}

		// 改地址提問 page 24
		else if (cmd.equals("carInsuranceCommunication_ChangeAddressSet1")) {
			// 彈跳視窗
			botMessageVO = insCarInsuranceService.carInsuranceCommunicationChangeAddressSet1(reqVO);
		}
		// 驗證 改地址 page 24
		else if (cmd.equals("carInsuranceCommunication_ChangeAddressSet2")) {
			botMessageVO = insCarInsuranceService.carInsuranceCommunicationChangeAddressSet2(reqVO);
		}

		// 第三組功能改車籍資料 QUERY_TYPE = 3
		// 我要改車籍資料 page 27
		else if (cmd.equals("carRegistration_Initial")) {
			botMessageVO = insCarInsuranceService.carRegistrationInitial(reqVO);
		}

		// 車輛選擇 [必須登入後] page 30
		else if (cmd.equals("carRegistration_CarSelected")) {
			botMessageVO = insCarInsuranceService.carRegistrationCarSelected(reqVO);
		}

		// 改車牌 page 32
		else if (cmd.equals("carRegistration_CarNoSet1")) {
			botMessageVO = insCarInsuranceService.carRegistrationCarNoSet1(reqVO);
		}

		// 改引擎 page 33
		else if (cmd.equals("carRegistration_CarEngineSet1")) {
			botMessageVO = insCarInsuranceService.carRegistrationCarEngineSet1(reqVO);
		}

		// 改發照 page 34
		else if (cmd.equals("carRegistration_CarDocumentSet1")) {
			botMessageVO = insCarInsuranceService.carRegistrationCarDocumentSet1(reqVO);
		}

		// 改車牌 page 32
		else if (cmd.equals("carRegistration_CarNoSet2")) {
			botMessageVO = insCarInsuranceService.carRegistrationCarNoSet2(reqVO);
		}

		// 改引擎 page 33
		else if (cmd.equals("carRegistration_CarEngineSet2")) {
			botMessageVO = insCarInsuranceService.carRegistrationCarEngineSet2(reqVO);
		}

		// 改發照 page 34
		else if (cmd.equals("carRegistration_CarDocumentSet2")) {
			botMessageVO = insCarInsuranceService.carRegistrationCarDocumentSet2(reqVO);
		}

		// 車證上傳 page 36
		else if (cmd.equals("carRegistration_OpenCameraAlbum")) {
			botMessageVO = insCarInsuranceService.carRegistrationOpenCameraAlbum(reqVO);
		}

		// 結果整合頁
		else if (cmd.equals("carRegistration_SummaryPage")) {
			return insCarInsuranceService.carRegistrationSummaryPage(reqVO);
		}

		/*
		 * 我要查車險批改進度 page 67
		 * input cmd
		 * output 文字訊息、快速回復按鈕 InsQuickReplay.GODigitalService, InsQuickReplay.A05_OTHERHELP
		 */
		else if (cmd.equals("searchCarChangeList")) {
			botMessageVO = insCarInsuranceService.searchCarChangeList(reqVO);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	// 開始更改資料-名字-證件上傳 page 7
	@RequestMapping(value = "/cameraAlbumUPD")
	public TPIGatewayResult cameraAlbumUPD(@RequestBody CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = null;
		Boolean isVehicleCertificate = false;

		// 將送來資訊整理一下 身分證正面
		if (StringUtils.isNotBlank(reqVO.getIdImgPositive())) {
			reqVO.setIdImgPositive(reqVO.getIdImgPositive().substring(reqVO.getIdImgPositive().indexOf(",") + 1));
		}
		// 將送來資訊整理一下 身分證反面
		if (StringUtils.isNotBlank(reqVO.getIdImgNegative())) {
			reqVO.setIdImgNegative(reqVO.getIdImgNegative().substring(reqVO.getIdImgNegative().indexOf(",") + 1));
		}

		if (StringUtils.isNotBlank(reqVO.getVehicleFrontBase64())) {
			isVehicleCertificate = true;
			reqVO.setVehicleFrontBase64(
					reqVO.getVehicleFrontBase64().substring(reqVO.getIdImgNegative().indexOf(",") + 1));
		}

		if (isVehicleCertificate) {
			// 車輛證件
			// botMessageVO = insCarInsuranceService.cameraAlbumUPD(reqVO);
		} else {
			// 身分證件
			botMessageVO = insCarInsuranceService.cameraAlbumUPD(reqVO, isVehicleCertificate);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	// 更改車主哪一項資料確認頁 page 6
	@RequestMapping(value = "/changeCarMemberCheck")
	public TPIGatewayResult changeCarMemberCheck(@RequestBody CarInsuranceRequest reqVO) {
		return insCarInsuranceService.changeCarMemberCheck(reqVO);
	}

	// 更改車險通訊哪一項資料確認頁面 page 19
	@RequestMapping(value = "/changeCarInsuranceCheck")
	public TPIGatewayResult changeCarInsuranceCheck(@RequestBody CarInsuranceRequest reqVO) {
		return insCarInsuranceService.changeCarInsuranceCheck(reqVO);
	}

	// 更改車險車籍哪一項資料確認頁面 page 31
	@RequestMapping(value = "/changeCarRegistrationCheck")
	public TPIGatewayResult changeCarRegistrationCheck(@RequestBody CarInsuranceRequest reqVO) {
		return insCarInsuranceService.changeCarRegistrationCheck(reqVO);
	}

	// 更改車籍轉用20按鈕api page 39
	@RequestMapping(value = "/changeCarRegistration")
	public TPIGatewayResult changeCarRegistration(@RequestBody CarInsuranceRequest reqVO) {
		BotMessageVO botMessageVO = null;
		botMessageVO = insCarInsuranceService.changeCarRegistration(reqVO);
		return TPIGatewayResult.ok().setData(botMessageVO);
	}

}
