package com.tp.tpigateway.modules.cathaysec.service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysec.FallBackRequest;
import com.tp.common.model.cathaysec.RegularRequest;

import java.util.Map;

public interface RegularService {

	/**
	 * For 台股定期定額-申購委託(RB01)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	Map<String, Object> getRB01(RegularRequest reqVO, Map<String, String> param);
	
	/**
	 * For 台股定期定額-簽署含申購委託(RB27)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	Map<String, Object> getRB27(RegularRequest reqVO, Map<String, String> param);
	
	/**
	 * For 台股定期定額-推薦股票查詢(RB02)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getRB02(RegularRequest reqVO, Map<String, String> param);
	
	/**
	 * For 台股定期定額-合約簽署狀態查詢(RB03)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	Map<String, Object> getRB03(RegularRequest reqVO, Map<String, String> param);
	
	/**
	 * For 台股定期定額-標的是否開放定期定額(RB10)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getRB10(RegularRequest reqVO, Map<String, String> param);
	
	/**
	 * For 台股定期定額-單一股票有效申購查詢(RB11)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getRB11(RegularRequest reqVO, Map<String, String> param);
	
	/**
	 * For 證券帳戶-銀行餘額查詢(RB12)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getRB12(RegularRequest reqVO, Map<String, String> param);
	
	/**
	 * For 台股定期定額-可申購股票查詢(RB17)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getRB17(RegularRequest reqVO, Map<String, String> param);
	
	/**
	 * For 台股定期定額-績效回測(RB18)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getRB18(RegularRequest reqVO, Map<String, String> param);	
	
	/**
	 * For 台股定期定額-有效申購查詢(RB19)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getRB19(RegularRequest reqVO, Map<String, String> param);
	
	/**
	 * For 阿發問你問題(RB22)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getRB22(RegularRequest reqVO, Map<String, String> param);
	
	/**
	 * For 阿發問你答題(RB23)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	BotMessageVO getRB23(RegularRequest reqVO, Map<String, String> param);

	TPIGatewayResult chooseCmd(RegularRequest reqVO);

	TPIGatewayResult rb02BuyOk(RegularRequest reqVO);

	TPIGatewayResult rb02Check(FallBackRequest reqVO);
	/**
	  *  定期定額相關-WEB動作
	 * @param reqVO
	 * @return
	 */
	TPIGatewayResult chooseWebCmd(RegularRequest reqVO);
}
