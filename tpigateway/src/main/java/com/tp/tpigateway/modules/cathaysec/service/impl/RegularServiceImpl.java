package com.tp.tpigateway.modules.cathaysec.service.impl;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotMessageVO.CImageDataItem;
import com.tp.common.model.BotMessageVO.CImageTextItem;
import com.tp.common.model.BotMessageVO.CTextItem;
import com.tp.common.model.BotMessageVO.CardItem;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysec.FallBackRequest;
import com.tp.common.model.cathaysec.RegularRequest;
import com.tp.common.util.DataUtils;
import com.tp.common.util.DateUtils;
import com.tp.common.util.QuickReplay;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.common.util.PropUtils;
import com.tp.tpigateway.modules.cathaysec.fegin.RegularAPIFeignService;
import com.tp.tpigateway.modules.cathaysec.model.SecQuickReplay;
import com.tp.tpigateway.modules.cathaysec.mybatis.service.IStockDataService;
import com.tp.tpigateway.modules.cathaysec.service.RegularService;

@Service("regularService")
public class RegularServiceImpl implements RegularService{
	private Logger logger = LoggerFactory.getLogger(getClass());
	private static final String[] RB02_DETAIL = {"StockName:股票名稱", "KindDes:股票類別", "SDate:啟用日期", "CData:停止日期", "MarketTypeDes:市場別"};
	private static final String[] RB02_DETAIL_TEST = {"ClosePrice:股價:F", "RiseFallPcnt:漲跌幅:T", "EPS:EPS:F", "Yields:預估殖利率:F"};

	@Autowired
	BotResponseUtils botResponseUtils;

	@Autowired
	RegularAPIFeignService regularAPIFeignService;

	@Autowired
	IStockDataService stockDataService;

	@Override
    public TPIGatewayResult chooseCmd(RegularRequest reqVO) {
        BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

        TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

        String cmd = getString(reqVO.getCmd()).toLowerCase();
        
        switch (cmd) {        	
	        case "rb02_buy_money":       	
	        	botResponseUtils.setTextResult(vo, "你一個月想投資多少錢呢？（最少1,000元）"); 	        	
	            break;
	        case "rb02_buy_money_error":       	
	        	botResponseUtils.setTextResult(vo, "申請金額以千元為單位，最少1,000元呦～"); 	        	
	            break;
	        case "rb02_buy_money_limit_error":       	
	        	botResponseUtils.setTextResult(vo, "定期定額單次扣款上限為999,999,000。請重新輸入"); 	        	
	            break;
	        case "rb02_buy_month":       	
	        	botResponseUtils.setTextResult(vo, "好的，你想要在每個月幾號扣款呢？"); 
	        	botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_02, SecQuickReplay.RB_MONTH_6
	        			, SecQuickReplay.RB_MONTH_16, SecQuickReplay.RB_MONTH_26);
	            break;
	        case "rb02_buy_month_error":       	
	        	botResponseUtils.setTextResult(vo, "請輸入正確的日期（6,16,26）！");
	        	botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_02, SecQuickReplay.RB_MONTH_6
	        			, SecQuickReplay.RB_MONTH_16, SecQuickReplay.RB_MONTH_26);
	            break;
	        case "rb02_detail":
	        	botResponseUtils.setTextResult(vo, "定期定額是什麼...不知道欸～");
	            break;
	        case "rb02_search":       	
	        	botResponseUtils.setTextResult(vo, "查看我的定期定額...API在哪裡呢？");
	            break;
	        case "rb02_select":  	        	
	        	botResponseUtils.setTextResult(vo, "查看個股資訊（" + reqVO.getStockId() + "）API在哪裡呢？");
	            break;
	        case "rb017_check":  	        	
	        	botResponseUtils.setTextResult(vo, "你想看股票還是ETF呢？");

	        	QuickReplay q1 = SecQuickReplay.RB17_1;
				QuickReplay q2 = SecQuickReplay.RB17_2;

	        	botResponseUtils.setQuickReplayResult(vo, q1, q2);

				tpiGatewayResult.setMsgList(Arrays.asList(q1.getText(), q2.getText()));
				tpiGatewayResult.setOptionList(Arrays.asList(q1, q2));

	            break;
	        case "rb017_check_send":
	        	vo = rb17_check_send(reqVO);
	            break;
	        case "rb019_check_send":
	        	vo = rb19_check_send(reqVO);
	            break;
	        case "rb_agent_faq":
	        	botResponseUtils.setTextResult(vo, "TODO [FAQ] 什麼是委任帳戶");
	            break;
	        case "rb03_get":
	        	botResponseUtils.setTextResult(vo, "TODO 引導至E櫃台簽屬SIP合約");
	            break;
	        case "rb23_close":
	        	botResponseUtils.setTextResult(vo, "下次有問題再來找阿發唷");        
	        	break;
	        case "rb_note":
	        	botResponseUtils.setTextResult(vo, "TODO [FAQ] 定期定額扣款注意事項");        
	        	break;
	        case "rb_sip_note":
	        	// TODO 阿發內嵌靜態網頁
	        	vo = rb_sip_note(reqVO);	        	  
	        	break;
	        default:
	        	botResponseUtils.setTextResult(vo, "還有什麼需要阿發為你服務的呢？"); 
	            break;
	    }
 		
        return tpiGatewayResult.setData(vo);
    }
	
	@Override
    public TPIGatewayResult chooseWebCmd(RegularRequest reqVO) {
        BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

        TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

        String cmd = getString(reqVO.getCmd()).toLowerCase();
        
        switch (cmd) {        	
	        case "rb02_buy":       	
	        	botResponseUtils.setTextResult(vo, "阿！接下來的任務要在APP才能完成");
	        	botResponseUtils.setQuickReplayResult(vo,BotMessageVO.LTYPE_04,SecQuickReplay.E_APP_DOWNLOAD);
	            break;
	        default:
	        	botResponseUtils.setTextResult(vo, "還有什麼需要阿發為你服務的呢？"); 
	            break;
	    }
 		
        return tpiGatewayResult.setData(vo);
    }

	@Override
	public TPIGatewayResult rb02BuyOk(RegularRequest reqVO) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> result = getRB17_select(reqVO.getSymbol());

		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		//取得股票名稱
//      String stockId = reqVO.getStockId();
//		Map<String, StockData> allStockIdData = stockDataService.getAllStockIdData();
//		StockData stockData = allStockIdData.get(stockId);
//		String stockName = stockData.getStockName();
		botResponseUtils.setTextResult(vo, "好的，以下是你的定期定額申購資訊");

		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		CardItem cards = vo.new CardItem();
		cards.setCLinkType(1);
		cards.setCTextType("1");
		cards.setCWidth(BotMessageVO.CWIDTH_250);

		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_GENERIC);
		cImageData.setCImageTextType(2);

		List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
		CImageTextItem cImageText = vo.new CImageTextItem();

		cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("股票名稱");
		cImageText.setCImageText(result.get("SymbolName").toString());
		cImageTexts.add(cImageText.getCImageTexts());

		/*
		cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("扣款帳號");
		cImageText.setCImageText("1122334455667788");
		cImageTexts.add(cImageText.getCImageTexts());
		*/

		cImageData.setCImageTexts(cImageTexts);
		cards.setCImageData(cImageData.getCImageDatas());

		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		for (int i = 1; i <= 2; i++) {
			CTextItem cText = vo.new CTextItem();
//			if (i == 1) {
//				String tDate = result.get("TDate").toString();
//				tDate = tDate.substring(4, 6) + "/" + tDate.substring(6, 8);
//
//				cText.setCLabel("股價(" + tDate + ")");
//				cText.setCText(result.get("ClosePrice").toString());
//			} else
			if (i == 1) {
				cText.setCLabel("申購金額（每筆）");
				cText.setCText(changeMoneyString(reqVO.getMoney()) + "元");
			} else if (i == 2) {
				cText.setCLabel("每月交易日");
				cText.setCText("每月" + reqVO.getDay() + "日");
			}
			cTextList.add(cText.getCTexts());
		}
		cards.setCTexts(cTextList);

		QuickReplay q1 = SecQuickReplay.MODIFY_RSP(reqVO.getSymbol());
		QuickReplay q2 = SecQuickReplay.RB_BUY(reqVO.getSymbol());

		//傳給flow紀錄
		tpiGatewayResult.setMsgList(Arrays.asList(q1.getText(), q2.getText()));
		tpiGatewayResult.setOptionList(Arrays.asList(q1, q2));

		cards.setCLinkList(botResponseUtils.getCLinkList(vo, q1, q2));
		cardList.add(cards.getCards());

		botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);

		return tpiGatewayResult.setData(vo);
	}

	@Override
    public Map<String, Object> getRB01(RegularRequest reqVO, Map<String, String> param) {
        BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
        Map<String, Object> result = new HashMap<String, Object>();  
        Map<String, Object> returnResult = new HashMap<String, Object>();
        
        // 檢查帳號是否已簽署過SIP合約
        Map<String, Object> rb03Result = getRB03(reqVO, param); 
        String sipFlag = rb03Result.get("sipFlag").toString();
        
        if("Y".equals(sipFlag)) {
        	param.put("FunctionID", "RB01");
        	String rpjson = regularAPIFeignService.getRB01(JSON.toJSONString(param));
            
        	/*
     		if(StringUtils.isNotBlank(rpjson)) { 	
     			result= returnToMap(rpjson); 	
     			String resultCode = getString(result.get("ResultCode"));
     			String resultMessage = getString(result.get("ResultMessage"));
     			
     			if(resultCode.equals("0000")) { 				
    				botResponseUtils.setTextResult(vo, "申請成功了！預計在1個工作天後生效^0^");
    				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, SecQuickReplay.RB18_ELSE2
    						, SecQuickReplay.RB_NOTE);				
     			} else {
     				botResponseUtils.setTextResult(vo, "挖..申購失敗了..[" + resultMessage + "]");
     			} 			
    		}else {
    			String replyText = "RB01 api error";
    			botResponseUtils.setTextResult(vo, replyText);	
    		}
    		*/
        	
        	if(StringUtils.isNotBlank(rpjson)) { 	
        		result= returnToMap(rpjson); 	
        		String resultCode = getString(result.get("ResultCode"));
        		String resultMessage = getString(result.get("ResultMessage"));
        		
        		Map<String, Object> symbolResult = getRB17_select(reqVO.getSymbol());
        		
        		if(resultCode.equals("0000")) {
        			// 單張牌卡
        			List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
        			CardItem cards = vo.new CardItem();
        			cards.setCName("");
        			cards.setCWidth(BotMessageVO.CWIDTH_200);
        			cards.setCTextType("3");	
        			cards.setCLinkType(1);  
        			
        			CImageDataItem cImageData = vo.new CImageDataItem();
        			cImageData.setCImage(BotMessageVO.IMG_CC);
        			cImageData.setCImageUrl("card-regular@2x.png");
        			cImageData.setCImageTextType(14);
        			
        			List<Map<String,Object>> cImageTextList = new ArrayList<Map<String,Object>>();			
    				CImageTextItem cImageText = vo.new CImageTextItem();
    				cImageText.setCImageTextTitle(symbolResult.get("SymbolName").toString());
    				cImageText.setCImageText(reqVO.getSymbol());
    				cImageText.setCImageTextTag("bottomCenter");
    				cImageTextList.add(cImageText.getCImageTexts());
        			        			
        			cImageData.setCImageTexts(cImageTextList);        			        		
        			
        			List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();         			
        			for(int i=1; i<=3; i++) {
        				CTextItem cText = vo.new CTextItem();
        				if(i == 1) {
        					cText.setCLabel("申購帳號");
        					cText.setCText(reqVO.getBranchCode() + "–" + reqVO.getAccount());            					
        				} else if(i == 2) {
        					cText.setCLabel("申購金額");
        					cText.setCText(reqVO.getChargeAmt() + "元");
        				} else if(i == 3) {
        					cText.setCLabel("每月交易日");
        					cText.setCText(getString(reqVO.getChargeDay()) + "日");
        				}
        				cTextList.add(cText.getCTexts());
        			}
        			
        			cards.setCImageData(cImageData.getCImageDatas());
        			cards.setCTexts(cTextList);								
        			cardList.add(cards.getCards());	
        			
        			botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);
        			botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, SecQuickReplay.RB18_ELSE2
    						, SecQuickReplay.RB_NOTE);
        		} 
        		/*
        		else if(resultCode.equals("9108")) {        			
        			// 測試
        			List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
        			CardItem cards = vo.new CardItem();
        			cards.setCName("");
        			cards.setCWidth(BotMessageVO.CWIDTH_200);
        			cards.setCTextType("10");	
        			cards.setCLinkType(1);   
        			
        			// 牌卡圖片
        			CImageDataItem cImageData = vo.new CImageDataItem();
        			cImageData.setCImage(BotMessageVO.IMG_CC);
        			cImageData.setCImageUrl("policy_01.jpg");

        			cImageData.setCImageTextType(4);
        			
        			List<Map<String,Object>> cImageTextList = new ArrayList<>();    					
        			CImageTextItem cImageText = vo.new CImageTextItem();
        			cImageText.setCImageText("申請已送出:將於	3個營業日後生效");
        			cImageTextList.add(cImageText.getCImageTexts());
        			cImageData.setCImageTexts(cImageTextList);												
        			cards.setCImageData(cImageData.getCImageDatas());
        			
        			List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();            			
        			for(int i=1; i<=4; i++) {
        				CTextItem cText = vo.new CTextItem();
        				if(i == 1) {
        					cText.setCLabel("申購帳號");
        					cText.setCText(reqVO.getId());          					
        				} else if(i == 2) {
        					cText.setCLabel("申購標的");
        					cText.setCText(reqVO.getName() + "(" + reqVO.getSymbol() + ")");
        				} else if(i == 3) {
        					cText.setCLabel("申購金額");
        					cText.setCText(reqVO.getChargeAmt() + "元");
        				} else if(i == 4) {
        					cText.setCLabel("每月交易");
        					cText.setCText(getString(reqVO.getChargeDay()) + "號");
        				}
        				cTextList.add(cText.getCTexts());
        			}
        			cards.setCTexts(cTextList);								
        			cardList.add(cards.getCards());	
        			
        			botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);
        		} 
        		*/
        		else {
        			botResponseUtils.setTextResult(vo, "挖...申購失敗了...[" + resultMessage + "]");
        		} 			
        	} else {
        		String replyText = "RB01 api error";
        		botResponseUtils.setTextResult(vo, replyText);	
        	}
        } else {
        	vo = (BotMessageVO) rb03Result.get("botMessageVO");
        }

        returnResult.put("botMessageVO", vo);
 		returnResult.put("sipFlag", sipFlag);
        
        return returnResult;
    }
	
	@Override
    public Map<String, Object> getRB27(RegularRequest reqVO, Map<String, String> param) {
        BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
        Map<String, Object> result = new HashMap<String, Object>();  
        Map<String, Object> returnResult = new HashMap<String, Object>();
    	
    	String rpjson = regularAPIFeignService.getRB27(JSON.toJSONString(param));
        
    	if(StringUtils.isNotBlank(rpjson)) { 	
    		result= returnToMap(rpjson); 	
    		String resultCode = getString(result.get("ResultCode"));
    		String resultMessage = getString(result.get("ResultMessage"));
    		
    		Map<String, Object> symbolResult = getRB17_select(reqVO.getSymbol());
    		
    		if(resultCode.equals("0000")) {
    			// 單張牌卡
    			List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
    			CardItem cards = vo.new CardItem();
    			cards.setCName("");
    			cards.setCWidth(BotMessageVO.CWIDTH_200);
    			cards.setCTextType("3");	
    			cards.setCLinkType(1);  
    			
    			CImageDataItem cImageData = vo.new CImageDataItem();
    			cImageData.setCImage(BotMessageVO.IMG_CC);
    			cImageData.setCImageUrl("card-regular@2x.png");
    			cImageData.setCImageTextType(14);
    			
    			List<Map<String,Object>> cImageTextList = new ArrayList<Map<String,Object>>();			
				CImageTextItem cImageText = vo.new CImageTextItem();
				cImageText.setCImageTextTitle(symbolResult.get("SymbolName").toString());
				cImageText.setCImageText(reqVO.getSymbol());
				cImageText.setCImageTextTag("bottomCenter");
				cImageTextList.add(cImageText.getCImageTexts());
    			        			
    			cImageData.setCImageTexts(cImageTextList);        			        		
    			
    			List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();         			
    			for(int i=1; i<=3; i++) {
    				CTextItem cText = vo.new CTextItem();
    				if(i == 1) {
    					cText.setCLabel("申購帳號");
    					cText.setCText(reqVO.getBranchCode() + "–" + reqVO.getAccount());            					
    				} else if(i == 2) {
    					cText.setCLabel("申購金額");
    					cText.setCText(reqVO.getChargeAmt() + "元");
    				} else if(i == 3) {
    					cText.setCLabel("每月交易日");
    					cText.setCText(getString(reqVO.getChargeDay()) + "日");
    				}
    				cTextList.add(cText.getCTexts());
    			}
    			
    			cards.setCImageData(cImageData.getCImageDatas());
    			cards.setCTexts(cTextList);								
    			cardList.add(cards.getCards());	
    			
    			botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);
    			botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, SecQuickReplay.RB18_ELSE2
						, SecQuickReplay.RB_NOTE);
    		} else {
    			botResponseUtils.setTextResult(vo, "挖...申購失敗了...[" + resultMessage + "]");
    		} 			
    	} else {
    		String replyText = "RB27 api error";
    		botResponseUtils.setTextResult(vo, replyText);	
    	}

        returnResult.put("botMessageVO", vo);        
        return returnResult;
    }
	
	@Override
    public BotMessageVO getRB02(RegularRequest reqVO, Map<String, String> param) {
        BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
        Map<String, Object> result = new HashMap<String, Object>(); 										 		
        String rpjson = regularAPIFeignService.getRB02(JSON.toJSONString(param));
        
 		if(StringUtils.isNotBlank(rpjson)) { 	
 			result= returnToMap(rpjson); 
 			String resultCode = getString(result.get("ResultCode"));
 			String epsYq = getString(result.get("EPS_YQ"));
 			String yieldDate = getString(result.get("Yield_Date"));
 			
 			List<Map<String, Object>> dataList = (ArrayList<Map<String, Object>>) result.get("StockList");
 			
 			if(resultCode.equals("0000")) {	
 				// 阿發對話
 				botResponseUtils.setTextResult(vo, "阿發猜你喜歡");
 				
	 			// 牌卡
	 			botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, drawCard(vo, dataList, RB02_DETAIL_TEST, "StockId", "", epsYq, yieldDate));
	 			
	 			// 快速按鈕
	 			botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, SecQuickReplay.rb02_check_1
	 	    			, SecQuickReplay.selectStockType);
 			} else {
 				String replyText = "RB02 api error(" + resultCode + ")";
 				botResponseUtils.setTextResult(vo, replyText);
 			} 			 						
		}else {
			String replyText = "RB02 api error(null)";
			botResponseUtils.setTextResult(vo, replyText);	
		}
 		
        return vo;
    }
	
	@Override
    public Map<String, Object> getRB03(RegularRequest reqVO, Map<String, String> param) {
        BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
        Map<String, Object> result = new HashMap<String, Object>();
        Map<String, Object> returnResult = new HashMap<String, Object>();
        
        param.put("FunctionID", "RB03");        
        String rpjson = regularAPIFeignService.getRB03(JSON.toJSONString(param));
        String sipFlag = "";
        
 		if(StringUtils.isNotBlank(rpjson)) { 	
 			result= returnToMap(rpjson); 	 			 			
 			String resultCode = getString(result.get("ResultCode")); 			
		
 			if(resultCode.equals("0000")) {
 				sipFlag = getString(result.get("SIPFlag"));
 				
 				if("Y".equals(sipFlag)) {
 					// 已簽署定期定額契約
// 					String replyText = "你一個月想投資多少錢呢? (最少1,000元)";
// 					botResponseUtils.setTextResult(vo, replyText);
 				} else {
 					// 未簽署定期定額契約 					
 					botResponseUtils.setTextResult(vo, "我們先來簽合約就可以開始定期定額嚕！");
 					botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, SecQuickReplay.RB03_FLAG_N_N
 							, SecQuickReplay.RB03_FLAG_N(reqVO.getSymbol()), SecQuickReplay.INIT);
 				}
 			} else { 				
 				botResponseUtils.setTextResult(vo, "我好像發生了一些錯誤...");
 				botResponseUtils.setTextResult(vo, getString(result.get("ResultMessage"))); 				
 			}
	 			
		}else {
			String replyText = "RB03 api error";
			botResponseUtils.setTextResult(vo, replyText);	
		}
 		
 		returnResult.put("botMessageVO", vo);
 		returnResult.put("sipFlag", sipFlag);
 		
        return returnResult;
    }
	
	@Override
    public BotMessageVO getRB10(RegularRequest reqVO, Map<String, String> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
        Map<String, Object> result = new HashMap<String, Object>(); 										 		
        String rpjson = regularAPIFeignService.getRB10(JSON.toJSONString(param));
        
 		if(StringUtils.isNotBlank(rpjson)) { 	
 			result= returnToMap(rpjson); 	 			 			
 			String resultCode = getString(result.get("ResultCode"));
 			String isOpen = getString(result.get("IsOpen"));
 			
 			if(resultCode.equals("0000")) {
 				if("Y".equals(getString(isOpen))) {
 					// 開放定期定額
 					// TODO 
 					String replyText = "股票（" + reqVO.getSymbol() + "）開放定期定額";
 					botResponseUtils.setTextResult(vo, replyText);
 				} else {
 					// 未開放定期定額				
 					String replyText = "股票（" + reqVO.getSymbol() + "）未開放定期定額";
 					botResponseUtils.setTextResult(vo, replyText);
 				}
 			} else { 				
 				botResponseUtils.setTextResult(vo, "我好像發生了一些錯誤...");
 				botResponseUtils.setTextResult(vo, getString(result.get("ResultMessage"))); 				
 			}
	 			
		}else {
			String replyText = "RB10 api error";
			botResponseUtils.setTextResult(vo, replyText);	
		}
 		
        return vo;
    }
	
	@Override
    public BotMessageVO getRB11(RegularRequest reqVO, Map<String, String> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
        Map<String, Object> result = new HashMap<String, Object>(); 										 		
        String rpjson = regularAPIFeignService.getRB11(JSON.toJSONString(param));
        
 		if(StringUtils.isNotBlank(rpjson)) { 	
 			result= returnToMap(rpjson); 	 			 			
 			String resultCode = getString(result.get("ResultCode"));			
 			
 			if(resultCode.equals("0000")) { 				
 				List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
 				CardItem cards = vo.new CardItem();
 				cards.setCName("");
 				cards.setCWidth(BotMessageVO.CWIDTH_200);
 				cards.setCTextType("9");
 				cards.setCLinkType(1);

 				// 牌卡圖片
 				CImageDataItem cImageData = vo.new CImageDataItem();
 				cImageData.setCImage(BotMessageVO.IMG_CC);
 				cImageData.setCImageUrl("card22.jpg");	
 				cImageData.setCImageTextType(1);
 				cards.setCImageData(cImageData.getCImageDatas());

 				cImageData.setCImageTextType(5);

 				List<Map<String,Object>> cImageTextList = new ArrayList<>();    					
 				CImageTextItem cImageText = vo.new CImageTextItem();
 				cImageText.setCImageText(reqVO.getSymbolName());
 				cImageTextList.add(cImageText.getCImageTexts());
 				cImageData.setCImageTexts(cImageTextList);												
 				cards.setCImageData(cImageData.getCImageDatas());

 				List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
 				for (int i = 1; i <= 5; i++) {
 					CTextItem cText = vo.new CTextItem();
 					if (i == 1) {
 						cText.setCTextClass("0");
 						cText.setCLabel("申請金額");
 						cText.setCText(reqVO.getChargeAmt());
 					} else if (i == 2) {
 						cText.setCTextClass("0");
 						cText.setCLabel("交易日");
 						cText.setCText(reqVO.getChargeDay());
 					} else if (i == 3) {
 						cText.setCTextClass("0");
 						cText.setCLabel("累計股數");
 						cText.setCText(reqVO.getSumQty());
 					} else if (i == 4) {
 						cText.setCTextClass("0");
 						cText.setCLabel("成交均價");
 						cText.setCText(reqVO.getAvgPrice());
 					} else if (i == 5) {
 						cText.setCTextClass("0");
 						cText.setCLabel("累計金額");
 						cText.setCText(reqVO.getSumAmt());
 					}
 					cTextList.add(cText.getCTexts());
 				}
 				cards.setCTexts(cTextList);
 				cardList.add(cards.getCards());

 				botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);
 			} else {
 				botResponseUtils.setTextResult(vo, "我好像發生了一些錯誤...");
 				botResponseUtils.setTextResult(vo, getString(result.get("ResultMessage")));
 			}

		}else {
			String replyText = "RB11 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}

        return vo;
    }

	@Override
    public BotMessageVO getRB12(RegularRequest reqVO, Map<String, String> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
        Map<String, Object> result = new HashMap<String, Object>();
        String rpjson = regularAPIFeignService.getRB12(JSON.toJSONString(param));

 		if(StringUtils.isNotBlank(rpjson)) {
 			result= returnToMap(rpjson);
 			String resultCode = getString(result.get("ResultCode"));

 			if(resultCode.equals("0000")) {
				String replyText = result.get("Bankname") + "_" + result.get("Sname") + "_銀行餘額";
				botResponseUtils.setTextResult(vo, result.get("Sname") + "你好^^");
				botResponseUtils.setTextResult(vo, "證券帳戶：" + reqVO.getBranchCode() + "-" + reqVO.getAccount());
				botResponseUtils.setTextResult(vo, "國泰世華" + getString(result.get("Bankname")) + "：" + getString(result.get("Bankno")));
				botResponseUtils.setTextResult(vo, "餘額：" + changeMoneyString(getString(result.get("Balance"))) + "元");
 			} else {
 				botResponseUtils.setTextResult(vo, "我好像發生了一些錯誤...");
 				botResponseUtils.setTextResult(vo, getString(result.get("ResultMessage")));
 			}

		}else {
			String replyText = "RB12 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}

        return vo;
    }

	@Override
    public BotMessageVO getRB17(RegularRequest reqVO, Map<String, String> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
        Map<String, Object> result = new HashMap<String, Object>();
        String rpjson = regularAPIFeignService.getRB17(JSON.toJSONString(param));

 		if(StringUtils.isNotBlank(rpjson)) {
 			result= returnToMap(rpjson);
 			String resultCode = getString(result.get("ResultCode"));
 			List<Map<String, Object>> dataList = (ArrayList<Map<String, Object>>) result.get("StocksData");

 			if(resultCode.equals("0000")) {
 				List<Map<String, Object>> dataSelectList = new ArrayList<Map<String, Object>>();
 				String kind = reqVO.getKind();
 				String title = kind.equals("0") ? "股票" : "ETF";

 				if(kind.equals("0") || kind.equals("10")) {
 					for(int i=0; i<dataList.size(); i++) {
 	 					if(kind.equals(dataList.get(i).get("Kind"))) {
 	 						Map<String, Object> dataMap = dataList.get(i);
 	 						String yields = dataMap.get("Yields").toString();

 	 						if(!"N/A".equals(yields)) {
 	 							yields += " %";
 	 							dataMap.put("Yields", yields);
 	 							dataList.set(i, dataMap);
 	 						}

 	 						dataSelectList.add(dataList.get(i));
 	 					}
 	 				}

 					// 清單
 		 			botResponseUtils.setRegularPolicy(vo, dataSelectList, title);
 				} else {
 					// 清單
 		 			botResponseUtils.setRegularPolicy(vo, dataList, title);
 				}

 			} else {
 				String replyText = "RB17 api error(" + resultCode + ")";
 				botResponseUtils.setTextResult(vo, replyText);
 			}
		}else {
			String replyText = "RB17 api error(null)";
			botResponseUtils.setTextResult(vo, replyText);
		}

        return vo;
    }

    public Map<String, Object> getRB17_select(String symbol) {
        Map<String, Object> result = new HashMap<String, Object>();
        Map<String, Object> dataSelectMap = new HashMap<String, Object>();

        Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("FunctionID", "RB17");
		requireParams.put("Channel", PropUtils.getApiChannel());

		String rpjson = regularAPIFeignService.getRB17(JSON.toJSONString(requireParams));

 		if(StringUtils.isNotBlank(rpjson)) {
 			result= returnToMap(rpjson);
 			String resultCode = getString(result.get("ResultCode"));
 			List<Map<String, Object>> dataList = (ArrayList<Map<String, Object>>) result.get("StocksData");

 			if(resultCode.equals("0000")) {
				for(int i=0; i<dataList.size(); i++) {
 					if(symbol.equals(dataList.get(i).get("Symbol"))) {
 						dataSelectMap = dataList.get(i);
 						break;
 					}
 				}
 			}
		}

        return dataSelectMap;
    }

	@Override
    public BotMessageVO getRB18(RegularRequest reqVO, Map<String, String> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
        Map<String, Object> result = new HashMap<String, Object>();
        String rpjson = regularAPIFeignService.getRB18(JSON.toJSONString(param));

 		if(StringUtils.isNotBlank(rpjson)) {
 			result= returnToMap(rpjson);
 			String resultCode = getString(result.get("ResultCode"));
 			String return1 = getString(result.get("Return1"));
 			String return2 = getString(result.get("Return2"));
 			String return3 = getString(result.get("Return3"));

 			return1 = return1.indexOf("-") > -1 ? "<spanGreen>▼ " + return1 + "%</spanGreen>" : "<spanRed>▲ " + return1 + "%</spanRed>";
 			return2 = return2.indexOf("-") > -1 ? "<spanGreen>▼ " + return2 + "%</spanGreen>" : "<spanRed>▲ " + return2 + "%</spanRed>";
 			return3 = return3.indexOf("-") > -1 ? "<spanGreen>▼ " + return3 + "%</spanGreen>" : "<spanRed>▲ " + return3 + "%</spanRed>";

 			List<Map<String, Object>> dataList = (ArrayList<Map<String, Object>>) result.get("PerData");

 			if(resultCode.equals("0000")) {
	 			// 清單
	 			//botResponseUtils.setRegularPolicy(vo, dataList);
 				Map<String, Object> getDetail = getRB17_select(reqVO.getSymbol()); 				
 				
 				String replyText = "以下是阿發模擬過去1～3年，每個月"
 								 + "<spanRed>" + param.get("ChargeDay") + "</spanRed>日固定買" + getDetail.get("SymbolName").toString()
 								 + "<spanRed>" + changeMoneyString(param.get("Amount")) + "</spanRed>元，"
 								 + "買進後持有至今的歷史報酬率。";
 				botResponseUtils.setTextResult(vo, replyText);
	 			botResponseUtils.setTextResult(vo, "<b>歷史報酬率</b>" + "<br />" + "過去1年 " + return1 + "<br />" + "過去2年 " + return2 + "<br />" + "過去3年 " + return3);
	 			botResponseUtils.setTextResult(vo, "以上不含手續費及交易稅的喔！");
	 			botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04
	 							, SecQuickReplay.selectStockType, SecQuickReplay.RB_BEGIN_BUY("確認申購", reqVO.getSymbol()));
 			} else {
 				String replyText = "RB18 api error(" + resultCode + ")";
 				botResponseUtils.setTextResult(vo, replyText);
 			}
		}else {
			String replyText = "RB18 api error(null)";
			botResponseUtils.setTextResult(vo, replyText);
		}

        return vo;
    }

	@Override
    public BotMessageVO getRB19(RegularRequest reqVO, Map<String, String> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
        Map<String, Object> result = new HashMap<String, Object>();
        String rpjson = regularAPIFeignService.getRB19(JSON.toJSONString(param));

 		if(StringUtils.isNotBlank(rpjson)) {
 			result= returnToMap(rpjson);
 			String resultCode = getString(result.get("ResultCode"));
 			List<Map<String, Object>> dataList = (ArrayList<Map<String, Object>>) result.get("CusEntrust");

 			if(resultCode.equals("0000")) {

	 			if(dataList.size() > 0) {
	 				botResponseUtils.setTextResult(vo, "以下是目前正在扣款中的申購");

	 				// 清單
	 				botResponseUtils.setRegular19Policy(vo, dataList);
	 			} else {
	 				botResponseUtils.setTextResult(vo, "你目前沒有進行中的申購");
	 				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, SecQuickReplay.selectStockType);
	 			}

 			} else {
 				String replyText = "RB19 api error(" + resultCode + ")";
 				botResponseUtils.setTextResult(vo, replyText);
 			}
		}else {
			String replyText = "RB19 api error(null)";
			botResponseUtils.setTextResult(vo, replyText);
		}

        return vo;
    }

	@Override
    public BotMessageVO getRB22(RegularRequest reqVO, Map<String, String> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		boolean showQuestion = false;

		// 隨機 阿發問你or滿意度
		int random = (int) (Math.random() * 2);
		if(random == 0) {
			showQuestion = true;
		}
		
		//2019/12/16 如果是從新手上路進入阿發問你，則不清除對話紀錄
		String isClearMessage = "";
		if("Y".equals(StringUtils.defaultString(reqVO.getShowQA()).toUpperCase())) {
			// 下一個問題 呈現阿發問你
			showQuestion = true;
			isClearMessage = "N";
		}

		if(showQuestion) {
			// 阿發問你
			Map<String, Object> result = new HashMap<String, Object>();
	        String rpjson = regularAPIFeignService.getRB22(JSON.toJSONString(param));

	 		if(StringUtils.isNotBlank(rpjson)) {
	 			result= returnToMap(rpjson);
	 			String resultCode = getString(result.get("ResultCode"));
//	 			String question = getString(result.get("Question"));
	 			
	 			
	 			List<Map<String, Object>> dataList = (ArrayList<Map<String, Object>>) result.get("QuestionItem");

	 			if(resultCode.equals("0000")) {
	 				botResponseUtils.setRegularQAPolicy(vo, reqVO.getId(), reqVO.getUdid(), "Q" + isClearMessage, BotMessageVO.LTYPE_04, null, null, dataList);
	 			} else {
	 				botResponseUtils.setTextResult(vo, "我好像有點怪怪的，可以等等再試試看嗎？");
	 				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, SecQuickReplay.INIT);
	 			}
	 			/*	 			
	 			List<Map<String, Object>> dataList = (ArrayList<Map<String, Object>>) result.get("AnswerItem");

	 			if(resultCode.equals("0000")) {
	 				botResponseUtils.setRegularQAPolicy(vo, reqVO.getId(), reqVO.getUdid(), "Q", BotMessageVO.LTYPE_04, getString(result.get("QuestionDesc")),
	 													null, question, dataList);
	 			} else {
	 				botResponseUtils.setTextResult(vo, "我好像有點怪怪的, 可以等等再試試看嗎?");
	 				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, SecQuickReplay.INIT);
	 			}
	 			*/

			}else {
				String replyText = "RB22 api error";
				botResponseUtils.setTextResult(vo, replyText);
			}
		} else {
			// 滿意度
			botResponseUtils.setSurvey(vo);
		}

        return vo;
    }

	@Override
    public BotMessageVO getRB23(RegularRequest reqVO, Map<String, String> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
        Map<String, Object> result = new HashMap<String, Object>();
        String rpjson = regularAPIFeignService.getRB23(JSON.toJSONString(param));

 		if(StringUtils.isNotBlank(rpjson)) {
 			result= returnToMap(rpjson);
 			String resultCode = getString(result.get("ResultCode"));

 			if(resultCode.equals("0000")) {
 				if("X".equals(reqVO.getAnswer())) {
 					// TODO
 					botResponseUtils.setTextResult(vo, "你拒絕回答我！！");
 				} else {
 					String image = reqVO.getCmtPic().toUpperCase();
 					String[] hasImageAry = {"G01", "G02", "G03", "G04", "G05", "G06", "G07", "G08", "G09", "G10"
 							, "G11", "G12", "G13", "G14", "G15"};
 					 					
 					if(Arrays.asList(hasImageAry).contains(image)) {
 						image += ".gif";
 					} else {
 						image = "G07.gif";
 					}

 					image = "qa/" + image;
 					
 	 				botResponseUtils.setRegularQAPolicy(vo, reqVO.getId(), reqVO.getUdid(), "A", null, reqVO.getCmtDesc(), image, null);
 				}
 			} else {
 				botResponseUtils.setTextResult(vo, "我好像有點怪怪的，可以等等再試試看嗎？");
 				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, SecQuickReplay.INIT);
 			}

		}else {
			String replyText = "RB23 api error";
			botResponseUtils.setTextResult(vo, replyText);
		}

        return vo;
    }

    @Override
    public TPIGatewayResult rb02Check(FallBackRequest reqVO) {
        BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

        List<Map<String, Object>> cardList = new ArrayList<>();
		CardItem cards = vo.new CardItem();
		cards.setCName("");
		cards.setCWidth(BotMessageVO.CWIDTH_200);
		cards.setCTextType("10");
		cards.setCLinkType(1);

		// 牌卡圖片
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CC);
		cImageData.setCImageUrl("card35.png");
		cImageData.setCImageTextType(1);
		cards.setCImageData(cImageData.getCImageDatas());

		List<Map<String, Object>> cTextList = new ArrayList<>();
		CTextItem cText = vo.new CTextItem();
		cText.setCLabel("定期定額介紹");
		cText.setCText("每月存股，<br>最低只要1,000元");
		cTextList.add(cText.getCTexts());
		cards.setCTexts(cTextList);

		cards.setCLinkList(botResponseUtils.getCLinkList(vo, SecQuickReplay.RSP_FAQ, SecQuickReplay.HOT_RSP, SecQuickReplay.rb02_check_1));
		cardList.add(cards.getCards());

		botResponseUtils.setTextResult(vo, "每個月只要1,000元就可以成為存股達人喔！");
		botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);

        return TPIGatewayResult.ok().setData(vo).put("msgList", Arrays.asList(SecQuickReplay.HOT_RSP.getText())).put("optionList", Arrays.asList(SecQuickReplay.HOT_RSP));
    }

    public BotMessageVO rb17_check_send(RegularRequest reqVO) {
    	BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
        Map<String, Object> result = new HashMap<String, Object>();

        List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		CardItem cards = vo.new CardItem();
		cards.setCName("");
		cards.setCWidth(BotMessageVO.CWIDTH_250);
		cards.setCTextType("9");
		cards.setCLinkType(1);

		// 牌卡圖片
 		CImageDataItem cImageData = vo.new CImageDataItem();
 		cImageData.setCImage(BotMessageVO.IMG_GENERIC);
 		cImageData.setCImageUrl("card22.jpg");
 		cImageData.setCImageTextType(1);
 		cards.setCImageData(cImageData.getCImageDatas());

		cImageData.setCImageTextType(5);

		List<Map<String,Object>> cImageTextList = new ArrayList<>();
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageText(reqVO.getSymbolName());
		cImageTextList.add(cImageText.getCImageTexts());
		cImageData.setCImageTexts(cImageTextList);
		cards.setCImageData(cImageData.getCImageDatas());

		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		for (int i = 1; i <= 4; i++) {
			CTextItem cText = vo.new CTextItem();
			if (i == 1) {
				cText.setCTextClass("0");
				cText.setCLabel("收盤價");
				cText.setCText(reqVO.getClosePrice());
			} else if (i == 2) {
				String riseFallPcnt = getString(reqVO.getRiseFallPcnt());

				if("".equals(riseFallPcnt)) {
					cText.setCTextClass("0");
				} else if(riseFallPcnt.indexOf("-") > -1) {
					// 跌幅
					riseFallPcnt = riseFallPcnt.replace("-", "");
					cText.setCTextClass("2");
				} else {
					// 漲幅
					cText.setCTextClass("1");
				}

				cText.setCLabel("漲跌幅");
				cText.setCText(riseFallPcnt + "％");
			} else if (i == 3) {
				cText.setCTextClass("0");
				cText.setCLabel("EPS");

				String eps = reqVO.getEps().equals("") ? "--" : reqVO.getEps();
				cText.setCText(eps);
			} else if (i == 4) {
				cText.setCTextClass("0");
				cText.setCLabel("預估殖利率");
				cText.setCText(reqVO.getYields());
			}
			cTextList.add(cText.getCTexts());
		}
		cards.setCTexts(cTextList);

		QuickReplay q1 = SecQuickReplay.RB_HISTORY(reqVO.getSymbol());
		QuickReplay q2 = SecQuickReplay.RB_BEGIN_BUY("開始申購", reqVO.getSymbol());

		cards.setCLinkList(botResponseUtils.getCLinkList(vo, q1, q2));
		cardList.add(cards.getCards());

		botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);

        return vo;
    }

    public BotMessageVO rb19_check_send(RegularRequest reqVO) {
    	BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
        Map<String, Object> result = new HashMap<String, Object>();

        List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		CardItem cards = vo.new CardItem();
		cards.setCName("");
		cards.setCWidth(BotMessageVO.CWIDTH_200);
		cards.setCTextType("9");
		cards.setCLinkType(1);

		// 牌卡圖片
		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CC);
		cImageData.setCImageUrl("card22.jpg");
		cImageData.setCImageTextType(1);
		cards.setCImageData(cImageData.getCImageDatas());

		cImageData.setCImageTextType(5);

		List<Map<String,Object>> cImageTextList = new ArrayList<>();
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageText(reqVO.getSymbolName());
		cImageTextList.add(cImageText.getCImageTexts());
		cImageData.setCImageTexts(cImageTextList);
		cards.setCImageData(cImageData.getCImageDatas());

		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		for (int i = 1; i <= 5; i++) {
			CTextItem cText = vo.new CTextItem();
			if (i == 1) {
				cText.setCTextClass("0");
				cText.setCLabel("申請金額");
				cText.setCText(changeMoneyString(reqVO.getChargeAmt()) + "元");
			} else if (i == 2) {
				cText.setCTextClass("0");
				cText.setCLabel("交易日");
				cText.setCText("每月" + reqVO.getChargeDay() + "日");
			} else if (i == 3) {
				cText.setCTextClass("0");
				cText.setCLabel("累計股數");
				cText.setCText(reqVO.getSumQty() + "股");
			} else if (i == 4) {
				cText.setCTextClass("0");
				cText.setCLabel("成交均價");
				cText.setCText(changeMoneyString(reqVO.getAvgPrice()) + "元");
			} else if (i == 5) {
				cText.setCTextClass("0");
				cText.setCLabel("累計金額");
				cText.setCText(changeMoneyString(reqVO.getSumAmt()) + "元");
			}
			cTextList.add(cText.getCTexts());
		}
		cards.setCTexts(cTextList);
		cardList.add(cards.getCards());

		botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);
		if(StringUtils.equals(reqVO.getDevice(),"app")) {
			botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04, SecQuickReplay.RB_RETURN);
		}
        return vo;
    }

    public BotMessageVO rb_sip_note(RegularRequest reqVO) {
    	BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
    	// 正式環境
//    	String url = "https://webap.cathaysec.com.tw/Contract/RQ/index.aspx";
    	// 測試環境
    	String url = "https://mapptest.cathaysec.com.tw/Contract/RQ/index.aspx";
    	
    	botResponseUtils.setRegularIFrame(vo, url);
    	
    	return vo;
    }
    
	public String getString(Object str){
		String returnStr = "";
		if(str != null){
			returnStr = str.toString();
		}

		return returnStr;
	}

    public Integer getInteger(Object data){
		Integer returnInt = 0;
		if(data != null){
			try {
				returnInt = Integer.parseInt(data.toString());
			}catch (NumberFormatException e) {
				return returnInt;
			}
		}

		return returnInt;
	}

    private String changeMoneyString(String money) {
		NumberFormat numberFormat = NumberFormat.getNumberInstance();
		return numberFormat.format(getInteger(money));
	}

    public static Map<String, Object> returnToMap(String json) {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> rumap = new HashMap<String, Object>();
		try {
			rumap = mapper.readValue(json, new TypeReference<HashMap<String,Object>>(){});
			return rumap;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rumap;
	}

    public List<Map<String, Object>> drawCard(BotMessageVO vo, List<Map<String, Object>> arrayList
    		, String[] detailArray, String titleKey, String title, String epsYq, String yieldDate) {
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();

		for(int i=0; i<arrayList.size(); i++) {
			String idKey = getString(arrayList.get(i).get(titleKey));
			String idName = getString(arrayList.get(i).get("StockName"));

			CardItem cards = vo.new CardItem();
			cards.setCName("");
			cards.setCWidth(BotMessageVO.CWIDTH_200);
			cards.setCTextType("9");
			cards.setCLinkType(1);

			// 牌卡圖片
			CImageDataItem cImageData = vo.new CImageDataItem();
			cImageData.setCImage(BotMessageVO.IMG_CC);
			cImageData.setCImageUrl("card37-01.png");
			cImageData.setCImageTextType(7);

			List<Map<String,Object>> cImageTextList = new ArrayList<>();

//			for(int k=0; k<2; k++) {
//				CImageTextItem cImageText = vo.new CImageTextItem();
//
//				if(k == 0) {
//					cImageText.setCImageText(idName);
//				} else if(k == 1) {
//					cImageText.setCImageText(idKey);
//				}
//
//				cImageTextList.add(cImageText.getCImageTexts());
//			}

//			if(!getString(title).equals("")) {
//				cards.setCTitle(title);
//			}else if(!getString(titleKey).equals("")){
//				cards.setCTitle(idKey);
//			}

			List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
			
			String imageTextColor = "black";
			String riseFallPcnt= "";
			String closePrice = "";

			for(int j=0; j<detailArray.length; j++) {
				CTextItem cText = vo.new CTextItem();

				String key = detailArray[j].split(":")[0];
				String label = detailArray[j].split(":")[1];
				String keyType = detailArray[j].split(":")[2];

				String text = getString(arrayList.get(i).get(key));

				if("".equals(text)) {
					text = "--";
					cText.setCTextClass("0");
				} else {

					if("Yields".equals(key)) {
						text += "％";
					}
					
					if("ClosePrice".equals(key)) {
						closePrice = text;
					}

					if ("T".equals(getString(keyType)) && "RiseFallPcnt".equals(key)) {
						if (StringUtils.equals(DataUtils.handleMoney2(text.replace("+", "")), "0.00")) {
							riseFallPcnt += "  " + text.replace("+", "") + "％";
							imageTextColor = "";
						} else if (text.indexOf("-") > -1) {
							// 跌幅
							riseFallPcnt = " ▼" + text.replace("-", "") + "％";
							imageTextColor = "green";
						} else {
							// 漲幅
							riseFallPcnt += " ▲" + text.replace("+", "") + "％";
							imageTextColor = "red";
						}
					} else {
						cText.setCTextClass("0");
					}
				}
				
				if("Yields".equals(key)) {
					cText.setCLabel(label);
					cText.setCText(text);
					cTextList.add(cText.getCTexts());
				} else if("EPS".equals(key)) {
					label += "（" + epsYq + "）";
					cText.setCLabel(label);
					cText.setCText(text);
					cTextList.add(cText.getCTexts());
				}
			}
			
			CImageTextItem cImageText = vo.new CImageTextItem();
			cImageText.setCImageTextTitle(getString(idName + " <span  style=\"color:#939ca0\">" + idKey + "</span>"));
			cImageText.setCImageText( DateUtils.format(new Date(), "yyyy/") + yieldDate);
			cImageText.setCImageTextTag("Label");
			cImageTextList.add(cImageText.getCImageTexts());
			
			CImageTextItem cImageText2 = vo.new  CImageTextItem();	
			cImageText2.setCImageTextTitle(getString(closePrice) + "::" + imageTextColor);
			cImageText2.setCImageText(riseFallPcnt + "::" + imageTextColor);
			cImageText2.setCImageTextTag("data2");
			cImageTextList.add(cImageText2.getCImageTexts());
			
			cImageData.setCImageTexts(cImageTextList);
			cards.setCImageData(cImageData.getCImageDatas());
			
			cards.setCTexts(cTextList);

//			Map<String, Object> parameter = new HashMap<>();
//			parameter.put("stockId", idKey);
//			parameter.put("priceType", "個股");
//			QuickReplay q1 = new SecQuickReplay(2, "查看個股資訊", "查看" + idKey + "個股資訊", "報價", "報價類型", parameter);
			QuickReplay q1 = SecQuickReplay.RB_HISTORY(idKey);	// 歷史報酬試算
			QuickReplay q2 = SecQuickReplay.RB_BEGIN_BUY("申購", idKey);

			cards.setCLinkList(botResponseUtils.getCLinkList(vo, q1, q2));
			
			cardList.add(cards.getCards());				
		}			
		
		return cardList;
	}
    
}
