package com.tp.tpigateway.modules.cathayins.fegin;

import com.tp.common.annotation.TpSysLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@FeignClient(name = "insproducttellapi", url = "${api.path}")
@TpSysLog(remark = "InsProductTellAPIFeignService")
public interface InsProductTellAPIFeignService {

	@RequestMapping(value = "getInsuresItemDesc", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	String getT01(Map<String, Object> map);

}
