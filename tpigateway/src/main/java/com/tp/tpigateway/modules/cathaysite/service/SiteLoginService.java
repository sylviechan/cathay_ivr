package com.tp.tpigateway.modules.cathaysite.service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotRequest;

public interface SiteLoginService {

	/**
	 * For 投信/證券-呼叫登入頁面
	 */
	BotMessageVO getLogin(BotRequest botRequest);

}
