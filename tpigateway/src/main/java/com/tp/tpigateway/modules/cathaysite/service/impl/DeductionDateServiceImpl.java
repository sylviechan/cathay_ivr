package com.tp.tpigateway.modules.cathaysite.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.cathaysec.DefaultRequest;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.modules.cathaysite.service.DeductionDateService;

@Service("deductionDateService")
public class DeductionDateServiceImpl implements DeductionDateService {
	@Autowired
	BotResponseUtils botResponseUtils;

	@Override
	public BotMessageVO deductionDate(DefaultRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		// int startDate = Integer.parseInt(new
		// SimpleDateFormat("dd").format(getFirstMonthDay(calendar)));
		int endDate = Integer.parseInt(new SimpleDateFormat("dd").format(getLastMonthDay(Calendar.getInstance())));
		int today = Integer.parseInt(new SimpleDateFormat("dd").format(Calendar.getInstance().getTime()));
		List<Map<String, Object>> list = new ArrayList<>(); // 送出物件
		String selectVal = "";
		Map<String, Object> map = new HashMap<String, Object>();
		List<Map<String, String>> txList = new ArrayList<>();
		for (int i = 1; i <= 31; i++) {
			if (i == 1) {
				map.put("title", "每月01號-每月10號");
			} else if (i == 11) {
				list.add(map);
				map.put("content", txList);
				txList = new ArrayList<>();
				map = new HashMap<String, Object>();
				map.put("title", "每月11號-每月20號");
			} else if (i == 21) {
				list.add(map);
				map.put("content", txList);
				txList = new ArrayList<>();
				map = new HashMap<String, Object>();
				map.put("title", "每月21號-每月31號");
			} else if (i == 31) {
				map.put("content", txList);
				list.add(map);
			}

			Map<String, String> txMap = new HashMap<String, String>();

			txMap.put("txt", "每月" + String.valueOf(i) + "號");
			txMap.put("keyValue", "keyValue" + String.valueOf(i));
			String status = "def";
			if (i > endDate) {
				status = "lock";
			} else if (i == today) {
				selectVal = "keyValue" + String.valueOf(i);
				status = "selected";
			}
			txMap.put("status", status);

			txList.add(txMap);
		}
		String title ="扣款日期";
		botResponseUtils.setDeductionDate(botMessageVO,title, list, selectVal);
		return botMessageVO;
	}

	// 月的第一天日期
	public static Date getFirstMonthDay(Calendar calendar) {
		calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DATE));
		return calendar.getTime();
	}

	// 月的最後一天日期
	public static Date getLastMonthDay(Calendar calendar) {
		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
		return calendar.getTime();
	}

}
