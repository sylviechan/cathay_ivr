package com.tp.tpigateway.modules.cathayins.mybatis.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.tp.tpigateway.modules.cathayins.mybatis.entity.TopQuestion;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author blaze
 * @since 2019-10-31
 */
@Mapper
public interface TopQuestionMapper extends BaseMapper<TopQuestion> {
    @Select("select * from top_question tq where tq.question_type = #{topQuestion}")
    List<TopQuestion> getAll(@Param("topQuestion") int questionType);
}
