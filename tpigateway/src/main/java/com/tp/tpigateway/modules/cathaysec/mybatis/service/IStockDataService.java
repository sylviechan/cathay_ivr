package com.tp.tpigateway.modules.cathaysec.mybatis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tp.tpigateway.modules.cathaysec.mybatis.entity.StockData;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author matt
 * @since 2019-09-04
 */
public interface IStockDataService extends IService<StockData> {

    Map<String, StockData> getAllStockIdData();

    Map<String, StockData> getAllStockNameData();
}
