package com.tp.tpigateway.modules.cathaysec.controller;

import java.util.Map;

import com.tp.tpigateway.common.util.PropUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.tp.common.model.TPIGatewayResult;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.cathaysec.fegin.LoginAPIFeignService;

@RestController
@RequestMapping("/Login")
public class LoginController extends BaseController {

    @Autowired
    LoginAPIFeignService loginAPIFeignService;
	
    @RequestMapping(value = "/loginForOtherDevice",method = RequestMethod.POST ,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult loginForOtherDevice(@RequestBody Map<String, String> secReqVO) {
    	try {
    		secReqVO.put("Channel", PropUtils.getApiChannel());
    		secReqVO.put("FunctionID", "RB28");
    		log.info("JSON.toJSONString(secReqVO) : " + JSON.toJSONString(secReqVO));
    		Map<String, Object> result = loginAPIFeignService.loginForOtherDevice(secReqVO);
    		log.info("API :result : " + JSON.toJSONString(result));
			if (result != null) {
				return TPIGatewayResult.ok().setData(result);
			} else {
				log.error("result is null");
				return TPIGatewayResult.error("result is null");
			}
    	} catch (feign.RetryableException e) {
			String msg = e.getClass() + ":" + e.getMessage();
			log.error(msg);
			e.printStackTrace();
			return TPIGatewayResult.error("連線失敗");
		} catch (Exception e) {
			String msg = e.getClass() + ":" + e.getMessage();
			log.error(msg);
			e.printStackTrace();
			return TPIGatewayResult.error(msg);
		}
    }
}
