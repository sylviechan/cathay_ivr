package com.tp.tpigateway.modules.cathaysite.controller;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysite.FundRequest;
import com.tp.common.model.cathaysite.FundStopRequest;
import com.tp.tpigateway.common.constant.TPIConst;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.cathaysite.service.FundService;
import com.tp.tpigateway.modules.cathaysite.service.SiteInvestmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("cathaysite/fund")
public class FundController extends BaseController {
	
	//假資料，測試暫用
	public static List<String> phonyDataList;

    @Autowired
    FundService fundService;

    @Autowired
    SiteInvestmentService siteInvestmentService;

    @RequestMapping(value = "/webFundKey", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult WebFundKey(@RequestBody FundRequest reqVO) {
        BotMessageVO botMessageVO = null;
		botMessageVO = fundService.webFundKey(reqVO);

        return TPIGatewayResult.ok().setData(botMessageVO);
    }

	@RequestMapping(value = "/getWebFund", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getWebFund(@RequestBody FundRequest reqVO) {
		BotMessageVO botMessageVO = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<>();
		requireParams.put("UrlPara", reqVO.getUrlPara());
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("Channel", reqVO.getChannel());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			requireParams.put("cstno", reqVO.getCstno()); // 可為空值
			requireParams.put("isallot", reqVO.getIsallot());

			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = fundService.getWebFund(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

    @RequestMapping(value = "/getWebFundNav", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult GetWebFundNav(@RequestBody FundRequest reqVO) {
        BotMessageVO botMessageVO = null;

        // 設定必要參數
        Map<String, String> requireParams = new HashMap<String, String>();
        requireParams.put("fund", reqVO.getFund());
        requireParams.put("UrlPara", reqVO.getUrlPara());
        requireParams.put("SessionID", reqVO.getSessionId());
        requireParams.put("ClientIP", reqVO.getClientIp());
        requireParams.put("Channel", reqVO.getChannel());

        // 必要參數檢核
        if (checkParams(requireParams)) {
            requireParams.put("cstno", reqVO.getCstno());
            
            Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
            return fundService.getWebFundNav(reqVO, obj);
        } else {
        	return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}

    }

    @RequestMapping(value = "/getWebFundAllotinterest", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult GetWebFundAllotinterest(@RequestBody FundRequest reqVO) {
        BotMessageVO botMessageVO = null;

        // 設定必要參數
        Map<String, String> requireParams = new HashMap<String, String>();
        requireParams.put("fund", reqVO.getFund());
        requireParams.put("UrlPara", reqVO.getUrlPara());
        requireParams.put("SessionID", reqVO.getSessionId());
        requireParams.put("ClientIP", reqVO.getClientIp());
        requireParams.put("Channel", reqVO.getChannel());

        // 必要參數檢核
        if (checkParams(requireParams)) {
            requireParams.put("cstno", reqVO.getCstno());
            
            Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
            return fundService.getWebFundAllotinterest(reqVO, obj);
        } else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}

    }

	@RequestMapping(value = "/getFundStopList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getFundStopList(@RequestBody FundStopRequest reqVO) {
		BotMessageVO botMessageVO = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = fundService.getFundStopList(reqVO, obj);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/getFundStopDetail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getFundStopDetail(@RequestBody FundStopRequest reqVO) {

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			return fundService.getFundStopDetail(reqVO, obj);
		}
		else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}

	}

	@RequestMapping(value = "/getFundStopTxPwd", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getFundStopTxPwd(@RequestBody FundStopRequest reqVO) {
		BotMessageVO botMessageVO = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = fundService.getFundStopTxPwd(reqVO, obj);
		}
		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/setFundStatusRestore", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult setFundStatusRestore(@RequestBody FundStopRequest reqVO) {
		BotMessageVO botMessageVO = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		//requireParams.put("pwd", Aes256Encrypt.encrypt(reqVO.getPwd())); //改在chatweb加密,避免flow裡面存著明碼
		requireParams.put("pwd", reqVO.getPwd());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			requireParams.put("tno", reqVO.getTno());

			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = fundService.setFundStatusRestore(reqVO, obj);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/getFundList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getFundList(@RequestBody FundRequest reqVO) {
		BotMessageVO botMessageVO = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("SendType", reqVO.getSendType());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = fundService.getFundList(reqVO, obj);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}
	
	@RequestMapping(value = "/getFundDetail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getFundDetail(@RequestBody FundRequest reqVO) {
		BotMessageVO botMessageVO = null;
		Map<String, Object> result = null;
		String type = reqVO.getType();
		String isPass = null;
		String rDate = null;
		String amt = null;
		String acnt = null;
		String acntName = null;
		String sno = null;
		String note = null;
		String fundDoc = null;
		String declare = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("fund", reqVO.getFund());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			result = fundService.getFundDetail(reqVO, obj);
			botMessageVO = (BotMessageVO) result.get("botMessageVO");
			isPass = result.get("isPass").toString();
			rDate =  result.get("rDate").toString();
			amt = result.get("amt").toString();
			acnt = result.get("acnt").toString();
			acntName = result.get("acntName").toString();
			sno = result.get("sno").toString();
			note = result.get("note").toString();
			fundDoc = result.get("fundDoc").toString();
			declare = result.get("declare").toString();
		}

		return TPIGatewayResult.ok().setData(botMessageVO).put("type", type).put("isPass", isPass).put("rDate", rDate)
														  .put("amt", amt).put("acnt", acnt).put("acntName", acntName)
														  .put("sno", sno).put("note", note).put("fundDoc", fundDoc)
														  .put("declare", declare);
	}
	
	@RequestMapping(value = "/setAdd", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult setAdd(@RequestBody FundRequest reqVO) {

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("fund", reqVO.getFund());
		requireParams.put("amt", reqVO.getAmt());
		requireParams.put("rdate", reqVO.getRdate().replaceAll("/", "").replaceAll("-", ""));
		requireParams.put("bact", reqVO.getSno() + "-" + reqVO.getAcnt());
		requireParams.put("pwd", reqVO.getPwd());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			return fundService.setAdd(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}

	}
	
	@RequestMapping(value = "/getWebFundChoice", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getWebFundChoice(@RequestBody FundRequest reqVO) {
		BotMessageVO botMessageVO = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());	
		requireParams.put("UrlPara", reqVO.getUrlPara());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			requireParams.put("cstno", reqVO.getCstno());
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = fundService.getWebFundChoice(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}

	@RequestMapping(value = "/getApplyFund", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getApplyFund(@RequestBody FundRequest reqVO) {
		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("SendType", reqVO.getSendType());
		requireParams.put("fund", reqVO.getFund());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			return fundService.getApplyFund(reqVO, obj);
		} else {
			return TPIGatewayResult.error(TPIConst.PARAMETER_ERROR);
		}
	}

	@RequestMapping(value = "/getFundCmd", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getFundCmd(@RequestBody FundRequest reqVO) {
		return fundService.chooseCmd(reqVO);
	}
	
	@RequestMapping(value="/getOne", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getOne(@RequestBody FundRequest reqVO) {
		BotMessageVO botMessageVO = new BotMessageVO();
		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("query", reqVO.getQuery());
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = fundService.getOne(reqVO, obj);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}
	
	@PostMapping(value="/getQuota",produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getQuota(@RequestBody FundRequest reqVO) {
		BotMessageVO botMessageVO = new BotMessageVO();
		Map<String, String> param = new HashMap<String, String>();
		param.put("SessionID", reqVO.getSessionId());
		param.put("ClientIP", reqVO.getClientIp());
		param.put("types", reqVO.getType());
		Map<String, Object> obj = new HashMap<String, Object>();
		obj.put("obj", param);
		botMessageVO = siteInvestmentService.getQuota(reqVO, obj);
		return TPIGatewayResult.ok().setData(botMessageVO);
	}
	
	@RequestMapping(value = "/getOrderGroup", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getOrderGroup(@RequestBody FundRequest reqVO) {
		BotMessageVO botMessageVO = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = fundService.getOrderGroup(reqVO, obj);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}
	
	@RequestMapping(value = "/getOrderDetail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getOrderDetail(@RequestBody FundRequest reqVO) {
		BotMessageVO botMessageVO = null;		

		// 設定必要參數
//		Map<String, String> requireParams = new HashMap<String, String>();
//		requireParams.put("SessionID", reqVO.getSessionId());
//		requireParams.put("ClientIP", reqVO.getClientIp());
		
//		requireParams.put("otype", reqVO.getOtype());
//		requireParams.put("tnos", reqVO.getTnos());

		// 必要參數檢核
//		if (checkParams(requireParams)) {
//			Map<String, Object> obj = new HashMap<String, Object>();
//			obj.put("obj", requireParams);
			botMessageVO = fundService.getOrderDetail(reqVO);
//		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}
	
	@RequestMapping(value = "/getECopenProcess", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult getECopenProcess(@RequestBody FundRequest reqVO) {
		BotMessageVO botMessageVO = null;

		// 設定必要參數
		Map<String, String> requireParams = new HashMap<String, String>();
		requireParams.put("SessionID", reqVO.getSessionId());
		requireParams.put("ClientIP", reqVO.getClientIp());
		requireParams.put("uid", reqVO.getUid());
		requireParams.put("birth", reqVO.getBirth());

		// 必要參數檢核
		if (checkParams(requireParams)) {
			Map<String, Object> obj = new HashMap<String, Object>();
			obj.put("obj", requireParams);
			botMessageVO = fundService.getECopenProcess(reqVO, obj);
		}

		return TPIGatewayResult.ok().setData(botMessageVO);
	}
	
	@RequestMapping(value = "/closeFundList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult closeFundList(@RequestBody FundRequest reqVO) {
		
		if(reqVO.getType() == null) {
			reqVO.setType("");
		}
		
		BotMessageVO botMessageVO = fundService.closeFundList(reqVO);

		return TPIGatewayResult.ok().setData(botMessageVO);
	}
}
