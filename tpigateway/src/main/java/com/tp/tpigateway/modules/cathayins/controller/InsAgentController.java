package com.tp.tpigateway.modules.cathayins.controller;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathayins.InsAgentRequest;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.common.service.CommonService;
import com.tp.tpigateway.modules.cathayins.service.InsAgentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/Agent")
public class InsAgentController extends BaseController{
	private static Logger logger = LoggerFactory.getLogger(InsAgentController.class);	
	
	@Autowired
	InsAgentService insAgentService;

	@Autowired
	CommonService commonService;

	@RequestMapping(value="/cmd", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult cmd(@RequestBody InsAgentRequest reqVO) {        
        BotMessageVO botMessageVO = insAgentService.chooseCmd(reqVO);
	
	    return TPIGatewayResult.ok().setData(botMessageVO);
    }
	
	@RequestMapping(value="/Agent4_1", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult Agent4_1(@RequestBody InsAgentRequest reqVO) {        
		// 設定必要參數
		Map<String, Object> requireParams = new HashMap<>();
		requireParams.put("SESSION_ID", reqVO.getSessionId());
		requireParams.put("channel", reqVO.getChannel());
		
		requireParams.put("cmd", reqVO.getCmd());
		requireParams.put("chatID", reqVO.getChatID());
		requireParams.put("chatType", reqVO.getChatType());
		requireParams.put("chatLog", reqVO.getChatLog());
		requireParams.put("chatLogUrl", reqVO.getChatLogUrl());
		requireParams.put("customerId", reqVO.getCustomerId());
		
		requireParams.put("isAppeal", reqVO.isAppeal());
		requireParams.put("userName", reqVO.getUserName());
		requireParams.put("sex", reqVO.getSex());
		requireParams.put("policyNo", reqVO.getPolicyNo());
		requireParams.put("rejectLogin", reqVO.getRejectLogin());
		requireParams.put("loginStatus", reqVO.getLoginStatus());

		return insAgentService.getAgent4_1(reqVO, requireParams);

    }
	
	@RequestMapping(value="/Agent4_2", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult Agent4_2(@RequestBody InsAgentRequest reqVO) {        
		// 設定必要參數
		Map<String, Object> requireParams = new HashMap<>();
		requireParams.put("SESSION_ID", reqVO.getSessionId());
		requireParams.put("channel", reqVO.getChannel());
		
		requireParams.put("cmd", reqVO.getCmd());
		requireParams.put("chatID", reqVO.getChatID());
		requireParams.put("customerMsgId", reqVO.getCustomerMsgId());
		requireParams.put("chatMessage", reqVO.getChatMessage());
		requireParams.put("faqAnsList", reqVO.getFaqAnsList());
		requireParams.put("customerId", reqVO.getCustomerId());

		requireParams.put("userName", reqVO.getUserName());
		requireParams.put("sex", reqVO.getSex());
		requireParams.put("loginStatus", reqVO.getLoginStatus());

		return insAgentService.getAgent4_2(reqVO, requireParams);
    }
		
	@RequestMapping(value="/Agent4_7", method=RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult Agent4_7(@RequestBody InsAgentRequest reqVO) {        
		// 設定必要參數
		Map<String, Object> requireParams = new HashMap<>();
		requireParams.put("SESSION_ID", reqVO.getSessionId());
		requireParams.put("channel", reqVO.getChannel());
		
		requireParams.put("cmd", reqVO.getCmd());
		requireParams.put("chatID", reqVO.getChatID());

		return insAgentService.getAgent4_7(reqVO, requireParams);
    }

}

