package com.tp.tpigateway.modules.cathaysec.model;

import com.tp.common.model.BotRequest;

public class HelloRequest extends BotRequest {

    private String idno;
    private String udId;
    private String Id;

    public String getUdId() {
		return udId;
	}

	public void setUdId(String udId) {
		this.udId = udId;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getIdno() {
        return idno;
    }

    public void setIdno(String idno) {
        this.idno = idno;
    }
    
}
