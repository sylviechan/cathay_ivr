package com.tp.tpigateway.modules.cathaysec.mybatis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tp.tpigateway.common.constant.CacheConst;
import com.tp.tpigateway.modules.cathaysec.mybatis.entity.StockData;
import com.tp.tpigateway.modules.cathaysec.mybatis.mapper.StockDataMapper;
import com.tp.tpigateway.modules.cathaysec.mybatis.service.IStockDataService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.support.SimpleValueWrapper;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author matt
 * @since 2019-09-04
 */
@Service
public class StockDataServiceImpl extends ServiceImpl<StockDataMapper, StockData> implements IStockDataService {

    @Autowired
    CacheManager cacheManager;

    @Override
    public Map<String, StockData> getAllStockIdData() {
        List<StockData> list;
        Map<String, StockData> stockDataMap;

        if (cacheManager.getCache(CacheConst.TPI_CACHE).get(CacheConst.STOCK_ID_DATA_KEY) == null) {
            list = this.list();

            if (CollectionUtils.isNotEmpty(list)) {
                stockDataMap = list.stream().collect(Collectors.toMap(s -> StringUtils.trim(s.getStockId()), Function.identity()));
            } else {
                stockDataMap = new HashMap<>();
            }
            cacheManager.getCache(CacheConst.TPI_CACHE).put(CacheConst.STOCK_ID_DATA_KEY, stockDataMap);

        } else {
            SimpleValueWrapper simpleValueWrapper = (SimpleValueWrapper) cacheManager.getCache(CacheConst.TPI_CACHE).get(CacheConst.STOCK_ID_DATA_KEY);
            stockDataMap = (Map<String, StockData>) simpleValueWrapper.get();
        }

        return stockDataMap;
    }

    @Override
    public Map<String, StockData> getAllStockNameData() {
        List<StockData> list;
        Map<String, StockData> stockDataMap;

        if (cacheManager.getCache(CacheConst.TPI_CACHE).get(CacheConst.STOCK_NAME_DATA_KEY) == null) {
            list = this.list();

            if (CollectionUtils.isNotEmpty(list)) {
                //stockDataMap = list.stream().collect(Collectors.toMap(s -> StringUtils.trim(s.getStockName()), Function.identity()));  //有重複key的話會拋錯的話,可改用下面寫法
                stockDataMap = list.stream().collect(Collectors.toMap(s -> StringUtils.trim(s.getStockName()), Function.identity(), (oldValue, newValue) -> newValue));
            } else {
                stockDataMap = new HashMap<>();
            }
            cacheManager.getCache(CacheConst.TPI_CACHE).put(CacheConst.STOCK_NAME_DATA_KEY, stockDataMap);

        } else {
            SimpleValueWrapper simpleValueWrapper = (SimpleValueWrapper) cacheManager.getCache(CacheConst.TPI_CACHE).get(CacheConst.STOCK_NAME_DATA_KEY);
            stockDataMap = (Map<String, StockData>) simpleValueWrapper.get();
        }

        return stockDataMap;
    }
}
