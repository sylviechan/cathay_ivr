package com.tp.tpigateway.modules.cathaysec;

public class SecTestApiData {
	
	/**
	 * RB50
	 * @return response json string
	 */
	public static String getRB50ResponseString() {
		return""+"{   \"orderSumarrys\": [     {       \"notDealQty\": 1000,       \"avgDealPrice\": 0,       \"symbolName\": \"台泥\",       \"symbol\": \"1101\",       \"orderType\": \"0B\",       \"availQty\": 1000,       \"sumDealQty\": 0     },     {       \"notDealQty\": 1000,       \"avgDealPrice\": 0,       \"symbolName\": \"華南金\",       \"symbol\": \"2880\",       \"orderType\": \"0B\",       \"availQty\": 1000,       \"sumDealQty\": 0     },     {       \"notDealQty\": 17000,       \"avgDealPrice\": 19.7,       \"symbolName\": \"中信金\",       \"symbol\": \"2891\",       \"orderType\": \"0B\",       \"availQty\": 20000,       \"sumDealQty\": 3000     }   ],   \"functionID\": \"RB50\" } ";
	}	
	
	/**
	 * RB51
	 * @return response json string
	 */
	public static String getRB51ResponseString() {
		return""+"{\"execuSummarys\": [ { \"avgDealPrice\": 9.03, \"symbolName\": \"開發金\", \"symbol\": \"2883\", \"orderType\": \"0S\", \"sumDealQty\": 1000}, { \"avgDealPrice\": 18.65, \"symbolName\": \"元大金\", \"symbol\": \"2885\", \"orderType\": \"0S\", \"sumDealQty\": 2000}, { \"avgDealPrice\": 20.75, \"symbolName\": \"中信金\", \"symbol\": \"2891\", \"orderType\": \"0S\",  \"sumDealQty\": 3000 }, { \"avgDealPrice\": 19.7, \"symbolName\": \"中信金\", \"symbol\": \"2891\", \"orderType\": \"0B\", \"sumDealQty\": 3000  } ], \"functionID\": \"RB51\" }";
	}	
	
	/**
	 * RB53
	 * @return response json string
	 */
	public static String getRB53ResponseString() {
		return""+"{   \"invois\": [     {       \"exdt\": \"20190918\",       \"expr\": \"113847\"     },     {       \"exdt\": \"20190919\",       \"expr\": \"0\"     },     {       \"exdt\": \"20190920\",       \"expr\": \"0\"     }   ],   \"functionID\": \"RB53\" }";
	}
}
