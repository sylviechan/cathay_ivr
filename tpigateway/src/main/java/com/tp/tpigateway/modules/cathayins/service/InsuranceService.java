package com.tp.tpigateway.modules.cathayins.service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathayins.InsuranceRequest;

import java.util.Map;

public interface InsuranceService {
	
	/**
	 * For 產險-呼叫登入頁面
	 */
	BotMessageVO getLogin(BotRequest botRequest);
	
	/**
	 * For 產險-非API回覆
	 */
	TPIGatewayResult chooseCmd(InsuranceRequest reqVO);
	
	/**
	 * For 產險-官網登入 [A01]
	 */
	TPIGatewayResult getA01(InsuranceRequest reqVO, Map<String, String> param);
	
	/**
	 * For 產險-發送OPT簡訊 [A02]
	 */
	TPIGatewayResult getA02(InsuranceRequest reqVO, Map<String, String> param);
	
	/**
	 * For 產險-驗證OPT簡訊 [A03]
	 */
	TPIGatewayResult getA03(InsuranceRequest reqVO, Map<String, String> param);
	
	/**
	 * For 產險-保單認證 [A04]
	 */
	Map<String, Object> getA04(InsuranceRequest reqVO, Map<String, String> param);
	
	/**
	 * For 產險-查詢保單清單 [A05]
	 */
	BotMessageVO getA05(InsuranceRequest reqVO, Map<String, String> param);
	
	/**
	 * For 產險-查詢保單明細_車險認證會員 [A06]
	 */
	Map<String, Object> getCarDetail(InsuranceRequest reqVO, Map<String, String> param);
	
	/**
	 * For 產險-查詢保單明細_車險一般會員 [A06]
	 */
	Map<String, Object> getCarDetailNormal(InsuranceRequest reqVO, Map<String, String> param);
	
	/**
	 * For 產險-查詢保障內容_車險 [A06]
	 */
	Map<String, Object> getCarContent(InsuranceRequest reqVO, Map<String, String> param);
	
	/**
	 * For 產險-查詢車輛資訊_車險 [A06]
	 */
	Map<String, Object> getCarInfo(InsuranceRequest reqVO, Map<String, String> param);
	
	/**
	 * For 產險-查詢保單明細_健康傷害險認證會員 [A06]
	 */
	Map<String, Object> getHealthDetail(InsuranceRequest reqVO, Map<String, String> param);
	
	/**
	 * For 產險-查詢保單明細_健康傷害險一般會員 [A06]
	 */
	Map<String, Object> getHealthDetailNormal(InsuranceRequest reqVO, Map<String, String> param);
	
	/**
	 * For 產險-查詢保障內容_健康傷害險 [A06]
	 */
	Map<String, Object> getHealthContent(InsuranceRequest reqVO, Map<String, String> param);
	
	/**
	 * For 產險-查詢關係人資訊_健康傷害險 [A06]
	 */
	Map<String, Object> getHealthManInfo(InsuranceRequest reqVO, Map<String, String> param);
	
	/**
	 * For 產險-查詢保單明細_旅遊綜合險認證會員 [A06]
	 */
	Map<String, Object> getTravelDetail(InsuranceRequest reqVO, Map<String, String> param);
	
	/**
	 * For 產險-查詢保單明細_旅遊綜合險一般會員 [A06]
	 */
	Map<String, Object> getTravelDetailNormal(InsuranceRequest reqVO, Map<String, String> param);
	
	/**
	 * For 產險-查詢保障內容_旅遊綜合險 [A06]
	 */
	Map<String, Object> getTravelContent(InsuranceRequest reqVO, Map<String, String> param);
	
	/**
	 * For 產險-查詢保障項目說明_旅遊綜合險 [A06]
	 */
	Map<String, Object> getTravelContentDesc(InsuranceRequest reqVO, Map<String, String> param);
	
	/**
	 * For 產險-查詢關係人資訊_旅遊綜合險 [A06]
	 */
	Map<String, Object> getTravelManInfo(InsuranceRequest reqVO, Map<String, String> param);
	
	/**
	 * For 產險-查詢快速理賠資訊_旅遊綜合險 [A06]
	 */
	Map<String, Object> getTravelClaimInfo(InsuranceRequest reqVO, Map<String, String> param);
	
	/**
	 * For 產險-一般會員-看保障內容
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 */
	Map<String, Object> getContentNormalReply(InsuranceRequest reqVO, Map<String, String> param);

	/**
	 * For 產險-轉真人客服 [A07]
	 */
	TPIGatewayResult getA07(InsuranceRequest reqVO, Map<String, String> param);
	
	/**
	 * For 產險-名下車輛清單(是否有道路救援) [C01]
	 */
	BotMessageVO getC01(InsuranceRequest reqVO, Map<String, String> param);
	
	/**
	 * For 產險-電子強制險證下載 [C05]
	 */
	Map<String, Object> getC05(InsuranceRequest reqVO, Map<String, String> param);
	
	/**
	 * For 產險-理賠辦理 [F01]
	 */
	BotMessageVO getF01(InsuranceRequest reqVO);
	
	/**
	 * For 產險-理賠辦理 [F02]
	 */
	BotMessageVO getF02(InsuranceRequest reqVO);
	
	/**
	 * For 產險-查詢旅綜險保單清單 [T02]
	 */
	TPIGatewayResult getT02(InsuranceRequest reqVO);
	
	/**
	 * For 產險-變更旅綜險日期選擇畫面 [getDateView]
	 */
	BotMessageVO getDateView(InsuranceRequest reqVO);
	
	/**
	 * For 產險-旅綜險保單批改 [T03]
	 */
	TPIGatewayResult getT03(InsuranceRequest reqVO, Map<String, String> param);
	
	/**
	 * For 產險-呼叫信用卡畫面
	 */
	BotMessageVO getCreditCardView(InsuranceRequest botRequest);
	/**
	 * For 產險-旅綜險確認與刷卡 [T04]
	 */
	TPIGatewayResult getT04(InsuranceRequest reqVO);
	
	/**
	 * For 產險- 熱門問題[Top Questions]
	 */
	BotMessageVO getTopQuestions(InsuranceRequest reqVO);
}
