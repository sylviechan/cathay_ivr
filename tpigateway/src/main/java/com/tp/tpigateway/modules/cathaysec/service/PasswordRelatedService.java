package com.tp.tpigateway.modules.cathaysec.service;

import java.util.Map;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotRequest;
import com.tp.common.model.cathaysec.PasswordRequest;

/*
密碼相關流程
*/
public interface PasswordRelatedService {
	// 密碼相關開頭畫面
	BotMessageVO passwordRelated(PasswordRequest reqVO);

	// 修改密碼預設畫面
	BotMessageVO pswdChangeInit(PasswordRequest reqVO);

	// 修改密碼成功
	BotMessageVO pswdChangeSuccess(PasswordRequest reqVO);

	// 修改密碼失敗
	BotMessageVO pswdChangeFail(PasswordRequest reqVO);

	// 修改密碼3次上鎖
	BotMessageVO passwordRelatedLock(PasswordRequest reqVO);

	
	// 密碼解鎖
	BotMessageVO pswdOnLockInit(PasswordRequest reqVO);

	// 密碼解鎖發送至手機
	BotMessageVO pswdSenderPhoneF(PasswordRequest reqVO);

	// 密碼解鎖成功
	BotMessageVO pswdOnLockSuccess(PasswordRequest reqVO);

	// 密碼解鎖失敗
	BotMessageVO pswdOnLockFail(PasswordRequest reqVO);

	// 密碼解鎖無法
	BotMessageVO pswdOnLockCanNot(PasswordRequest reqVO);

	// 密碼解鎖無法再送
	BotMessageVO pswdOnLockFullAmount(PasswordRequest reqVO);
	
	
	
	// 密碼補發
    BotMessageVO pswdReissueInit(PasswordRequest reqVO);
    // 密碼補發成功
 	BotMessageVO pswdReissueSuccess(PasswordRequest reqVO,String epass);

 	// 密碼補發失敗
 	BotMessageVO pswdReissueFail(PasswordRequest reqVO);

 	// 密碼補發無法
 	BotMessageVO pswdReissueCanNot(PasswordRequest reqVO, String resultCode);

 	// 密碼補發無法再送
 	BotMessageVO pswdReissueFullAmount(PasswordRequest reqVO);
 		
	//重來
 	BotMessageVO pswdReissueRt(PasswordRequest reqVO);

	// 結束
	BotMessageVO pswdStop(PasswordRequest reqVO);
	
	BotMessageVO updSpeechPwd(PasswordRequest reqVO);

	/**
	 * For 密碼變更(RB04)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	Map<String, Object> getRB04(PasswordRequest reqVO, Map<String, String> param);

	/**
	 * For 密碼解鎖-OTP 申請(RB05)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	Map<String, Object> getRB05(PasswordRequest reqVO, Map<String, String> param);

	/**
	 * For 密碼解鎖-OTP 驗證(RB06)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	Map<String, Object> getRB06(PasswordRequest reqVO, Map<String, String> param);

	/**
	 * For 密碼補發-身分檢核(RB07)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	Map<String, Object> getRB07(PasswordRequest reqVO, Map<String, String> param);

	/**
	 * For 密碼補發-OTP 申請(RB08)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	Map<String, Object> getRB08(PasswordRequest reqVO, Map<String, String> param);

	/**
	 * For 密碼補發-OTP驗證(RB09)
	 * 
	 * @param reqVO
	 * @param param
	 * @return
	 * @throws Exception
	 */
	Map<String, Object> getRB09(PasswordRequest reqVO, Map<String, String> param);


}
