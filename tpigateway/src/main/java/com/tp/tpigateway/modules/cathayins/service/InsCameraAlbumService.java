package com.tp.tpigateway.modules.cathayins.service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotRequest;

public interface InsCameraAlbumService {

	BotMessageVO cameraAlbum(BotRequest reqVO);

}
