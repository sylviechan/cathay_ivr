package com.tp.tpigateway.modules.cathaysite.fegin;

import com.tp.common.annotation.TpSysLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@FeignClient(name = "custapi", url = "${cathaysite.fund.path}")
@TpSysLog(remark = "CustAPIFeignService")
public interface CustAPIFeignService {
	@TpSysLog(remark = "FundAPIFeignService", showParams = false, showResult = false)
	@RequestMapping(value = "online/api/cust.asmx/Login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> login(Map<String, Object> obj);
	
	@RequestMapping(value = "online/api/cust.asmx/getCusbank", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> getCusbank(Map<String, Object> obj, @RequestHeader("token") String token);
	
	@RequestMapping(value = "online/api/cust.asmx/BillIdCheck", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> getBillIdCheck(Map<String, Object> obj, @RequestHeader("token") String token);
	
	@RequestMapping(value = "online/api/cust.asmx/BillSend", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
			headers= {"sysKey=${cathaysite.sysKey}","sysId=${cathaysite.sysId}"})
	Map<String, Object> getBillSend(Map<String, Object> obj, @RequestHeader("token") String token);
	
	@RequestMapping(value = "online/api/cust.asmx/SendPwdIdCheck", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, headers = {
			"sysKey=${cathaysite.sysKey}", "sysId=${cathaysite.sysId}" })
	Map<String, Object> sendPwdIdCheck(Map<String, Object> obj);

	@RequestMapping(value = "online/api/cust.asmx/SendPwd", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, headers = {
			"sysKey=${cathaysite.sysKey}", "sysId=${cathaysite.sysId}" })
	Map<String, Object> sendPwd(Map<String, Object> obj);

	@RequestMapping(value = "online/api/cust.asmx/sendCphoneCAPTCHA", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, headers = {
			"sysKey=${cathaysite.sysKey}", "sysId=${cathaysite.sysId}" })
	Map<String, Object> sendCphoneCAPTCHA(Map<String, Object> obj);

	@RequestMapping(value = "online/api/cust.asmx/checkCphoneCAPTCHA", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, headers = {
			"sysKey=${cathaysite.sysKey}", "sysId=${cathaysite.sysId}" })
	Map<String, Object> checkCphoneCAPTCHA(Map<String, Object> obj);
}
