package com.tp.tpigateway.modules.cathaysite.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysec.DefaultRequest;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.modules.cathaysite.service.DeductionDateService;

@RestController
@RequestMapping("/DeductionDate")
public class DeductionDateController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(DeductionDateController.class);

	@Autowired
	DeductionDateService deductionDateService;

	@RequestMapping(value = "/showView", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public TPIGatewayResult pwdCmd(@RequestBody DefaultRequest reqVO) {
		String cmd = reqVO.getCmd();
		BotMessageVO botMessageVO = null;
		botMessageVO = deductionDateService.deductionDate(reqVO);
		return TPIGatewayResult.ok().setData(botMessageVO);
	}

}
