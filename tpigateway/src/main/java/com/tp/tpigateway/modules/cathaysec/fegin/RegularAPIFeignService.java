package com.tp.tpigateway.modules.cathaysec.fegin;

import com.tp.common.annotation.TpSysLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "regularapi", url = "${api.sec.path}/eCathayRobo/")
@TpSysLog(remark = "RegularAPIFeignService")
public interface RegularAPIFeignService {

	@RequestMapping(value = "RB01.svc/RB01Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    String getRB01(String json);
	
	@RequestMapping(value = "RB27.svc/RB27Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    String getRB27(String json);
	
	@RequestMapping(value = "RB02.svc/RB02Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    String getRB02(String json);
	
	@RequestMapping(value = "RB03.svc/RB03Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    String getRB03(String json);
	
	@RequestMapping(value = "RB10.svc/RB10Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    String getRB10(String json);
	
	@RequestMapping(value = "RB11.svc/RB11Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    String getRB11(String json);
	
	@RequestMapping(value = "RB12.svc/RB12Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    String getRB12(String json);
	
	@RequestMapping(value = "RB17.svc/RB17Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    String getRB17(String json);
	
	@RequestMapping(value = "RB18.svc/RB18Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    String getRB18(String json);
	
	@RequestMapping(value = "RB19.svc/RB19Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    String getRB19(String json);
	
	@RequestMapping(value = "RB20.svc/RB20Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	String getRB20(String json);
	
	@RequestMapping(value = "RB22.svc/RB22Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    String getRB22(String json);
	
	@RequestMapping(value = "RB23.svc/RB23Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    String getRB23(String json);
	
	@RequestMapping(value = "RB25.svc/RB25Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    String getRB25(String json);
	
	@RequestMapping(value = "RB26.svc/RB26Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    String getRB26(String json);
}
