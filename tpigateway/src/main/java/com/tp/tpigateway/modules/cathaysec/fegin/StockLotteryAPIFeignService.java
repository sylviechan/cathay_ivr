package com.tp.tpigateway.modules.cathaysec.fegin;

import com.tp.common.annotation.TpSysLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@FeignClient(name = "stocklotteryapi", url = "${api.sec.path}/eCathayRobo/")
@TpSysLog(remark = "StockLotteryAPIFeignService")
public interface StockLotteryAPIFeignService {

	@RequestMapping(value = "RB12.svc/RB12Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> getRB12(Map<String, String> json);

	@RequestMapping(value = "RB13.svc/RB13Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> getRB13(Map<String, String> json);

	@RequestMapping(value = "RB14.svc/RB14Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> getRB14(Map<String, String> json);

	@RequestMapping(value = "RB15.svc/RB15Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> getRB15(Map<String, String> json);

	@RequestMapping(value = "RB16.svc/RB16Json", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	Map<String, Object> getRB16(Map<String, String> json);
}
