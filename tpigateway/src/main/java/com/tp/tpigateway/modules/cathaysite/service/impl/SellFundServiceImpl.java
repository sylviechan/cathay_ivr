package com.tp.tpigateway.modules.cathaysite.service.impl;

import static com.tp.common.util.cathaysite.SiteApiUtils.getCode;
import static com.tp.common.util.cathaysite.SiteApiUtils.getData;
import static com.tp.common.util.cathaysite.SiteApiUtils.getDataMap;
import static com.tp.common.util.cathaysite.SiteApiUtils.getDataValue;
import static com.tp.common.util.cathaysite.SiteApiUtils.getDetail;
import static com.tp.common.util.cathaysite.SiteApiUtils.getList;
import static com.tp.common.util.cathaysite.SiteApiUtils.getMsg;
import static com.tp.common.util.cathaysite.SiteApiUtils.isOverTime;
import static com.tp.common.util.cathaysite.SiteApiUtils.isPwdError;
import static com.tp.common.util.cathaysite.SiteApiUtils.isSuccess;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotMessageVO.CImageDataItem;
import com.tp.common.model.BotMessageVO.CImageTextItem;
import com.tp.common.model.BotMessageVO.CTextItem;
import com.tp.common.model.BotMessageVO.CardItem;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysite.SellFundRequest;
import com.tp.common.util.QuickReplay;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.modules.cathaysite.fegin.FundAPIFeignService;
import com.tp.tpigateway.modules.cathaysite.model.SiteQuickReplay;
import com.tp.tpigateway.modules.cathaysite.service.CathaySiteService;
import com.tp.tpigateway.modules.cathaysite.service.SellFundService;

@Service("sellFundService")
public class SellFundServiceImpl implements SellFundService {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	BotResponseUtils botResponseUtils;

	@Autowired
	FundAPIFeignService fundAPIFeignService;

	@Autowired
	CathaySiteService cathaySiteService;

	@Value("${cathaysite.fund.path}")
	protected String cathaysiteFundPath;

	protected NumberFormat currencyFormat = new DecimalFormat("###,###");
	protected NumberFormat unitFormat = new DecimalFormat("###,##0.##");
	protected NumberFormat unitFormat2 = new DecimalFormat("###,##0.0000");// for靠右對齊
	protected static String hint = "提醒你：送出委託時間若超過當日營業日下午四點時，就會算下一個營業日的委託喔!";

	protected Integer getInteger(Object data) {
		Integer returnInt = 0;
		if (data != null) {
			try {
				returnInt = (int) Double.parseDouble(data.toString());
			} catch (NumberFormatException e) {
				return returnInt;
			}
		}

		return returnInt;
	}

	protected Double getDouble(Object data) {
		Double returnDouble = 0d;
		if (data != null) {
			try {
				returnDouble = Double.parseDouble(data.toString());
			} catch (NumberFormatException e) {
				return returnDouble;
			}
		}

		return returnDouble;
	}

	protected String changeMoneyString(String money) {
		NumberFormat numberFormat = NumberFormat.getNumberInstance();
		return numberFormat.format(getInteger(money));
	}

	@Override
	public List<Map<String, Object>> getSellFundList(SellFundRequest reqVO, Map<String, Object> param,
			BotMessageVO vo) {
		Map<String, Object> result = fundAPIFeignService.getSellFundList(param, reqVO.getToken());

		List<Map<String, Object>> fundList = new ArrayList<>();

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				fundList = getList(data);
				for (Map<String, Object> dataMap : fundList) {
					Number aeunit = (Number) dataMap.get("aeunit");
					Number redeunit = (Number) dataMap.get("redeunit");
					BigDecimal canBuy = new BigDecimal(aeunit.doubleValue() - redeunit.doubleValue());
					// canBuy = canBuy.setScale(4, java.math.RoundingMode.HALF_UP);
					dataMap.put("canBuy", unitFormat2.format(canBuy));
				}
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return fundList;
	}

	@Override
	public BotMessageVO getSellFundList(SellFundRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		List<Map<String, Object>> list = getSellFundList(reqVO, param, vo);

		if (list.size() > 0) {
			botResponseUtils.setTextResult(vo, "請挑選你想要買回的基金:");
			botResponseUtils.setSellFundList(vo, list);
		} else {
			botResponseUtils.setTextResult(vo, "找不到你要買回的基金嗎？");
			botResponseUtils.setTextResult(vo, "有些外幣基金沒有開放電子交易，請留言或聯絡客服。");
			cathaySiteService.setSiteCustomerService(vo, reqVO.getSessionId(), reqVO.getClientIp());
		}

		return vo;
	}

	@Override
	public TPIGatewayResult getSellFundView(SellFundRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		List<Map<String, Object>> list = getSellFundList(reqVO, param, vo);

		if (list.size() > 0) {
			Map<String, Object> sellFundData = list.get(0);
			logger.info("sellFundData=" + sellFundData);

			botResponseUtils.setTextResult(vo, "請確認你所要買回的基金資訊：	");
			botResponseUtils.setTextResult(vo, hint);

			List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();

			CardItem cardItem = vo.new CardItem();
			cardItem.setCTexts(new ArrayList<Map<String, Object>>());
			cardItem.setCName("");
			cardItem.setCWidth(BotMessageVO.CWIDTH_250);
			cardItem.setCTextType("1");

			// "cImageData"
			CImageDataItem cImage = vo.new CImageDataItem();
			cImage.setCImage(BotMessageVO.IMG_CB03);
			cImage.setCImageTextType(1);
			cImage.setCImageTexts(new ArrayList<Map<String, Object>>());

			CImageTextItem cImageText = vo.new CImageTextItem();
			cImageText.setCImageTextTitle("基金名稱");
			cImageText.setCImageText((String) sellFundData.get("fund_name"));
			cImage.getCImageTexts().add(cImageText.getCImageTexts());

			Number aeunit = (Number) sellFundData.get("aeunit");
			Number redeunit = (Number) sellFundData.get("redeunit");
			BigDecimal canBuy = new BigDecimal(aeunit.doubleValue() - redeunit.doubleValue());
			cImageText = vo.new CImageTextItem();
			cImageText.setCImageTextTitle("可買回單位數");
			cImageText.setCImageText(unitFormat.format(canBuy));
			cImage.getCImageTexts().add(cImageText.getCImageTexts());

			cardItem.setCImageData(cImage.getCImageDatas());

			CTextItem cText = vo.new CTextItem();

			String navdt = (String) sellFundData.get("navdt");
			cardItem.setCMessage("資訊更新 " + formatDate(navdt));
			cardItem.setIsCMessageIconHide("true");
			cardItem.setCMessageStyle("color:black;");

			String[] cTextLabelArray = { "持有單位數", "已委託買回單位數", "控管中單位數", "投資成本", "最新淨值", "市價預估", "預估報酬率	" };
			String[] cTextArray = { "unit", "redeunit", "cunit", "cost", "nav", "val", "pl_rate" };
			for (int i = 0; i < cTextLabelArray.length; i++) {
				cText = vo.new CTextItem();
				cText.setCLabel(cTextLabelArray[i]);
				BigDecimal bd = (BigDecimal) sellFundData.get(cTextArray[i]);
				switch (cTextArray[i]) {
				case "cost":
				case "val":
				case "TWDval":
					cText.setCText("NT. " + currencyFormat.format(bd));
					break;
				case "pl_rate":
				case "pl_int_rate":
					cText.setCText(bd + "%");
					break;
				case "unit":
				case "redeunit":
				case "cunit":
				case "nav":
					cText.setCText(unitFormat.format(bd));
					break;
				}

				if (bd.floatValue() < 0)
					cText.setCTextStyle("color:red;");

				cardItem.getCTexts().add(cText.getCTexts());
			}

			QuickReplay q1 = SiteQuickReplay.CHOOSE_SELL_FUND(reqVO.getFund());
			QuickReplay q2 = SiteQuickReplay.SELECT_SELL_FUND;
			QuickReplay q3 = SiteQuickReplay.INIT_SELL_FUND;
			cardItem.setCLinkList(botResponseUtils.getCLinkList(vo, q1, q2, q3));

			resultList.add(cardItem.getCards());
			botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, resultList);

			tpiGatewayResult.setMsgList(Arrays.asList(q1.getText(), q2.getText()));
			tpiGatewayResult.setOptionList(Arrays.asList(q1, q2));

		} else {
			botResponseUtils.setTextResult(vo, "你沒有可贖回的基金資訊。");
		}
		return tpiGatewayResult.setData(vo);
	}

	@Override
	public TPIGatewayResult getSellFundForm(SellFundRequest reqVO) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		switch (reqVO.getTrcode()) {
		case "1":
			getSellFundForm1(reqVO, vo, tpiGatewayResult);
			break;
		case "8":
			getSellFundForm8(reqVO, vo, tpiGatewayResult);
			break;
		}

		return tpiGatewayResult.setData(vo);
	}

	/** 匯款牌卡 */
	protected void getSellFundForm1(SellFundRequest reqVO, BotMessageVO vo, TPIGatewayResult tpiGatewayResult) {
		botResponseUtils.setTextResult(vo, "請確認你要買回的基金交易資訊：");
		botResponseUtils.setTextResult(vo, hint);

		// 組牌卡
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		CardItem cards = vo.new CardItem();
		cards.setCLinkType(1);
		cards.setCTextType("3");
		cards.setCWidth(BotMessageVO.CWIDTH_250);

		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);

		List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("買回基金");
		cImageText.setCImageText(reqVO.getFundName());
		cImageTexts.add(cImageText.getCImageTexts());

		cImageData.setCImageTexts(cImageTexts);
		cards.setCImageData(cImageData.getCImageDatas());

		List<Map<String, Object>> cTexts = new ArrayList<Map<String, Object>>();
		cards.setCTexts(cTexts);

		cTexts.addAll(tcodeCard(reqVO, vo));

		CTextItem cText = vo.new CTextItem();
		cText.setCLabel("匯款帳戶");
		cText.setCText(reqVO.getAcntName());
		cText.setCUnit(reqVO.getAcnt());
		cTexts.add(cText.getCTexts());

		String fund = reqVO.getFund();

		QuickReplay q1 = SiteQuickReplay.BUY_SELL_FUND(fund, "是的!確定買回");
		QuickReplay q2 = SiteQuickReplay.MODIFY_SELL_FUND(fund);
		QuickReplay q3 = SiteQuickReplay.CANCEL_SELL_FUND(fund, "我不買回了");

		cards.setCLinkList(botResponseUtils.getCLinkList(vo, q1, q2, q3));

		tpiGatewayResult.setMsgList(Arrays.asList(q1.getText(), q2.getText(), q3.getText()));
		tpiGatewayResult.setOptionList(Arrays.asList(q1, q2, q3));

		cardList.add(cards.getCards());
		botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);
	}

	/** 轉申購牌卡 */
	protected void getSellFundForm8(SellFundRequest reqVO, BotMessageVO vo, TPIGatewayResult tpiGatewayResult) {
		botResponseUtils.setTextResult(vo, "請確認你要轉申購的基金交易資訊：");
		botResponseUtils.setTextResult(vo, hint);

		// 組牌卡
		List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
		CardItem cards = vo.new CardItem();
		cards.setCLinkType(1);
		cards.setCTextType("3");
		cards.setCWidth(BotMessageVO.CWIDTH_250);

		CImageDataItem cImageData = vo.new CImageDataItem();
		cImageData.setCImage(BotMessageVO.IMG_CB03);
		cImageData.setCImageTextType(1);
		cImageData.setCImageTag(reqVO.getTrfundkindTxt());

		List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
		CImageTextItem cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("買回基金");
		cImageText.setCImageText(reqVO.getFundName());
		cImageTexts.add(cImageText.getCImageTexts());

		cImageText = vo.new CImageTextItem();
		cImageText.setCImageTextTitle("轉申購基金");
		cImageText.setCImageText(reqVO.getTrfundName());
		cImageTexts.add(cImageText.getCImageTexts());

		cImageData.setCImageTexts(cImageTexts);
		cards.setCImageData(cImageData.getCImageDatas());

		List<Map<String, Object>> cTexts = new ArrayList<Map<String, Object>>();
		cards.setCTexts(cTexts);

		cTexts.addAll(tcodeCard(reqVO, vo));

		String fund = reqVO.getFund();

		QuickReplay q1 = SiteQuickReplay.BUY_SELL_FUND(fund, "是的!確定轉申購");
		QuickReplay q2 = SiteQuickReplay.MODIFY_SELL_FUND(fund);
		QuickReplay q3 = SiteQuickReplay.CANCEL_SELL_FUND(fund, "我不轉申購了");

		cards.setCLinkList(botResponseUtils.getCLinkList(vo, q1, q2, q3));

		tpiGatewayResult.setMsgList(Arrays.asList(q1.getText(), q2.getText(), q3.getText()));
		tpiGatewayResult.setOptionList(Arrays.asList(q1, q2, q3));

		cardList.add(cards.getCards());
		botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);
	}

	protected List<Map<String, Object>> tcodeCard(SellFundRequest reqVO, BotMessageVO vo) {
		List<Map<String, Object>> cTexts = new ArrayList<Map<String, Object>>();
		CTextItem cText = null;
		switch (reqVO.getTcode()) {
		case "0":
			cText = vo.new CTextItem();
			cText.setCLabel("買回單位數");
			cText.setCText("全數買回");
			cTexts.add(cText.getCTexts());
			break;
		case "3":
			cText = vo.new CTextItem();
			cText.setCLabel("買回單位數");
			cText.setCText(unitFormat.format(getDouble(reqVO.getRunit())));
			cTexts.add(cText.getCTexts());

			cText = vo.new CTextItem();
			cText.setCLabel("指定買回類別");
			switch (reqVO.getRtype()) {
			case "1":
				cText.setCText("先單筆後定額");
				break;
			case "2":
				cText.setCText("先定額後單筆");
				break;
			}
			cTexts.add(cText.getCTexts());
			break;
		case "4":
			cText = vo.new CTextItem();
			cText.setCLabel("買回金額");
			cText.setCText("NT. " + currencyFormat.format(getInteger(reqVO.getRamt())));
			cTexts.add(cText.getCTexts());

			cText = vo.new CTextItem();
			cText.setCLabel("指定買回類別");
			switch (reqVO.getRtype()) {
			case "1":
				cText.setCText("先單筆後定額");
				break;
			case "2":
				cText.setCText("先定額後單筆");
				break;
			}
			cTexts.add(cText.getCTexts());
			break;
		}
		return cTexts;
	}

	@Override
	public BotMessageVO getSellFundTxpwd(SellFundRequest reqVO, String errMsg) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		List<Map> linkList = new ArrayList<>();

		Map<String, Object> param = new HashMap<>();
		Map<String, String> l1obj = new HashMap<String, String>();
		l1obj.put("SessionID", reqVO.getSessionId());
		l1obj.put("ClientIP", reqVO.getClientIp());
		l1obj.put("fund", reqVO.getFund());
		param.put("obj", l1obj);
		Map<String, Object> result = fundAPIFeignService.getSellFundDetail(param, reqVO.getToken());
		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				logger.info("data=" + data);
				Map<String, Object> dataMap = getDetail(data);

				if (StringUtils.equals(reqVO.getTrcode(), "8") && StringUtils.isNotBlank(reqVO.getTrfund())) {
					List<Map<String, Object>> trfund_list = (List<Map<String, Object>>) dataMap.get("trfund_list");
					for (Map<String, Object> trfund : trfund_list) {
						String fund = (String) trfund.get("fund");
						if (StringUtils.equals(fund, reqVO.getTrfund())) {
							String fund_doc = (String) trfund.get("fund_doc");
							if (StringUtils.isNotBlank(fund_doc)) {
								String fund_doc_url = cathaysiteFundPath + fund_doc;
								linkList.add(botResponseUtils
										.getLinkListItem(vo, new SiteQuickReplay(4, "公開說明書", "", "", fund_doc_url, ""))
										.getLinkList());
							}
						}
					}
				}

				Map<String, String> declare = (Map<String, String>) dataMap.get("Declare");
				for (String key : declare.keySet()) {
					l1obj.put("query", key);
					Map<String, Object> resultOne = fundAPIFeignService.getOne(param, reqVO.getToken());
					if (resultOne != null) {
						Map<String, Object> dataOne = getData(resultOne);
						String title = "", alt = "";
						if (isSuccess(dataOne)) {
							Map<String, Object> dvOne = getDataValue(dataOne);
							Map<String, Object> mapDataOne = (Map<String, Object>) dvOne.get("data");
							title = (String) mapDataOne.get("title");
							alt = (String) mapDataOne.get("txt");
							String stype = (String) mapDataOne.get("stype");

							if (StringUtils.equals(stype, "2")) {
								// 純文字
								alt = alt.replaceAll("\r\n", "<br>");
							}

						} else {
							String code = getCode(dataOne), msg = getMsg(dataOne);
							logger.error("code = {}, msg = {}", code, msg);
							alt = code + ":" + StringUtils.defaultString(msg);
						}
						linkList.add(botResponseUtils
								.getLinkListItem(vo, new SiteQuickReplay(0, title, alt, "", "", "")).getLinkList());
					}
				}

				botResponseUtils.doTxPwd(vo, "確認贖回", "點選確認贖回", "基金", "doBuyFund_sellFund", MapUtils.EMPTY_MAP, linkList,
						errMsg);
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return vo;
	}

	@Override
	public TPIGatewayResult sellSetAdd(SellFundRequest reqVO, Map<String, Object> param) {
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		Map<String, Object> result = fundAPIFeignService.sellSetAdd(param, reqVO.getToken());

		boolean isOverTime = false;
		boolean isSuccess = false;

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				isSuccess = true;
				logger.info("data=" + data);
				Map<String, Object> dataMap = getDataMap(data);
				if (dataMap.size() > 0) {
					List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
					CardItem cardItem = vo.new CardItem();
					cardItem.setCName("");
					cardItem.setCWidth(BotMessageVO.CWIDTH_250);
					cardItem.setCTextType("12");
					cardItem.setCLinkType(1);

					// 牌卡圖片
					CImageDataItem cImageData = vo.new CImageDataItem();
					cImageData.setCImage(BotMessageVO.IMG_GENERIC);
					cImageData.setCImageUrl("card33-02.png");
					cImageData.setCImageTextType(1);
					cardItem.setCImageData(cImageData.getCImageDatas());

					List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
					switch (reqVO.getTrcode()) {
					case "1":
						cTextList.addAll(sellSetAddResult1(reqVO, vo, dataMap));
						break;
					case "8":
						cTextList.addAll(sellSetAddResult8(reqVO, vo, dataMap));
						break;
					}

					cardItem.setCTexts(cTextList);
					cardList.add(cardItem.getCards());

					botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);
					botResponseUtils.setTextResult(vo, "等下會寄委託通知單到你的約定信箱");

					cardList = new ArrayList<Map<String, Object>>();
					cardItem = vo.new CardItem();
					cardItem.setCLinkType(1);
					cardItem.setCWidth(BotMessageVO.CWIDTH_250);

					// 牌卡圖片
					cImageData = vo.new CImageDataItem();
					cImageData.setCImage(BotMessageVO.IMG_CB03_ROUND_FH);
					cImageData.setCImageTextType(1);

					List<Map<String, Object>> cImageTextList = new ArrayList<>();
					CImageTextItem cImageText = vo.new CImageTextItem();
					cImageText.setCImageTextTitle("Email：");
					cImageText.setCImageText(dataMap.get("email").toString());
					cImageTextList.add(cImageText.getCImageTexts());
					cImageData.setCImageTexts(cImageTextList);
					cardItem.setCImageData(cImageData.getCImageDatas());

					cardList.add(cardItem.getCards());

					botResponseUtils.setCardsResult19(vo, BotMessageVO.CTYPE_CARD, cardList);

					botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01,
							SiteQuickReplay.BUT_OTHER_SELL_FUND, SiteQuickReplay.INIT);
				} else {
					botResponseUtils.setTextResult(vo, "你沒有可買回的基金資訊。");
				}
			} else if (isOverTime(data)) {
				isOverTime = true;
				botResponseUtils.setImageResult(vo, BotMessageVO.EMBARRASSED_PIC);
				botResponseUtils.setTextResult(vo, "Sorry！交易時間已超過今天的交易截止時間(下午4:00)，你今天的此筆申購失敗！");
				botResponseUtils.setTextResult(vo, "需要阿發自動幫你遞延此交易到下一個營業日嗎！");

				QuickReplay q1 = SiteQuickReplay.AUTO_DELAY_ONE_FUND;
				QuickReplay q2 = SiteQuickReplay.RETRY_ONE_FUND;

				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, SiteQuickReplay.AUTO_DELAY_SELL_FUND,
						SiteQuickReplay.RETRY_SELL_FUND);

				tpiGatewayResult.setMsgList(Arrays.asList(q1.getText(), q2.getText()));
				tpiGatewayResult.setOptionList(Arrays.asList(q1, q2));

			} else if (isPwdError(data)) {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				vo = getSellFundTxpwd(reqVO, StringUtils.defaultString(msg));
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, /* code + ":" + */ StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		tpiGatewayResult.put("isOverTime", isOverTime);
		tpiGatewayResult.put("isSuccess", isSuccess);

		return tpiGatewayResult.setData(vo);
	}

	protected List<Map<String, Object>> sellSetAddResult1(SellFundRequest reqVO, BotMessageVO vo,
			Map<String, Object> dataMap) {
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		CTextItem cText = vo.new CTextItem();
		cText.setCLabel("買回基金");
		cText.setCText(dataMap.get("fund_name").toString());
		cTextList.add(cText.getCTexts());

		cText = vo.new CTextItem();
		cText.setCLabel("預計買回日");
		cText.setCText(formatDate(dataMap.get("rdate").toString()));
		cTextList.add(cText.getCTexts());

		cText = vo.new CTextItem();
		cText.setCLabel("預計付款日");
		cText.setCText(formatDate(dataMap.get("paydate").toString()));
		cTextList.add(cText.getCTexts());

		cText = vo.new CTextItem();
		cText.setCLabel("預計淨值日");
		cText.setCText(formatDate(dataMap.get("rnavd").toString()));
		cTextList.add(cText.getCTexts());

		cTextList.addAll(tcodeResult(reqVO, vo, dataMap));

		cText = vo.new CTextItem();
		cText.setCLabel("匯款帳戶");
		cText.setCText(dataMap.get("branch").toString());
		cText.setCUnit(dataMap.get("acnt").toString());
		cTextList.add(cText.getCTexts());

		String CurrentTime = (String) dataMap.get("CurrentTime");
		if (StringUtils.isNotBlank(CurrentTime)) {
			String d = CurrentTime.substring(0, CurrentTime.indexOf(" "));
			String t = CurrentTime.substring(CurrentTime.indexOf(" ") + 1, CurrentTime.indexOf("."));
			cText = vo.new CTextItem();
			cText.setCLabel("委託時間");
			cText.setCText(d);
			cText.setCUnit(t);
			cTextList.add(cText.getCTexts());
		}

		return cTextList;
	}

	protected List<Map<String, Object>> sellSetAddResult8(SellFundRequest reqVO, BotMessageVO vo,
			Map<String, Object> dataMap) {
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		CTextItem cText = vo.new CTextItem();
		cText.setCLabel("買回基金");
		cText.setCText(dataMap.get("fund_name").toString());
		cTextList.add(cText.getCTexts());

		List<String> trfund_txt = (List<String>) dataMap.get("trfund_txt");
		if (trfund_txt != null)
			for (String string : trfund_txt) {
				cText = vo.new CTextItem();
				cText.setCLabel("轉申購基金");
				cText.setCText(string);
				cTextList.add(cText.getCTexts());
			}

		cText = vo.new CTextItem();
		cText.setCLabel("預計買回日");
		cText.setCText(formatDate(dataMap.get("rdate").toString()));
		cTextList.add(cText.getCTexts());

		cText = vo.new CTextItem();
		cText.setCLabel("預計淨值日");
		cText.setCText(formatDate(dataMap.get("rnavd").toString()));
		cTextList.add(cText.getCTexts());

		List<String> trdate = (List<String>) dataMap.get("trdate");
		if (trdate != null)
			for (String string : trdate) {
				cText = vo.new CTextItem();
				cText.setCLabel("預計轉申購日");
				cText.setCText(formatDate(string));
				cTextList.add(cText.getCTexts());
			}

		cTextList.addAll(tcodeResult(reqVO, vo, dataMap));

		String CurrentTime = (String) dataMap.get("CurrentTime");
		if (StringUtils.isNotBlank(CurrentTime)) {
			String d = CurrentTime.substring(0, CurrentTime.indexOf(" "));
			String t = CurrentTime.substring(CurrentTime.indexOf(" ") + 1, CurrentTime.indexOf("."));
			cText = vo.new CTextItem();
			cText.setCLabel("委託時間");
			cText.setCText(d);
			cText.setCUnit(t);
			cTextList.add(cText.getCTexts());
		}

		return cTextList;
	}

	protected String formatDate(String date) {
		try {
			Date d = DateUtils.parseDate(date, "yyyyMMdd");
			date = DateFormatUtils.format(d, "yyyy/MM/dd");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	protected List<Map<String, Object>> tcodeResult(SellFundRequest reqVO, BotMessageVO vo,
			Map<String, Object> dataMap) {
		List<Map<String, Object>> cTextList = new ArrayList<Map<String, Object>>();
		CTextItem cText = null;

		switch (reqVO.getTcode()) {
		case "0":
		case "3":
			cText = vo.new CTextItem();
			cText.setCLabel("買回單位數");
			cText.setCText(unitFormat.format(dataMap.get("runit")));
			cTextList.add(cText.getCTexts());
			break;
		case "4":
			cText = vo.new CTextItem();
			cText.setCLabel("買回金額");
			cText.setCText("NT. " + currencyFormat.format(dataMap.get("ramt")));
			cTextList.add(cText.getCTexts());
			break;
		}

		cText = vo.new CTextItem();
		cText.setCLabel("手續費率");
		cText.setCText(dataMap.get("crate").toString() + "%");
		cTextList.add(cText.getCTexts());

		String rtype_txt = (String) dataMap.get("rtype_txt");
		if (StringUtils.isNotBlank(rtype_txt)) {
			cText = vo.new CTextItem();
			cText.setCLabel("指定買回類別");
			cText.setCText(rtype_txt);
			cTextList.add(cText.getCTexts());
		}
		return cTextList;
	}

	@Override
	public TPIGatewayResult waitSellFundTcode(SellFundRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		Map<String, Object> result = fundAPIFeignService.getSellFundDetail(param, reqVO.getToken());

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				logger.info("data=" + data);
				Map<String, Object> dataMap = getDetail(data);
				String amtcd = (String) dataMap.get("amtcd");

				botResponseUtils.setTextResult(vo, "你想要哪一種方式買回單位數呢?");

				List<QuickReplay> quickReplayList = new ArrayList<>();

				QuickReplay q1 = SiteQuickReplay.TCODE1_SELL_FUND;
				QuickReplay q2 = SiteQuickReplay.TCODE2_SELL_FUND;
				QuickReplay q3 = SiteQuickReplay.TCODE3_SELL_FUND;

				quickReplayList.add(q1);
				quickReplayList.add(q2);
				if (StringUtils.equals(amtcd, "1"))
					quickReplayList.add(q3);

				botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_04,
						quickReplayList.toArray(new QuickReplay[0]));

				tpiGatewayResult.setMsgList(Arrays.asList(q1.getText(), q2.getText(), q3.getText()));
				tpiGatewayResult.setOptionList(Arrays.asList(q1, q2, q3));

			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return tpiGatewayResult.setData(vo);
	}

	@Override
	public BotMessageVO waitSellFundRunit(SellFundRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		Map<String, Object> result = fundAPIFeignService.getSellFundDetail(param, reqVO.getToken());

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				logger.info("data=" + data);
				Map<String, Object> dataMap = getDetail(data);
				Number solow_limit = (Number) dataMap.get("solow_limit");

				doWaitSellFundRunit(vo, solow_limit, null);
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return vo;
	}

	protected void doWaitSellFundRunit(BotMessageVO vo, Number solow_limit, Number rest_aeunit) {
		String msg = "請輸入你想要買回的單位數。提醒你，買回的單位數不能小於最低買回單位數" + unitFormat.format(solow_limit) + "。";
		if (rest_aeunit != null)
			msg += "輸入買回單位數不能超過全部單位數" + unitFormat.format(rest_aeunit) + "。";
		botResponseUtils.setTextResult(vo, msg);
	}

	@Override
	public TPIGatewayResult validateSellFundRunit(SellFundRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		Map<String, Object> result = fundAPIFeignService.getSellFundDetail(param, reqVO.getToken());

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				logger.info("data=" + data);
				Map<String, Object> dataMap = getDetail(data);
				Number solow_limit = (Number) dataMap.get("solow_limit");
				Number rest_aeunit = (Number) dataMap.get("rest_aeunit");

				String checkResult = "Y";

				// 整數檢核
				String runit = reqVO.getRunit().replaceAll(",", "").replaceAll(" ", "");
				Double doubleVal = getDouble(runit);
				if (doubleVal == 0) {
					doWaitSellFundRunit(vo, solow_limit, null);
					checkResult = "N";
				} else if (doubleVal < solow_limit.doubleValue()) {
					doWaitSellFundRunit(vo, solow_limit, null);
					checkResult = "N";
				} else if (doubleVal > rest_aeunit.doubleValue()) {
					doWaitSellFundRunit(vo, solow_limit, rest_aeunit);
					checkResult = "N";
				}
				tpiGatewayResult.put("checkResult", checkResult);
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return tpiGatewayResult.setData(vo);
	}

	@Override
	public TPIGatewayResult waitSellFundRtype(SellFundRequest reqVO) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		botResponseUtils.setTextResult(vo, "你想要指定哪種買回類別?");

		QuickReplay q1 = SiteQuickReplay.RTYPE1_SELL_FUND;
		QuickReplay q2 = SiteQuickReplay.RTYPE2_SELL_FUND;

		botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, q1, q2);

		tpiGatewayResult.setMsgList(Arrays.asList(q1.getText(), q2.getText()));
		tpiGatewayResult.setOptionList(Arrays.asList(q1, q2));

		return tpiGatewayResult.setData(vo);
	}

	@Override
	public BotMessageVO waitSellFundRamt(SellFundRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		Map<String, Object> result = fundAPIFeignService.getSellFundDetail(param, reqVO.getToken());

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				logger.info("data=" + data);
				Map<String, Object> dataMap = getDetail(data);
				Number lower_amt = (Number) dataMap.get("lower_amt");

				doWaitSellFundRamt(vo, lower_amt, null);
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return vo;
	}

	protected void doWaitSellFundRamt(BotMessageVO vo, Number lower_amt, Number upper_amt) {
		String msg = "請輸入你想要買回的金額。提醒你，買回的金額不能少於金額買回下限(推估)" + changeMoneyString(lower_amt.toString()) + "。";
		if (upper_amt != null)
			msg += "輸入買回金額不能超過金額買回上限(推估)" + changeMoneyString(upper_amt.toString()) + "。";
		botResponseUtils.setTextResult(vo, msg);
	}

	@Override
	public TPIGatewayResult validateSellFundRamt(SellFundRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		Map<String, Object> result = fundAPIFeignService.getSellFundDetail(param, reqVO.getToken());

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				logger.info("data=" + data);
				Map<String, Object> dataMap = getDetail(data);
				Number lower_amt = (Number) dataMap.get("lower_amt");
				Number upper_amt = (Number) dataMap.get("upper_amt");

				String checkResult = "Y";

				// 金錢檢核
				String amt = reqVO.getRamt().replaceAll(",", "").replaceAll("NTD", "").replaceAll(" ", "");
				Integer intVal = getInteger(amt);
				if (intVal == 0) {
					doWaitSellFundRamt(vo, lower_amt, null);
					checkResult = "N";
				} else if (intVal < lower_amt.intValue()) {
					doWaitSellFundRamt(vo, lower_amt, null);
					checkResult = "N";
				} else if (intVal > upper_amt.intValue()) {
					doWaitSellFundRamt(vo, lower_amt, upper_amt);
					checkResult = "N";
				}
				tpiGatewayResult.put("checkResult", checkResult);
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return tpiGatewayResult.setData(vo);
	}

	@Override
	public TPIGatewayResult waitSellFundTrcode(SellFundRequest reqVO) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		botResponseUtils.setTextResult(vo, "你想要領取匯款或是轉申購其他基金呢?");

		QuickReplay q1 = SiteQuickReplay.TRCODE1_SELL_FUND;
		QuickReplay q2 = SiteQuickReplay.TRCODE2_SELL_FUND;

		botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, q1, q2);

		tpiGatewayResult.setMsgList(Arrays.asList(q1.getText(), q2.getText()));
		tpiGatewayResult.setOptionList(Arrays.asList(q1, q2));

		return tpiGatewayResult.setData(vo);
	}

	@Override
	public BotMessageVO waitSellFundAccount(SellFundRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		Map<String, Object> result = fundAPIFeignService.getSellFundDetail(param, reqVO.getToken());

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				logger.info("data=" + data);
				Map<String, Object> dataMap = getDetail(data);
				JSONArray acnts = (JSONArray) dataMap.get("acnts");

				botResponseUtils.setTextResult(vo, "想要匯款到哪一個約定買回帳戶呢?");

				List<Map<String, Object>> cardList = new ArrayList<Map<String, Object>>();
				for (int i = 0; i < acnts.size(); i++) {
					Map<String, Object> acntsMap = JSON.parseObject(acnts.getString(i), Map.class);

					if (acntsMap.size() > 0) {
						String acnt = (String) acntsMap.get("acntno");
						String acntName = (String) acntsMap.get("bdesc");
						String sno = (String) acntsMap.get("sno");

						CardItem cardItem = vo.new CardItem();
						cardItem.setCLinkType(1);
						cardItem.setCWidth(BotMessageVO.CWIDTH_250);

						CImageDataItem cImageData = vo.new CImageDataItem();
						cImageData.setCImage(BotMessageVO.IMG_CB03);
						cImageData.setCImageTextType(1);

						List<Map<String, Object>> cImageTexts = new ArrayList<Map<String, Object>>();
						CImageTextItem cImageText = vo.new CImageTextItem();
						cImageText.setCImageTextTitle("銀行");
						cImageText.setCImageText(acntName);
						cImageTexts.add(cImageText.getCImageTexts());

						cImageText = vo.new CImageTextItem();
						cImageText.setCImageTextTitle("帳號");
						cImageText.setCImageText(acnt);
						cImageTexts.add(cImageText.getCImageTexts());

						cImageData.setCImageTexts(cImageTexts);
						cardItem.setCImageData(cImageData.getCImageDatas());

						String intent = "基金";
						String secondBotIntent = "toUpdateAccount_sellFund";
						Map<String, Object> parameter = new HashMap<>();
						parameter.put("fund", reqVO.getFund());
						parameter.put("sno", sno);
						parameter.put("acnt", acnt);
						parameter.put("acntName", acntName);

						QuickReplay q1 = new SiteQuickReplay(2, "就匯到這個帳戶", sno + "-" + acnt, intent, secondBotIntent,
								parameter);

						cardItem.setCLinkList(botResponseUtils.getCLinkList(vo, q1));
						cardList.add(cardItem.getCards());
					}
				}

				botResponseUtils.setCardsResult(vo, BotMessageVO.CTYPE_CARD, cardList);
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return vo;
	}

	@Override
	public BotMessageVO waitSellFundTrfund(SellFundRequest reqVO, Map<String, Object> param) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		Map<String, Object> result = fundAPIFeignService.getSellFundDetail(param, reqVO.getToken());

		if (result != null) {
			Map<String, Object> data = getData(result);
			if (isSuccess(data)) {
				logger.info("data=" + data);
				Map<String, Object> dataMap = getDetail(data);
				List<Map<String, Object>> trfund_list = (List<Map<String, Object>>) dataMap.get("trfund_list");
				if (trfund_list.size() > 0) {
					botResponseUtils.setTextResult(vo, "請選擇轉申購哪一檔基金呢?");
					botResponseUtils.setSellFundTrfundList(vo, trfund_list);
				} else {
					botResponseUtils.setTextResult(vo, "查不到可以轉申購的基金");
					cathaySiteService.setSiteCustomerService(vo, reqVO.getSessionId(), reqVO.getClientIp());
				}
			} else {
				String code = getCode(data), msg = getMsg(data);
				logger.error("code = {}, msg = {}", code, msg);
				botResponseUtils.setTextResult(vo, code + ":" + StringUtils.defaultString(msg));
			}
		} else {
			logger.error("result is null");
			botResponseUtils.setTextResult(vo, "API failed!");
		}

		return vo;
	}

	@Override
	public TPIGatewayResult modifySellFund(SellFundRequest reqVO) {
		BotMessageVO vo = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();

		// 轉申購
		switch (reqVO.getTrcode()) {
		case "1":
			modifySellFund1(reqVO, vo, tpiGatewayResult);
			break;
		case "8":
			modifySellFund8(reqVO, vo, tpiGatewayResult);
			break;
		}

		return tpiGatewayResult.setData(vo);
	}

	/** 匯款修改項目 */
	protected void modifySellFund1(SellFundRequest reqVO, BotMessageVO vo, TPIGatewayResult tpiGatewayResult) {
		botResponseUtils.setTextResult(vo, "請問你要修改哪個買回項目呢?");

		QuickReplay q1 = SiteQuickReplay.MODIFY_SELL_FUND;
		QuickReplay q2 = SiteQuickReplay.MODIFY_SELL_ACCOUNT;
		QuickReplay q3 = SiteQuickReplay.MODIFY_SELL_TCODE;
		QuickReplay q4 = SiteQuickReplay.MODIFY_SELL_TRCODE8();

		botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, q3, q4, q2, q1);

		tpiGatewayResult.setMsgList(Arrays.asList(q2.getText(), q1.getText()));
		tpiGatewayResult.setOptionList(Arrays.asList(q2, q1));
	}

	/** 轉申購修改項目 */
	protected void modifySellFund8(SellFundRequest reqVO, BotMessageVO vo, TPIGatewayResult tpiGatewayResult) {
		botResponseUtils.setTextResult(vo, "請問你要修改哪個轉申購項目呢?");

		QuickReplay q1 = SiteQuickReplay.MODIFY_SELL_FUND;
		QuickReplay q2 = SiteQuickReplay.MODIFY_SELL_TRFUND;
		QuickReplay q3 = SiteQuickReplay.MODIFY_SELL_TCODE;
		QuickReplay q4 = SiteQuickReplay.MODIFY_SELL_TRCODE1();

		botResponseUtils.setQuickReplayResult(vo, BotMessageVO.LTYPE_01, q3, q4, q2, q1);

		tpiGatewayResult.setMsgList(Arrays.asList(q2.getText(), q1.getText()));
		tpiGatewayResult.setOptionList(Arrays.asList(q2, q1));
	}
}