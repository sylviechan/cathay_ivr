package com.tp.tpigateway.modules.cathaysec.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysec.DefaultRequest;
import com.tp.common.model.cathaysec.RegularRequest;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.common.util.PropUtils;
import com.tp.tpigateway.modules.cathaysec.service.OtherService;


@RestController
@RequestMapping("/Other")
public class OtherController extends BaseController {
    private static Logger logger = LoggerFactory.getLogger(OtherController.class);
    private static final String SOURCE = "other";

    @Autowired
    OtherService otherService;

    @RequestMapping(value = "/cmd", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult otherCmd(@RequestBody DefaultRequest reqVO) {
        BotMessageVO botMessageVO = null;
        long costTimeStart = System.currentTimeMillis();
        String cmd = reqVO.getCmd();
        Map<String, Object> result = null;

        try {
            botMessageVO = otherService.chooseCmd(reqVO);
			/*switch (cmd) {
				case "rb21_query1": 
	            break;
			}*/
        } finally {
            logger.error("[" + SOURCE + "_cmd(" + reqVO.getCmd() + ")] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
        }

        return TPIGatewayResult.ok().setData(botMessageVO);
    }

    @RequestMapping(value = "/RB20", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult RB20(@RequestBody RegularRequest reqVO) {
        BotMessageVO botMessageVO = null;
        long costTimeStart = System.currentTimeMillis();
        Map<String, Object> result = null;

        try {
            // 設定必要參數
            Map<String, String> requireParams = new HashMap<String, String>();
            requireParams.put("FunctionID", "RB20");
            requireParams.put("ID", reqVO.getId());
            requireParams.put("Channel", PropUtils.getApiChannel());

            // 必要參數檢核
            if (checkParams(requireParams)) {
                result = otherService.getRB20(reqVO, requireParams);
                botMessageVO = (BotMessageVO) result.get("botMessageVO");
            }

        } finally {
            logger.error("[" + SOURCE + "_RB20] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
        }

        return TPIGatewayResult.ok().setData(botMessageVO).put("accountDataMap", result.get("accountDataMap"));
    }

    @RequestMapping(value = "/RB20Action", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public TPIGatewayResult RB20Action(@RequestBody RegularRequest reqVO) {
        BotMessageVO botMessageVO = null;
        long costTimeStart = System.currentTimeMillis();
        Map<String, Object> result = null;

        try {
            botMessageVO = otherService.getRB20Action(reqVO);
        } finally {
            logger.error("[" + SOURCE + "_RB20Action] total cost time = " + (System.currentTimeMillis() - costTimeStart) + " ms");
        }

        return TPIGatewayResult.ok().setData(botMessageVO);
    }

}
