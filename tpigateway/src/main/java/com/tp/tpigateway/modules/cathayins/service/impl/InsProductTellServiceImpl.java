package com.tp.tpigateway.modules.cathayins.service.impl;

import com.alibaba.fastjson.JSON;
import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotMessageVO.CImageDataItem;
import com.tp.common.model.BotMessageVO.CardItem;
import com.tp.common.model.BotMessageVO.LinkListItem;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathayins.InsProductTellRequest;
import com.tp.common.util.DataUtils;
import com.tp.common.util.DemandProductionCardUtis;
import com.tp.common.util.QuickReplay;
import com.tp.tpigateway.common.util.BotResponseUtils;
import com.tp.tpigateway.common.util.InsUrlGather;
import com.tp.tpigateway.modules.cathayins.fegin.InsProductTellAPIFeignService;
import com.tp.tpigateway.modules.cathayins.fegin.InsuranceAPIFeignService;
import com.tp.tpigateway.modules.cathayins.model.InsQuickReplay;
import com.tp.tpigateway.modules.cathayins.service.InsProductTellService;
import com.tp.tpigateway.modules.cathayins.utils.InsData;
import com.tp.tpigateway.modules.cathaysec.model.SecQuickReplay;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 商品介紹相關
 */

@Service("insProductTellService")
public class InsProductTellServiceImpl implements InsProductTellService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	BotResponseUtils botResponseUtils;

	@Autowired
	InsuranceAPIFeignService insuranceAPIFeignService;
	
	@Autowired
	InsProductTellAPIFeignService insProductTellAPIFeignService;

	// 商品-初始畫面
	// 商品大類
	@Override
	public TPIGatewayResult commodityBigSort(InsProductTellRequest reqVO) {
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		reqVO.AnalyzeCommodityType();
		botResponseUtils.setTextResult(botMessageVO, "你想了解那些商品呢?");
		
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("PRODUCT_CODE", reqVO.getProductCode());
		map.put("PRODUCT_SUB_CODE",reqVO.getProductSubCode());
		
		Map<String, Object> req = new HashMap<String, Object>();
		req.put("map", map);
		
		//產險測試環境
		Map<String,Object> result = JSON.parseObject(insProductTellAPIFeignService.getT01(req),Map.class);
		//公司測試用
		//Map<String,Object>  result = JSON.parseObject(InsTestApiData.commodityBigSortString());
		
		Map<String, Object> respDetail = (Map<String, Object>) result.get("detail");
		if (StringUtils.equals(respDetail.get("RETURN_CODE").toString(), "0000")) {
			List<Map<String, Object>> datas = (List<Map<String, Object>>) respDetail.get("DATAS");
			
			//商品大類ID
			Map<String, Object> parameter = new HashMap<String, Object>();
			parameter.put("commodityType", "PC01_A");

			String insType = reqVO.getInsType(); //指定險種
			String conflictType = reqVO.getConflictType();
			List<String> imageUrls = new ArrayList<>();
			List<QuickReplay> commodityBigSors = new ArrayList<>();

			//1:車險 2:旅綜險 3:健傷險 4:其他
			if (StringUtils.isNotBlank(insType)) {
				addCommodityBigSorsAndImgUrls(insType, commodityBigSors, imageUrls, "PC01_A", reqVO.getSsoId());
			}
			else {
				if (StringUtils.isNotBlank(conflictType)) {
					if (StringUtils.contains(InsData.conflictTypeMap.get(conflictType), "1")) {
						addCommodityBigSorsAndImgUrls("1", commodityBigSors, imageUrls, "", reqVO.getSsoId());
					}
					if (StringUtils.contains(InsData.conflictTypeMap.get(conflictType), "2")) {
						addCommodityBigSorsAndImgUrls("2", commodityBigSors, imageUrls, "PC01_A", reqVO.getSsoId());
					}
					if (StringUtils.contains(InsData.conflictTypeMap.get(conflictType), "3")) {
						addCommodityBigSorsAndImgUrls("3", commodityBigSors, imageUrls, "", reqVO.getSsoId());
					}
					if (StringUtils.contains(InsData.conflictTypeMap.get(conflictType), "4")) {
						addCommodityBigSorsAndImgUrls("4", commodityBigSors, imageUrls, "", reqVO.getSsoId());
					}
				} else {
					addCommodityBigSorsAndImgUrls("1", commodityBigSors, imageUrls, "", reqVO.getSsoId());
					addCommodityBigSorsAndImgUrls("2", commodityBigSors, imageUrls, "PC01_A", reqVO.getSsoId());
					addCommodityBigSorsAndImgUrls("3", commodityBigSors, imageUrls, "", reqVO.getSsoId());
					addCommodityBigSorsAndImgUrls("4", commodityBigSors, imageUrls, "", reqVO.getSsoId());
				}
			}

			botResponseUtils.setCardsResult(botMessageVO, BotMessageVO.CTYPE_CARD, getCommodityBigSortList(tpiGatewayResult, botMessageVO, imageUrls, commodityBigSors));

//			QuickReplay quickReplay = InsQuickReplay.HELLO_M01_2(parameter);
//			QuickReplay[] commodityBigSors = {
//					quickReplay, InsQuickReplay.HELLO_M01_1(reqVO.getSsoId()), InsQuickReplay.HELLO_M01_3(reqVO.getSsoId()), InsQuickReplay.HELLO_M01_4(reqVO.getSsoId())
//			};
			
//			botResponseUtils.setCardsResult(botMessageVO, BotMessageVO.CTYPE_CARD, getCommodityBigSortList(tpiGatewayResult,botMessageVO,ImageUrls,commodityBigSors));
		}
		
		return tpiGatewayResult.setData(botMessageVO);
	}

	private void addCommodityBigSorsAndImgUrls(String type, List<QuickReplay> commodityBigSors, List<String> imageUrls, String commodityType, String ssoId) {
		Map<String, Object> parameter = new HashMap<>();
		parameter.put("commodityType", commodityType);

		if (StringUtils.equals(type, "1")) {
			commodityBigSors.add(InsQuickReplay.HELLO_M01_1(ssoId));
			imageUrls.add("card-Ins-car@2x.png");
		}
		else if (StringUtils.equals(type, "2")) {
			commodityBigSors.add(InsQuickReplay.HELLO_M01_2(parameter));
			imageUrls.add("card-b38.jpg");
		}
		else if (StringUtils.equals(type, "3")) {
			commodityBigSors.add(InsQuickReplay.HELLO_M01_3(ssoId));
			imageUrls.add("card-Ins-hospital@2x.png");
		}
		else if (StringUtils.equals(type, "4")) {
			commodityBigSors.add(InsQuickReplay.HELLO_M01_4(ssoId));
			imageUrls.add("card17.png");
		}
	}

	// 商品中類
	@Override
	public TPIGatewayResult commodityMediumSort(InsProductTellRequest reqVO) {
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		reqVO.AnalyzeCommodityType();
		botResponseUtils.setTextResult(botMessageVO, "是海外旅遊還是國內旅遊呢？");
		
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("PRODUCT_CODE", reqVO.getProductCode());
		map.put("PRODUCT_SUB_CODE",reqVO.getProductSubCode());
		map.put("CODE_ID_L1", reqVO.getCodeIdL1());
		
		Map<String, Object> req = new HashMap<String, Object>();
		req.put("map", map);
		
		//產險測試環境
		Map<String,Object>  result = JSON.parseObject(insProductTellAPIFeignService.getT01(req),Map.class);
		//公司測試用
		//Map<String,Object>  result = JSON.parseObject(InsTestApiData.commodityMediumSortString());
		
		Map<String, Object> respDetail = (Map<String, Object>) result.get("detail");
		if (StringUtils.equals(respDetail.get("RETURN_CODE").toString(), "0000")) {
			List<Map<String, Object>> datas = (List<Map<String, Object>>) respDetail.get("DATAS");
			botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_03, commodityMediumSortList(tpiGatewayResult,botMessageVO,datas));
		}
		
		return tpiGatewayResult.setData(botMessageVO);
	}

	// 商品小類
	@Override
	public TPIGatewayResult commoditySmallSort(InsProductTellRequest reqVO) {
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		reqVO.AnalyzeCommodityType();
		
		botResponseUtils.setTextResult(botMessageVO, "你想了解哪個項目呢？");
	
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("PRODUCT_CODE", reqVO.getProductCode());
		map.put("PRODUCT_SUB_CODE",reqVO.getProductSubCode());
		map.put("CODE_ID_L1", reqVO.getCodeIdL1());
		map.put("CODE_ID_L2", reqVO.getCodeIdL2());
		
		Map<String, Object> req = new HashMap<String, Object>();
		req.put("map", map);
		
		//產險測試環境
		Map<String,Object>  result = JSON.parseObject(insProductTellAPIFeignService.getT01(req),Map.class);
		//公司測試用
		//Map<String,Object>  result = JSON.parseObject(InsTestApiData.commoditySmallSortString());
		
		String[] detailTest = { "旅平險:與「人」相關的保障，如意外受傷、疾病、醫療等等", "不便險:與「物」相關的保障，如班機延誤、行李遺失、竊盜、租車等" };
		
		Map<String, Object> respDetail = (Map<String, Object>) result.get("detail");
		if (StringUtils.equals(respDetail.get("RETURN_CODE").toString(), "0000")) {
			List<Map<String, Object>> datas = (List<Map<String, Object>>) respDetail.get("DATAS");	
			//移到共用牌卡
			DemandProductionCardUtis cardUtis = new DemandProductionCardUtis();
			
			botResponseUtils.setCardsResult(botMessageVO, BotMessageVO.CTYPE_CARD, cardUtis.getCommoditySmallSortCard(
					tpiGatewayResult, botMessageVO, datas, detailTest, "商品", "clickTouristInsType"));
		}
		
		return tpiGatewayResult.setData(botMessageVO);
	}

	// 承辦項目
	@Override
	public TPIGatewayResult undertakeProject(InsProductTellRequest reqVO) {
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		reqVO.AnalyzeCommodityType();
		
		List<String> msgList =new ArrayList<String>();
		List<QuickReplay> optionList =new ArrayList<QuickReplay>();

		Map<String,Object> map = new HashMap<String, Object>();
		map.put("PRODUCT_CODE", reqVO.getProductCode());
		map.put("PRODUCT_SUB_CODE",reqVO.getProductSubCode());
		map.put("CODE_ID_L1", reqVO.getCodeIdL1());
		map.put("CODE_ID_L2", reqVO.getCodeIdL2());
		map.put("CODE_ID_L3", reqVO.getCodeIdL3());

		Map<String, Object> req = new HashMap<String, Object>();
		req.put("map", map);
		
		//產險測試環境
		Map<String,Object>  result = JSON.parseObject(insProductTellAPIFeignService.getT01(req),Map.class);
		//公司測試用
		//Map<String,Object>  result = JSON.parseObject(InsTestApiData.undertakeProjectString());
		
		Map<String, Object> respDetail = (Map<String, Object>) result.get("detail");
		if (StringUtils.equals(respDetail.get("RETURN_CODE").toString(), "0000")) {
			List<Map<String, Object>> datas = (List<Map<String, Object>>) respDetail.get("DATAS");
			for (Map<String,Object> data : datas) {
				String commodityType = data.get("PRODUCT_SUB_CODE") + "_" + data.get("CODE_ID_L3") + "_" + data.get("INSURE_ITEM_ID");
				
				Map<String, Object> parameter = new HashMap<String, Object>();
				parameter.put("commodityType", commodityType);
				QuickReplay quickReplay = new InsQuickReplay(1, data.get("INSURE_ITEM").toString(), data.get("INSURE_ITEM").toString(), "商品", "projectContent", parameter);
				LinkListItem linkList = botResponseUtils.getLinkListItem(botMessageVO, quickReplay);
				
				msgList.add(data.get("INSURE_ITEM").toString());
				optionList.add(quickReplay);
				dataList.add(linkList.getLinkList());
			}
		}
		
		botResponseUtils.setInsTopQuestion(botMessageVO, dataList, "");
		tpiGatewayResult.setMsgList(msgList);
		tpiGatewayResult.setOptionList(optionList);
		return tpiGatewayResult.setData(botMessageVO);
	}

	// 項目內容
	@Override
	public TPIGatewayResult projectContent(InsProductTellRequest reqVO) {
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		reqVO.AnalyzeCommodityType();

		Map<String,Object> map = new HashMap<String, Object>();
		map.put("PRODUCT_CODE", reqVO.getProductCode());
		map.put("PRODUCT_SUB_CODE",reqVO.getProductSubCode());
		map.put("CODE_ID_L1", reqVO.getCodeIdL1());
		map.put("CODE_ID_L2", reqVO.getCodeIdL2());
		map.put("CODE_ID_L3", reqVO.getCodeIdL3());
		map.put("INSURE_ITEM_ID", reqVO.getInsureItemId());
		
		Map<String, Object> req = new HashMap<String, Object>();
		req.put("map", map);
		
		//產險測試環境
		Map<String,Object>  result = JSON.parseObject(insProductTellAPIFeignService.getT01(req),Map.class);
		//公司測試用
		//Map<String,Object>  result = JSON.parseObject(InsTestApiData.projectContentString());
				
		Map<String, Object> respDetail = (Map<String, Object>) result.get("detail");
		if (StringUtils.equals(respDetail.get("RETURN_CODE").toString(), "0000")) {
			List<String> msgList =new ArrayList<String>();
			List<QuickReplay> optionList =new ArrayList<QuickReplay>();
			List<Map<String, Object>> datas = (List<Map<String, Object>>) respDetail.get("DATAS");
			
			botResponseUtils.setTextResult(botMessageVO, "你想知道哪個類別的問題呢？");
			
			//移到共用牌卡
			DemandProductionCardUtis cardUtis = new DemandProductionCardUtis();
			
			botResponseUtils.setCardsResult(botMessageVO, BotMessageVO.CTYPE_CARD, cardUtis.projectContentCard(botMessageVO, reqVO, datas, msgList, optionList, "商品", "projectContentQA"));
			
			// 2019/12/10 只有從國泰產險的旅綜險商品諮詢進入才出現回覆訊息，如從信用卡、1100贈送，以及從保單查詢串接商品諮詢時都不要出現！
			if (StringUtils.equals(map.get("CODE_ID_L1").toString(), "A")) {
				botResponseUtils.setTextResult(botMessageVO, "你也可以到<a href=\"javascript:void(0);\" onclick=\"window.open('"+InsUrlGather.travel_quote+DataUtils.getRectifySSOId(reqVO.getSsoId())+"');\">bobe線上投保網站</a>，直接投保旅平險哦~");
				
				botResponseUtils.setTextResult(botMessageVO, "刷信用卡贈送的旅遊綜合保險、投保國泰人壽贈送或加購的不便險保障，可到<a href=\"javascript:void(0);\" onclick=\"App.showUserMsg('旅遊保障查詢');App.sendMsgToChatWeb('旅遊綜合保險保單查詢');\">旅遊保障查詢</a>");
			}
			
			botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_03, InsQuickReplay.M02_TOURISM_3,
					InsQuickReplay.M02_TOURISM_4, InsQuickReplay.M02_TOURISM_5, SecQuickReplay.INIT);
			
			msgList.add(InsQuickReplay.M02_TOURISM_3.getText());
			msgList.add(InsQuickReplay.M02_TOURISM_4.getText());
			
			optionList.add(InsQuickReplay.M02_TOURISM_3);
			optionList.add(InsQuickReplay.M02_TOURISM_4);
			
			tpiGatewayResult.setMsgList(msgList);
			tpiGatewayResult.setOptionList(optionList);		
		}

		return tpiGatewayResult.setData(botMessageVO);
	}
	
	// 項目內容 From A06看保障項目內容說明
	@Override
	public TPIGatewayResult projectContentA06(InsProductTellRequest reqVO) {
		TPIGatewayResult tpiGatewayResult = TPIGatewayResult.ok();
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());

		Map<String,Object> map = new HashMap<String, Object>();
		map.put("POLICY_NO", reqVO.getPolicyNo());
		map.put("PRODUCT_POLICY_CODE",reqVO.getProductPolicyCode());
		map.put("CATEGORY_CODE", reqVO.getCategoryCode());
		map.put("PRODUCT_SUB_ID", reqVO.getProductSubId());
		
		Map<String, Object> req = new HashMap<String, Object>();
		req.put("map", map);
		
		//產險測試環境
		Map<String,Object>  result = JSON.parseObject(insProductTellAPIFeignService.getT01(req),Map.class);
		//公司測試用
		//Map<String,Object>  result = JSON.parseObject(InsTestApiData.projectContentString());
		
		
		
		Map<String, Object> respDetail = (Map<String, Object>) result.get("detail");
		if (StringUtils.equals(respDetail.get("RETURN_CODE").toString(), "0000")) {
			List<String> msgList =new ArrayList<String>();
			List<QuickReplay> optionList =new ArrayList<QuickReplay>();
			List<Map<String, Object>> datas = (List<Map<String, Object>>) respDetail.get("DATAS");
			
			botResponseUtils.setTextResult(botMessageVO, "你想知道哪個類別的問題呢？");
			
			//移到共用牌卡
			DemandProductionCardUtis cardUtis = new DemandProductionCardUtis();
			
			botResponseUtils.setCardsResult(botMessageVO, BotMessageVO.CTYPE_CARD, cardUtis.projectContentCard(botMessageVO, reqVO, datas, msgList, optionList, "商品", "projectContentQA"));
			
			//botResponseUtils.setTextResult(botMessageVO, "你也可以到<a href=\"javascript:void(0);\" onclick=\"window.open('"+InsUrlGather.travel_quote+DataUtils.getRectifySSOId(reqVO.getSsoId())+"');\">bobe線上投保網站</a>，直接投保旅平險哦");
			botResponseUtils.setQuickReplayResult(botMessageVO, BotMessageVO.LTYPE_03, InsQuickReplay.M02_TOURISM_3,
					InsQuickReplay.M02_TOURISM_4, InsQuickReplay.M02_TOURISM_5, SecQuickReplay.INIT);
			
			msgList.add(InsQuickReplay.M02_TOURISM_3.getText());
			msgList.add(InsQuickReplay.M02_TOURISM_4.getText());
			
			optionList.add(InsQuickReplay.M02_TOURISM_3);
			optionList.add(InsQuickReplay.M02_TOURISM_4);
			
			tpiGatewayResult.setMsgList(msgList);
			tpiGatewayResult.setOptionList(optionList);		
		}

		return tpiGatewayResult.setData(botMessageVO);
	}

	// 項目內容QA
	@Override
	public BotMessageVO projectContentQA(InsProductTellRequest reqVO) {
		BotMessageVO botMessageVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		reqVO.AnalyzeCommodityType();
		
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("PRODUCT_CODE", reqVO.getProductCode());
		map.put("PRODUCT_SUB_CODE",reqVO.getProductSubCode());
		map.put("CODE_ID_L1", reqVO.getCodeIdL1());
		map.put("CODE_ID_L2", reqVO.getCodeIdL2());
		map.put("CODE_ID_L3", reqVO.getCodeIdL3());
		map.put("INSURE_ITEM_ID", reqVO.getInsureItemId());
		
		Map<String, Object> req = new HashMap<String, Object>();
		req.put("map", map);
		
		//產險測試環境
		Map<String,Object>  result = JSON.parseObject(insProductTellAPIFeignService.getT01(req),Map.class);
		//公司測試用
		//Map<String,Object>  result = JSON.parseObject(InsTestApiData.projectContentString());
		
		Map<String, Object> respDetail = (Map<String, Object>) result.get("detail");
		if (StringUtils.equals(respDetail.get("RETURN_CODE").toString(), "0000")) {
			
			List<Map<String, Object>> datas = (List<Map<String, Object>>) respDetail.get("DATAS");
			Map<String,Object> data = datas.get(0);
			String text = data.get(reqVO.getCommodityClick()).toString();
			if(StringUtils.isNotBlank(text)) {
				botResponseUtils.setInsRescue(botMessageVO, reqVO.getCommodityClickName(), text.replace("\r\n", "<br>"));				
			}else {
				botResponseUtils.setInsRescue(botMessageVO, reqVO.getCommodityClickName(), "<br>無資訊<br>");
			}
		}
		
		BotMessageVO quickReplayListVO = botResponseUtils.initBotMessage(reqVO.getChannel(), reqVO.getRole());
		
		botResponseUtils.setQuickReplayResult(quickReplayListVO, BotMessageVO.LTYPE_03, InsQuickReplay.M02_TOURISM_3,
				InsQuickReplay.M02_TOURISM_4, InsQuickReplay.M02_TOURISM_5, SecQuickReplay.INIT);
		
		List<Map<String,Object>> content = quickReplayListVO.getContent();
		botMessageVO.getContent().get(0).put("quickReplayList", content);
		
		return botMessageVO;
	}

//  商品大類-API取資料
//	// 商品大類-牌卡
//	private List<Map<String, Object>> getCommodityBigSortList(TPIGatewayResult tpiGatewayResult,BotMessageVO vo, List<Map<String, Object>> datas) {
//		List<Map<String, Object>> cardList = new ArrayList<>();
//		List<String> msgList =new ArrayList<String>();
//		List<QuickReplay> optionList =new ArrayList<QuickReplay>();
//		
//		String[] ImageUrls = { "policy_03.jpg", "policy_03.jpg", "policy_03.jpg", "policy_03.jpg" };
//
//		for (int i = 0; i < ImageUrls.length; i++) {
//			// card1
//			CardItem cards = vo.new CardItem();
//			cards.setCName("");
//			cards.setCWidth(BotMessageVO.CWIDTH_200);
//
//			CImageDataItem cImageData = vo.new CImageDataItem();
//			cImageData.setCImage(BotMessageVO.IMG_CC);
//			cImageData.setCImageUrl(ImageUrls[i]);
//			cards.setCImageData(cImageData.getCImageDatas());
//			
//			Map<String,Object> data = datas.get(i);
//			String commodityType = data.get("PRODUCT_SUB_CODE") + "_" + data.get("CODE_ID_L1");
//
//			Map<String, Object> parameter = new HashMap<String, Object>();
//			parameter.put("commodityType", commodityType);
//			
//			QuickReplay quickReplay = new InsQuickReplay(1, data.get("CODE_NAME_L1").toString(),data.get("CODE_NAME_L1").toString(), "", "", parameter);
//			
//			cards.setCLinkList(botResponseUtils.getCLinkList(vo, quickReplay));
//			
//			msgList.add(data.get("CODE_NAME_L1").toString());
//			optionList.add(quickReplay);			
//			cardList.add(cards.getCards());
//		}
//
//		tpiGatewayResult.setMsgList(msgList);
//		tpiGatewayResult.setOptionList(optionList);
//		return cardList;
//	}
	
	/**
	 * 寫死
	 * 商品大類-牌卡
	 * @param tpiGatewayResult
	 * @param vo
	 * @param imageUrls
	 * @param commodityBigSors
	 * @return
	 */
	private List<Map<String, Object>> getCommodityBigSortList(TPIGatewayResult tpiGatewayResult, BotMessageVO vo, List<String> imageUrls, List<QuickReplay> commodityBigSors) {
		List<Map<String, Object>> cardList = new ArrayList<>();
		List<String> msgList = new ArrayList<String>();
		List<QuickReplay> optionList = new ArrayList<QuickReplay>();

		for (int i = 0; i < imageUrls.size(); i++) {
			// card1
			CardItem cards = vo.new CardItem();
			cards.setCName("");
			cards.setCWidth(BotMessageVO.CWIDTH_200);

			CImageDataItem cImageData = vo.new CImageDataItem();
			cImageData.setCImage(BotMessageVO.IMG_CC);
			cImageData.setCImageUrl(imageUrls.get(i));
			cards.setCImageData(cImageData.getCImageDatas());
			cards.setCLinkList(botResponseUtils.getCLinkList(vo, commodityBigSors.get(i)));

			msgList.add(commodityBigSors.get(i).getText());
			optionList.add(commodityBigSors.get(i));
			cardList.add(cards.getCards());
		}

		tpiGatewayResult.setMsgList(msgList);
		tpiGatewayResult.setOptionList(optionList);
		return cardList;
	}
	
	/**
	 * 商品中類-牌卡
	 * @param tpiGatewayResult
	 * @param botMessageVO
	 * @param datas
	 * @return
	 */
	private List<QuickReplay> commodityMediumSortList(TPIGatewayResult tpiGatewayResult,BotMessageVO botMessageVO,List<Map<String, Object>> datas) {
		List<QuickReplay> optionList = new ArrayList<QuickReplay>();
		List<String> msgList =new ArrayList<String>();
		
		for(Map<String,Object> map:datas) {
			String commodityType = map.get("PRODUCT_SUB_CODE") + "_" + map.get("CODE_ID_L2");
			
			Map<String, Object> parameter = new HashMap<String, Object>();
			parameter.put("commodityType", commodityType);
			parameter.put("mediumType", map.get("CODE_ID_L2"));
			
			msgList.add(map.get("CODE_NAME_L2").toString());
			optionList.add(new InsQuickReplay(1, map.get("CODE_NAME_L2").toString(), map.get("CODE_NAME_L2").toString(), "商品", "clickTouristArea", parameter));
		}
		
		tpiGatewayResult.setMsgList(msgList);
		tpiGatewayResult.setOptionList(optionList);
		return optionList;
	}

	/**
	 * 取得ecxel內容資料 filePath:檔案路徑 fileName:檔名 rowInitial:起始列 colInitial:起始行
	 * keyStrings:key名稱 sheetName:Shee名稱
	 */
	public List<Map<String, Object>> getExcel2(BotMessageVO vo, String filePath, String fileName, int rowInitial,
			int colInitial, String[] keyStrings, String sheetName) {
		final String EXCEL_XLS = "xls";
		final String EXCEL_XLSX = "xlsx";
		String[] keys = null;
		Workbook wb = null;
		Sheet sheet = null;
		String[] divTitle = null;
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();

		// 抓excel資料並做成list
		try {
			FileInputStream fileIn = new FileInputStream(filePath + fileName);
			if (fileName.endsWith(EXCEL_XLS)) {// Excel 2003
				wb = new HSSFWorkbook(fileIn);
			} else if (fileName.endsWith(EXCEL_XLSX)) {// Excel 2007/2010
				wb = new XSSFWorkbook(fileIn);
			}
			// 取得第一個Sheet
			if (StringUtils.isNotBlank(sheetName)) {
				sheet = wb.getSheet(sheetName);
			} else {
				sheet = wb.getSheetAt(0);
			}

			// getLastRowNum 如果sheet中一行數據都沒有則返回-1，只有第一行有數據則返回0，最後有數據的行是第n行則返回n-1；
			// getPhysicalNumberOfRows 獲取有紀錄的行數，即 : 最後有數據的行數是n行，前面有m行是空行沒數據，則返回n-m;

			int rowLength = sheet.getPhysicalNumberOfRows();
			Row rowZero = sheet.getRow(rowInitial);
			int colLength = rowZero.getPhysicalNumberOfCells();

			if (keyStrings == null) {
				keys = new String[colLength - colInitial];
				Row row = sheet.getRow(rowInitial);
				for (int j = colInitial; j < colLength; j++) {
					Cell cell = row.getCell(j);
					keys[j - colInitial] = cell.toString();
				}
				rowLength += 1;
			} else {
				keys = keyStrings;
			}

			divTitle = new String[colLength - colInitial];

			for (int i = rowInitial + 1; i < rowLength; i++) {
				Row row = sheet.getRow(i);
				Map<String, Object> map = new HashMap<String, Object>();
				for (int j = colInitial; j < colLength; j++) {
					Cell cell = row.getCell(j);
					if (MergedRegion(sheet, i, j)) {
						if (StringUtils.isBlank(cell.toString())) {
							map.put(keys[j - colInitial], divTitle[j - colInitial]);
						} else {
							map.put(keys[j - colInitial], cell.toString());
							divTitle[j - colInitial] = cell.toString();
						}
					} else {
						map.put(keys[j - colInitial], cell.toString());
					}
				}
				resultList.add(map);
			}

		} catch (Exception e) {

		}
		return resultList;
	}

	public Boolean MergedRegion(Sheet sheet, int rowInt, int cellInt) {
		for (int i = 0; i < sheet.getNumMergedRegions(); i++) {
			int firstColumn = sheet.getMergedRegion(i).getFirstColumn();
			int lastColumn = sheet.getMergedRegion(i).getLastColumn();
			int firstRow = sheet.getMergedRegion(i).getFirstRow();
			int lastRow = sheet.getMergedRegion(i).getLastRow();
			if ((firstColumn <= cellInt && lastColumn >= cellInt) && (firstRow <= rowInt && lastRow >= rowInt)) {
				return true;
			}
		}
		return false;
	}
}
