package com.tp.tpigateway.modules.cathayins.service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotRequest;
import com.tp.common.model.TPIGatewayResult;
import com.tp.common.model.cathaysec.FallBackRequest;

import java.util.Map;

public interface CathayInsService {

    TPIGatewayResult hello(BotRequest botRequest);

    TPIGatewayResult fallback(FallBackRequest fallBackRequest);

    TPIGatewayResult handleFaqReplay(Map<String, Object> request);

    TPIGatewayResult supportNLU(Map<String, Object> request);
    
    TPIGatewayResult getTopquestion(String topQuestionType);

    BotMessageVO showSurvey(BotRequest botRequest);
    
    
}
