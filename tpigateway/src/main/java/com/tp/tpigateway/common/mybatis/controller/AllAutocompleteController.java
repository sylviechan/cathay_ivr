package com.tp.tpigateway.common.mybatis.controller;

import com.tp.common.annotation.NotShowAopLog;
import com.tp.common.model.BotMessageVO;
import com.tp.common.model.TPIGatewayResult;
import com.tp.tpigateway.common.controller.BaseController;
import com.tp.tpigateway.common.mybatis.entity.AllAutocomplete;
import com.tp.tpigateway.common.mybatis.service.IAllAutocompleteService;
import com.tp.tpigateway.common.util.BotResponseUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author matt
 * @since 2019-08-08
 */
@RestController
public class AllAutocompleteController extends BaseController {

    @Autowired
    private IAllAutocompleteService autocompleteService;

    @Autowired
    private BotResponseUtils botResponseUtils;

    @ResponseBody
    @NotShowAopLog
    @GetMapping(value = "/getAllAutocomplete", produces = "application/json; charset=UTF-8")
    public TPIGatewayResult getAllAutoComplete() {
        List<AllAutocomplete> list = autocompleteService.getAllAutoCompleteList();
        BotMessageVO botMsg = botResponseUtils.getAutoCompleteBotMessageVO(list);
        Map<String, Object> data = (Map<String, Object>) CollectionUtils.get(botMsg.getContent(), 0);
        return TPIGatewayResult.ok().setData(data);
    }

}
