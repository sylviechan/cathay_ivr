package com.tp.tpigateway.common.exception;

import com.tp.common.exception.TPException;
import com.tp.common.model.TPIGatewayResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@RestControllerAdvice
public class TPExceptionHandler {

	private Logger log = LoggerFactory.getLogger(getClass());

	@ExceptionHandler(TPException.class)
	public TPIGatewayResult handleTPException(TPException e){
		log.error("TPException: " + e.getMsg());
		return TPIGatewayResult.error(e.getMsg());
	}

	@ExceptionHandler(Exception.class)
	public TPIGatewayResult handleException(Exception e){
		log.error(e.getMessage(), e);
		return TPIGatewayResult.error("系統發生異常");
	}

}
