package com.tp.tpigateway.common.mybatis.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.tp.common.mybatis.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author matt
 * @since 2019-08-08
 */
@ApiModel(value="AllAutocomplete对象", description="")
public class AllAutocomplete extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    private String displayString;

    private String hiddenString;

    private Integer stringOrder;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    public Integer getId() {
        return id;
    }

    public AllAutocomplete setId(Integer id) {
        this.id = id;
        return this;
    }
    public String getDisplayString() {
        return displayString;
    }

    public AllAutocomplete setDisplayString(String displayString) {
        this.displayString = displayString;
        return this;
    }
    public String getHiddenString() {
        return hiddenString;
    }

    public AllAutocomplete setHiddenString(String hiddenString) {
        this.hiddenString = hiddenString;
        return this;
    }
    public Integer getStringOrder() {
        return stringOrder;
    }

    public AllAutocomplete setStringOrder(Integer stringOrder) {
        this.stringOrder = stringOrder;
        return this;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public AllAutocomplete setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
        return this;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public AllAutocomplete setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("displayString", displayString)
                .append("hiddenString", hiddenString)
                .append("stringOrder", stringOrder)
                .append("createTime", createTime)
                .append("updateTime", updateTime)
                .toString();
    }
}
