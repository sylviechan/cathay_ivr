package com.tp.tpigateway.common.mybatis.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tp.tpigateway.common.mybatis.entity.AllAutocomplete;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author matt
 * @since 2019-08-08
 */
public interface AllAutocompleteMapper extends BaseMapper<AllAutocomplete> {

}
