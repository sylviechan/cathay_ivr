package com.tp.tpigateway.common.util;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class InsUrlGather {

	public static String insDomainURL;// 官方網址
	public static String digitalServiceDomain; // 數位服務平台
	public static String bobeDomain;// bobe網址
	public static String bankDomain; // 銀行網址

	// 功能使用網址
	public static String OCH1_0000;
	public static String OCH1_1000;
	public static String OCH1_0200;
	public static String OCH1_0300;
	public static String OCH1_0400;
	public static String OCAA_0100;
	public static String OCAB_0110;
	public static String OCAB_0120;
	public static String OCAB_0130;
	public static String OCAB_1000;
	public static String OCAB_1000_1;
	public static String ECIE_1000;

	public static String travel_quote;

	public static String insurance_Contact;
	public static String insurance_Products;
	public static String insurance_Products_Car;
	public static String insurance_Services;

	public static String pdf1;
	public static String pdf2;
	public static String pdf3;
	public static String pdf4;

	@Autowired
	private Environment env;

	@PostConstruct
	public void readConfig() {

		bobeDomain = env.getProperty("bobeURL.Formal");
		insDomainURL = env.getProperty("insURL.Formal");
		digitalServiceDomain = env.getProperty("digitalServiceURL.Formal");
		bankDomain = env.getProperty("bankURL.Formal");

		//混合:(商品介紹)旅綜險 - 投保 & (保單查詢)旅綜險 - 信用卡 - 投保 & (保單查詢)旅綜險 -國壽1100送 - 投保
		travel_quote = bobeDomain + "INSEBWeb/BOBE/travel/travel_quote/prompt"; 
		
		//混合:(保單查詢)車險 - 投保(查不到保單時) & (保單查詢)旅綜險 - 投保(查不到保單時) & (保單查詢)健傷險 - 投保(查不到保單時) & (保單批改)旅綜險 - 投保(查不到保單時)
		ECIE_1000 = bobeDomain + "INSEBWeb/servlet/HttpDispatcher/ECIE_1000/prompt";

		//混合:(理賠辦理)國泰世華信用卡理賠申請書 & (理賠辦理)匯豐信用卡理賠申請書
		pdf1 = insDomainURL + "insurance/assets/services/credit_card.pdf";

		//混合:(理賠辦理)理賠進度查詢 & (理賠辦理)理賠辦理
		OCAA_0100 = digitalServiceDomain + "INSOCWeb/servlet/HttpDispatcher/OCH1_0000/promptForCommon?loginForwardTrx=OCAA_0100/prompt";
		OCAB_1000_1 = digitalServiceDomain + "INSOCWeb/servlet/HttpDispatcher/OCH1_0000/promptForCommon?loginForwardTrx=OCAA_0100/prompt";

		// 商品介紹
		insurance_Products_Car = insDomainURL + "insurance/products/automobile/"; // 車險
		insurance_Products = insDomainURL + "insurance/products/"; // 健傷險 其他保險

		// 登入會員
		OCH1_0300 = digitalServiceDomain + "INSOCWeb/servlet/HttpDispatcher//OCH1_0300/prompt";// 忘記密碼
		OCH1_0200 = digitalServiceDomain + "INSOCWeb/servlet/HttpDispatcher/OCH1_0200/prompt";// 會員註冊

		//保單批改
		OCH1_0400 = digitalServiceDomain + "INSOCWeb/servlet/HttpDispatcher/OCH1_0000/promptForCommon?loginForwardTrx=OCH1_0400/prompt";//旅綜險 - 修改會員資料
		OCAB_0110 = digitalServiceDomain + "INSOCWeb/servlet/HttpDispatcher/OCH1_0000/promptForCommon?loginForwardTrx=OCAB_0100/prompt";//車險 - 改車主資料
		OCAB_0120 = digitalServiceDomain + "INSOCWeb/servlet/HttpDispatcher/OCH1_0000/promptForCommon?loginForwardTrx=OCAB_0100/prompt";//車險 - 改通訊資料
		OCAB_0130 = digitalServiceDomain + "INSOCWeb/servlet/HttpDispatcher/OCH1_0000/promptForCommon?loginForwardTrx=OCAB_0100/prompt";//車險 - 改車籍資料
		OCAB_1000 = digitalServiceDomain + "INSOCWeb/servlet/HttpDispatcher/OCH1_0000/promptForCommon?loginForwardTrx=OCAB_0100/prompt";//車險 - 批改進度查詢

		//理賠辦理
		pdf2 = insDomainURL + "insurance/assets/services/flower_credit_card.pdf";//花旗信用卡理賠申請書
		pdf3 = bankDomain + "cathaybk/-/media/5353f92c521e4c999b53cf8637493162.pdf?la=en&hash=53AC2F5C27D7966A91257061FEB801158CA2394C";//國壽1100送理賠申請書
		pdf4 = bankDomain + "cathaybk/-/media/28eb77b120c04dcaa3c7da2b2c8ed6ca.pdf?la=en&hash=A3B5512696E40542AFE8E3E4652815209E7B7595";//國壽1100送理賠應備文件
		
		//道路救援
		insurance_Services = insDomainURL + "insurance/services/assistance/";//道路就員資格查詢

		//真人客服
		insurance_Contact = insDomainURL + "insurance/contact/";//非服務時間 - 國泰產險客服信箱
		
		//功能使用網址
		OCH1_0000 = digitalServiceDomain + "INSOCWeb/servlet/HttpDispatcher/OCH1_0000/prompt";//主網1
		OCH1_1000 = digitalServiceDomain + "INSOCWeb/servlet/HttpDispatcher/OCH1_1000/prompt";//主網2
		
	}

}
