package com.tp.tpigateway.common.util;

import com.tp.tpigateway.common.mybatis.service.ISysConfigService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;

/**
 * properties 共用工具類
 */
@Component
public class PropUtils {

	private static Logger log = LoggerFactory.getLogger(PropUtils.class);

	private static String appId;

	private static String replayExcelPath;

	private static String apiChannel;

	private static Boolean isOpenApiMonitor;

	private static String cathaySecScoreFaqMin;

    private static String cathaySiteScoreFaqMin;

    private static String cathayInsScoreFaqMin;

	private static String cathaySecScoreNluMin;

	private static String cathaySiteScoreNluMin;

	private static String cathayInsScoreNluMin;

	public final static String RESULT_DESCRIPTION_SYSTEM_ERROR = "系統錯誤";
	public final static String RESULT_DESCRIPTION_PARAMETER_ERROR = "參數錯誤";

	@Autowired
	private Environment env;

	@Autowired
	private ISysConfigService sysConfigService;

	@PostConstruct
	public void readConfig() {
		appId = env.getProperty("app.id");
		replayExcelPath = env.getProperty("reply.excel.path");
		apiChannel = env.getProperty("api.channel", "AF");
		isOpenApiMonitor = Boolean.parseBoolean(env.getProperty("api.isOpenApiMonitor", "false"));

		cathaySecScoreNluMin = StringUtils.defaultString(getSysConfig("cathaysec.score.nlu.min"), "0.4");
		cathaySiteScoreNluMin = StringUtils.defaultString(getSysConfig("cathaysite.score.nlu.min"), "0.4");
		cathayInsScoreNluMin = StringUtils.defaultString(getSysConfig("cathayins.score.nlu.min"), "0.4");

        cathaySecScoreFaqMin = StringUtils.defaultString(getSysConfig("cathaysec.score.faq.min"), "0.6");
        cathaySiteScoreFaqMin = StringUtils.defaultString(getSysConfig("cathaysite.score.faq.min"), "0.6");
        cathayInsScoreFaqMin = StringUtils.defaultString(getSysConfig("cathayins.score.faq.min"), "0.6");
	}

	private String getSysConfig(String key) {
		Map<String, String> allSysConfig = sysConfigService.getAllSysConfig();
		return allSysConfig == null ? "" : allSysConfig.get(key);
	}

	public static String getCathaySecScoreNluMin() {
		return cathaySecScoreNluMin;
	}

	public static String getCathaySiteScoreNluMin() {
		return cathaySiteScoreNluMin;
	}

	public static String getCathayInsScoreNluMin() {
		return cathayInsScoreNluMin;
	}

    public static String getCathaySecScoreFaqMin() {
        return cathaySecScoreFaqMin;
    }

    public static String getCathaySiteScoreFaqMin() {
        return cathaySiteScoreFaqMin;
    }

    public static String getCathayInsScoreFaqMin() {
        return cathayInsScoreFaqMin;
    }

    public static String getAppId() {
		return appId;
	}

	public static String getReplayExcelPath() {
		return replayExcelPath;
	}

	public static String getApiChannel() {
		return apiChannel;
	}

	public String getProp(String name) {
		return env.getProperty(name);
	}

	public static Boolean getIsOpenApiMonitor() {
		return isOpenApiMonitor;
	}

}
