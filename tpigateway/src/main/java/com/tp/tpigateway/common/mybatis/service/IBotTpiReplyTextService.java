package com.tp.tpigateway.common.mybatis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tp.tpigateway.common.mybatis.entity.BotTpiReplyText;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author matt
 * @since 2019-08-12
 */
public interface IBotTpiReplyTextService extends IService<BotTpiReplyText> {

    String getReplyText(String roleId, String textId);

}
