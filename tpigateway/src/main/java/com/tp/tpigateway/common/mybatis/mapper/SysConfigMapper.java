package com.tp.tpigateway.common.mybatis.mapper;

import com.tp.tpigateway.common.mybatis.entity.SysConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author matt
 * @since 2019-08-19
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}
