package com.tp.tpigateway.common.mybatis.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.tp.common.mybatis.entity.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author matt
 * @since 2019-08-19
 */
public class SysConfig extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    private String key;

    private String value;

    private String remark;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    public Integer getId() {
        return id;
    }

    public SysConfig setId(Integer id) {
        this.id = id;
        return this;
    }
    public String getKey() {
        return key;
    }

    public SysConfig setKey(String key) {
        this.key = key;
        return this;
    }
    public String getValue() {
        return value;
    }

    public SysConfig setValue(String value) {
        this.value = value;
        return this;
    }
    public String getRemark() {
        return remark;
    }

    public SysConfig setRemark(String remark) {
        this.remark = remark;
        return this;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public SysConfig setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
        return this;
    }
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public SysConfig setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("key", key)
                .append("value", value)
                .append("remark", remark)
                .append("createTime", createTime)
                .append("updateTime", updateTime)
                .toString();
    }
}
