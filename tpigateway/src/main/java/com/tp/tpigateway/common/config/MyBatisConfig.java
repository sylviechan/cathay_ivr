package com.tp.tpigateway.common.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@MapperScan({
        "com.tp.tpigateway.common.mybatis.mapper",
        "com.tp.tpigateway.modules.cathaysec.mybatis.mapper",
        "com.tp.tpigateway.modules.cathayins.mybatis.mapper",
        "com.baomidou.mybatisplus.samples.quickstart.mapper"
})
public class MyBatisConfig {

    @Configuration
    protected static class MybatisPlusConfig {
        /**
         * 分页插件
         */
        @Bean
        public PaginationInterceptor paginationInterceptor() {
            return new PaginationInterceptor();
        }

    }

}
