package com.tp.tpigateway.common.mybatis.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.tp.tpigateway.common.mybatis.entity.AllAutocomplete;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author matt
 * @since 2019-08-08
 */
public interface IAllAutocompleteService extends IService<AllAutocomplete> {

    List<AllAutocomplete> getAllAutoCompleteList();

}
