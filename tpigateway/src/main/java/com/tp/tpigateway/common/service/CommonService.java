package com.tp.tpigateway.common.service;

import com.tp.common.model.BotMessageVO;
import com.tp.common.model.BotRequest;

public interface CommonService {
	
	/**
	 * For 投信/證券-呼叫登入頁面
	 */
	BotMessageVO getLogin(BotRequest botRequest);
}
