package com.tp.tpigateway.common.constant;

public class CacheConst {

    public static final String TPI_CACHE ="tpiCache";

    public static final String ALL_AUTO_COMPLETE_LIST_KEY = "allAutoCompleteList";

    public static final String SYS_CONFIG_KEY = "sysConfigMap";

    public static final String STOCK_ID_DATA_KEY = "stockIdDataMap";

    public static final String STOCK_NAME_DATA_KEY = "stockNameDataMap";

    public static final String BOT_TPI_REPLAY_TEXT_KEY = "botTpiReplayTextMap";

}
